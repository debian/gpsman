#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File		: langid.tcl
#  Last change:  6 October 2013
#
# Indonesian language file by Tri Agus Prayitno (acuss _AT_ bk.or.id)
#  last input from him inserted:  14 September 2008
#

# file langengl.tcl is consulted first; no need for duplicating entries here

# only 3 chars long names; check also ALLMONTH in file i18n-utf8.tcl
set MONTHNAMES "Jan Feb Mar Apr Mei Jun Jul Agt Sep Okt Nop Des"

set DLUNIT(KM,dist) km
set DLUNIT(KM,subdist) m
set DLUNIT(KM,speed) "km/jam"
set DLUNIT(KM,area) "km2"
set DLUNIT(NAUTMILE,dist) "mil laut"
set DLUNIT(NAUTMILE,subdist) kaki
set DLUNIT(NAUTMILE,speed) knot
set DLUNIT(NAUTMILE,area) "mil laut persegi"
set DLUNIT(STATMILE,dist) "mil darat"
set DLUNIT(STATMILE,subdist) kaki
set DLUNIT(STATMILE,speed) "mil/jam"
set DLUNIT(STATMILE,area) "wilayah"

set DLUNIT(M,dist) $DLUNIT(KM,subdist)
set DLUNIT(FT,dist) $DLUNIT(STATMILE,subdist)

set DTUNIT $DLUNIT($DISTUNIT,dist)
set SPUNIT $DLUNIT($DISTUNIT,speed)
set ARUNIT $DLUNIT($DISTUNIT,area)
set ALUNIT $DLUNIT($ALTUNIT,dist)

set MESS(RTcomp) "#\tWP\t\t$DTUNIT\tdeg\t$ALUNIT"
set MESS(TRcomp) "#\t\t\t\t$ALUNIT\t$DTUNIT\t$DTUNIT\th:m:s\t$SPUNIT\tdeg"
set MESS(WPNearest) "WP\t\t$DTUNIT\tdeg"

array set MESS {
    badscale   "Nilai salah untuk sekala peta"
    oktoexit   "Klik Ok untuk keluar (data yang belum disimpan akan hilang)"
    okclrmap   "Klik Ok untuk membersihkan layar"
    namelgth   "Nama tidak boleh lebih dari %d karakter"
    cmmtlgth   "Komentar tidak boleh lebih dari %d karakter"
    namevoid   "Nama harus diisi"
    baddate   "Tanggal salah; baris"
    badhdg   "Heading salah %s: harus %s atau +/-"
    badcoord   "Rentang/format koordinat salah: \"%s\", harusnya %s"
    outofrng   "Jumlah di luar jangkauan"
    UTMZN   "Harus huruf A..H, J..N, atau P..Z"
    badstrg "Karakter salah"
    strgvoid   "String tidak boleh kosong"
    nan   "\"%s\" bukan angka"
    written   "Ditulis oleh"
    undefWP   "Rute %s WP yang belum didefinisikan; gagal disimpan/ekspor"
    bigRT   "Rute %s > jumlah maks; gagal diekspor"
    unkndatum   "Datum tak dikenal"
    badcommdWP   "Perintah salah saat mengambil WP; baris"
    notabsWP   "Definisi WP salah; tidak ada tab, baris"
    undefinedWP   "Merujuk ke WP tak dikenal: %s"
    nofieldsWP   "WP salah, ada kolom yang kurang; baris"
    excfieldsWP  "WP salah, terlalu banyak kolom; baris"
    badWPsRT   "RT dengan daftar waypoint salah; baris"
    toomuchWPs   "Peringatan: ada lebih dari %d WP dalam RT"
    badcommdTP   "Perintah salah saat membuka TPs; baris"
    badTP   "Titik salah dalam TR; baris"
    badTPsTR   "TR dengan daftar titik trek salah; baris"
    noheader   "File tidak diawali dengan header"
    loaderr   "Kesalahan saat membuka dari file; baris"
    unkncommd   "Perintah tak dikenal saat membuka dari file; baris"
    noformat   "File tidak dimulai dengan Format baris"
    badformat   "Format salah, baris"
    badRT   "Definisi RT salah; baris"
    badRTargs   "Argumen RT salah; baris"
    badTR   "Definisi TR salah; baris"
    fileact   "%s file %s?"
    filexists   "File sudah ada!"
    GPSok   "Koneksi ok"
    toomany   "Terlalu banyak %ss (> %d)"
    cantfgt   "Tidak bisa melepaskan %s: milik item yang terpetakan"
    cantfgted   "Tidak bisa melepaskan %s: sedang diedit"
    cantmapRTunkn   "Rute gagal dipetakan; tidak ada info untuk WP"
    cantmapRTed   "Rute tidak dipetakan; WP sedang diedit"
    cantrun   "Gagal menjalankan"
    inprogr   "Operasi ini sudah dieksekusi"
    cantread   "Gagal membaca file bantuan"
    cantwrtopt   "Gagal menulis file konfigurasi"
    voidRT   "Rute tidak berisi waypoint"
    activeRT   "Rute 0 adalah rute aktif; lanjutkan?"
    voidTR   "Trek tidak berisi titik"
    idinuse   "Pengidentifikasi sudah digunakan"
    cantunmap   "Tidak bisa membongkar peta %s: milik item yang terpetakan"
    askrevert   "Kembalikan ke data awal?"
    askforget   "Lepaskan %s ini?"
    notimpl   "Belum diimplementasikan"
    forgetall   "Lepaskan semua item dalam daftar %s?"
    counted   "Ada %d %ss terdefinisi"
    notlisted   "Item tidak terdaftar"
    wrgval   "Nilai salah untuk"
    voidGR   "Grup tidak memiliki elemen"
    initselfGR   "Grup berisi grup itu sendiri melalui"
    GRelout   "Perintah untuk elemen berada di luar definisi GR; baris"
    badGRels   "Grup dengan daftar elemen tidak benar; baris"
    badcommdGRel   "Perintah tidak benar saat memanggil elemen GR; baris"
    notypeforGRel   "Elemen tanpa jenis; baris"
    badGRel   "Definisi elemen tidak benar; baris"
    check   "Memeriksa koneksi"
    toomanyerr   "Terlalu banyak kesalahan; membatalkan..."
    getWP   "Mengambil WPoint"
    getRT   "Mengambil Rute"
    getTR   "Mengambil Trek"
    putWP   "Meletakkan WPoint"
    putRT   "Meletakkan Rute"
    putTR   "Meletakkan Trek"
    fillbuffWP  "Memanggil penyangga WP"
    fillbuffRT  "Memanggil penyangga RT"
    noACKNAK  "Menerima paket saat mengharapkan ACK/NAK; periksa koneksi GPS"
    badplatform   "Tidak ada I/O dengan port serial pada platform ini"
    badserial "Tidak bisa membuka alat %s"
    nodata    "Tidak ada %ss pada pesawat"
    badimage "File gambar tidak benar"
    mapadjust "Masukkan WP pada peta; dan klik Ok jika sudah selesai"
    duplicate "%s sudah terpilih"
    clrcurrmap "Kosongkan peta sekarang?"
    mbkbaddatum "Datum tidak ada/dikenal"
    mbkbadscale "Sekala tidak boleh negatif"
    mbkbadat "Argumen salah"
    edityourrisk "Edit dengan resiko sendiri!"
    okclrbkmap   "Klik Ok untuk membersihkan latar belakang peta"
    okclrbkim    "Klik Ok untuk membersihkan gambar pada"
    badSYMBOLcode "Kode simbol tidak benar"
    badDISPOPTcode "Kode opsi tampilan tidak benar"
    goingdown "Konfigurasi tersimpan; tutup dan jalankan kembali program ini"
    putwhat "Jenis item apa yang mau diletakkan?"
    readwhat "Jenis item apa yang mau dibaca?"
    noWPinx "Terlalu banyak WP untuk diterima"
    noICinx "Terlalu banyak IC untuk diterima"
    getIC "Mengambil ikon"
    serial1 "awalan header salah"
    serial2 "Pengembalian perintah salah"
    checksum1 "Header Checksum salah"
    checksum2 "Data Checksum error"
    receiver "Tidak tersambung ke alat!"
    importonly1 "Hanya dapat mengimpor 1 jenis data"
    exportonly1 "Hanya dapat mengekspor 1 jenis data"
    outdatedprefs "File konfigurasi sudah basi; mohon periksa sekarang"
    mustchoose1type "Paling tidak 1 jenis item harus dipilih"
    nosuchitems "Tidak ada item yang terkait dengan deskripsi"
    resultsin "Hasil pencarian pada Grup"
    badWP "WP belum dimasukkan atau didefinisikan"
    badangle "Sudut harus > 0 dan < 360 derajat"
    georefhow  "Metode Geo-referensi"
    cantsolve  "Tidak dapat menyelesaikan persamaan"
    transfcantscale "Tidak ada sekala dengan transformasi ini"
    oldfilefmt "Format file lama; mohon simpan sebagai format baru!"
    unknProj "Proyeksi tak dikenal"
    unknTransf "Transformasi tak dikenal"
    badProjargs "Argumen salah untuk proyeksi"
    badTransfargs "Argumen salah untuk transformasi"
    badfield "Atribut=pasangan nilai tidak benar"
    badattr "Nama attribut salah"
    missattrs "Atribut yang kurang"
    mbkbadproj "Proyeksi salah"
    mbkbadtransf "Transformasi salah"
    notUTMproj "Info tersimpan untuk UTM; lanjutkan dengan memproyeksikan ke"
    projchg    "Pastikan nilai pada parameter proyeksi"
    badparam "Nilai salah untuk %s"
    connectedto "Terhubung ke %s"
    recnotsuppd "Model pesawat GPS tidak didukung"
    gotprots "Menerima definisi protokol"
    badprots "Definisi protokol tidak benar"
    defprots "Menggunakan tabel protokol"
    nohidden "Abaikan data tersembunyi?"
    badRS "Ada tahap rute diluar definisi RT; baris"
    badWPsRSs "Ada tahap RT sebelum WP awal atau setelah WP akhir; baris"
    windowdestr "Jendela sudah ditutup!"
    badhidden "Format tidak benar untuk nilai tersembunyi"
    replname "Gantikan \"%s\" dengan nama yang memiliki paling tidak %d dari karakter berikut: %s"
    badalt "Nilai salah untuk ketinggian"
    baddistunit "Satuan jarak salah untuk sekala peta pada file konfigurasi"
    badgridzone "Zona grid salah"
    outofgrid "Posisi berada di luar rentang grid"
    timeoffnotint "Zona waktu harus dengan bilangan asli atau berakhiran .5"
    cantchkprot "Tidak bisa memeriksa dalam protokol ini"
    mustconn1st "Silakan periksa dulu koneksi ke pesawat GPS"
    rltmnotsupp "Log real-time tidak didukung oleh protokol pada pesawat GPS ini"
    createdir "Silakan buat direktori %s dan ulangi program"
    oktomkdir "Klik Ok untuk membuat direktori %s?"
    projnameabbr "Buat nama dan nama singkat untuk proyeksi baru"
    abbrevinuse "Nama singkat ini sudah ada"
    nameinuse "Nama ini sudah ada"
    projinuse "Proyeksi sedang digunakan; perubahan akan diabaikan"
    gridneedsdatum "Definisi grid tidak benar untuk %s; belum ada datum"
    badgriddatum "Datum untuk grid %s harus %s"
    cantchggriddatum "Grid ini harus dengan datum %s"
    gridinuse "Grid, digunakan oleh %s, tidak bisa dihapus; lanjutkan?"
    gridinusenochg "Grid ini digunakan oleh %s; perubahan diabaikan"
    cantwrtprgr "Gagal membuat file proyeksi"
    cantwrtdtel "Gagal membuat file datum"
    movingWP "Letakkan %s dengan klik kiri\nUntuk membatalkan klik kanan"
    missingdata "Data tidak cukup!"
    needs1wp "Rute harus berisi minimal 1 titik"
    emptypos "Posisi dengan kolom kosong"
    cantwrtsstate "Gagal menyimpan file: %s"
    cantrdsstate "Tidak bisa membaca file: %s"
    corruptsstate "File memiliki kesalahan: %s"
    editrisk "Edit dengan resiko sendiri!"
    savestate "Simpan keadaan sekarang?"
    delsstate "Hapus file yang sudah tersimpan?"
    badmapinfo "File parameter peta tidak benar"
    badMHloc "Pelokasi Maidenhead salah"
    areais "Luas (tidak memotong sendiri) poligon adalah %.3f%s"
    areatoosmall "Wilayah terlalu kecil (<%s km2)"
    projarea "Menghitung wilayah terproyeksi"
    selfintsct "WP berulang: RT tidak bisa memotong RT itu sendiri!"
    badinvmdist "Prakiraan salah saat membalik jarak meridian"
    badinvproj "Prakiraan salah saat membalik proyeksi %s"
    negdlatlong "Rentang Lintang/bujur tidak boleh negatif!"
    allundef "Tidak ada definisi untuk WP dalam GR"
    badfloats "Konversi titik-mengambang tidak berjalan dengan semestinya; anda yakin ingin terhubung?"
    noprintcmd "Tidak ada perintah pencetakan; masukkan pada konfigurasi"
    cantexecasroot "GPSMan tidak bisa dijalankan oleh root"
    badargtofunc "Argumen tidak benar untuk fungsi %s"
    redefproj "Proyeksi %s buatan sendiri akan menghapus proyeksi yang sudah ada yang bernama singkat sama; silakan ganti definisi anda!"
    badattr "Nama kolom %s (tidak harus diisi) salah: %s"
    badattrval "Nilai kolom %s (tidak harus diisi) salah: %s"
    couldntcd "Gagal berpindah ke direktori %s"
    shpext "Ekstensi salah %s; Gunakan .shp, .shx, .dbf?"
    shpcntopen "Tidak bisa membuat/membuka file Shapefile"
    shpcntcrtfs "Gagal membuat kolom .dbf file Shapefile"
    shpcntwrtfs "Gagal menulis kolom .dbf file Shapefile"
    shpoutmem "Memori habis!"
    shpemptyfile "File kosong"
    shpwrongfile "Jenis file salah"
    shplessdim "Dimensi pada file kurang dari yang dibutuhkan; lanjutkan?"
    shpbadWPinRT "WP ke-%d dengan nilai koordinat salah diabaikan pada RT %s"
    badGTMvers "Versi file GTrackMaker tidak bisa dijalankan"
    badGTMfile "Awalan yang salah pada file GTrackMaker"
    badGTMcounts "Nilai negatif pada file GTrackMaker"
    badGTMlat "Nilai lintang di luar jangkauan file GTrackMaker"
    badGTMlong "Nilai bujur di luar jangkaian file GTrackMaker"
    badGTMdatum "Datum salah pada file GTrackMaker"
    unobscmap "Kemunginan kesalahan terjadi karena ada jendela/ikon pada peta; ulangi setelah jeda?"
    cantwrtimg "Gagal membuat file gambar dengan format %s"
    TRsimplres "TR hasil penyederhanaan dibuat dengan nama %s"
    cantsaveRTid "%d RT tidak disimpan: pengidentifikasi bukan angka"
    cantsaveTRid "%d TR tidak disimpan: pengidentifikasi bukan angka"
    badtrvconf "Konfigurasi rusak; ulangi dengan yang kosong"
    drvsimoff "Simulasi kendara: belum dimulai!"
    needWP "Simulasi kendara: silakan buka atau buat beberapa WP dulu"
    chgrecprot "Harap ganti protokol penerima"
    clrtrvlog "Kosongkan catatan perjalanan?"
    frgetGRcs "Abaikan grup ini dan semua isi di dalamnya ?!"
    nmeainuse "Log real-time sudah tercatat atau ada file NMEA lain yang sedang dibuka"
    badfile "Ada kesalahan dalam membaca file"
    RTnoWPname "Fitur WP dalam RT menurut nama saja sudah tidak ada"
    distlarge "Jarak terlalu besar!"
    timeconsmg "Operasi ini sangat lama: lanjutkan?"
    needNpoints "Anda harus memasukkan minimal %s titik!"
    twotimeoffsets "Beda zona waktu dalam file"
    notimeoffset "Tanpa beda zona waktu dalam file; asumsi 0"
    baddateas "Tanggal tidak benar: %s"
    unknownenc "Enkoding karakter tak dikenal %s"
    chgbaudto "Mencoba mengubah baud rate ke %s; mohon tunggu..."
    baudchgfailed "Tidak bisa mengubah baud rate"
    busytrylater "Ada perinta lain yang sedang berjalan, silakan coba lagi nanti"
    obssplit "Hasil pemisahan %s bernama \"%s\""
}

set TXT(RTcompflds) "# WP {$DTUNIT} drj {$ALUNIT} tahap label"
set TXT(TRcompflds) \
    "TP {} {} {} {$ALUNIT} {$DTUNIT} {$DTUNIT} h:m:s {$SPUNIT} deg"
set TXT(starttoend) "Mulai: untuk mengakhiri %s $DTUNIT;"
set TXT(startmax) "maks %s $DTUNIT;"
set TXT(WPnearflds) "WP {$DTUNIT} drj"
set TXT(within) "Dalam (${DTUNIT}s)"
set TXT(between) "Antara (${DTUNIT}s)"

array set TXT {
    GMtit   "GPS Manager - versi"
    exit   Keluar
    map   Peta
    load   Buka
    loadfrm   "Buka dari"
    save   Simpan
    saveels "Simpan elemen"
    saveto   "Simpan sebagai"
    clear   Kosongkan
    clearall   "Kosongkan Semua"
    newWP   "WP Baru"
    newRT   "RT Baru"
    newTR   "TR Baru"
    newGR   "GR Baru"
    import   Impor
    importfrm   "Impor dari"
    export   Ekspor
    exportels "Ekspor elemen"
    exportto   "Ekspor ke"
    count   Hitung
    trueN   "Utara Sebenarnya"
    automagn   "Auto Magnetis"
    usrdef   "Definsikan sendiri"
    nameWP   WPoint
    nameRT   Rute
    nameTR   Trek
    nameGR   Grup
    namePlot   Plot
    nameMap   Peta
    nameRTComp   "Penghitungan Rute"
    nameTRComp   "Penghitungan Trek"
    GPSrec   "Pesawat GPS"
    turnoff   "Matikan"
    get   Ambil
    put   Letakkan
    all   Semua
    select   Pilih
    selection   "Yang aktif"
    options   Pilihan
    DMS   DMS
    DMM   DMM
    DDD   DDD
    GRA   Tingkat
    UTM/UPS   UTM/UPS
    MH    MH
    message   Pesan
    cancel   Batalkan
    file   File
    ovwrt   Gantikan
    app   Gabung
    online   tersambung
    offline   terputus
    check   periksa
    create   Buat 
    revert   Kembalikan
    colour   warna
    grey   "abu-abu"
    mono   mono
    portr   tegak
    landsc   mendatar
    legend   Legenda
    incscale   "Dengan sekala"
    more   Lanjut
    waypoint   Waypoint
    name   Nama
    created   "Sudah dibuat"
    cmmt   Komentar
    withWP  "Dengan WP ini:"
    displ   "Tampilkan di peta"
    startRT "Start RT"
    route   Rute
    number   Jumlah
    insb   "Sisipkan sebelum"
    insa   "Sisipkan setelah"
    del   Hapus
    repl   "Gantikan dengan"
    comp   Hitung
    RTcomp   "Penghitungan Rute"
    savecomp "Simpan Penghitungan"
    totdst   "Total jarak"
    tottime   "Total waktu"
    track   Trek
    chophd   "Potong kepala"
    choptl   "Potong ekor"
    incb   "Sertakan sebelum"
    date   Tanggal
    newdate   "Buat tanggal untuk titik selanjutnya"
    endprTR   "Akhir dari trek terdahulu"
    begnxt   "Awal untuk lanjutan"
    date1st   "Tanggal untuk titik pertama lanjutan"
    TRcomp   "Penghitungan Trek"
    avgsp   "Kec. rata-rata"
    maxsp   "Kec. maksimal"
    minsp   "Kec. minimal"
    lat   Lintang
    long   Bujur
    ze   ZT
    zn   ZU
    eastng   Easting
    nrthng   Northing
    zone   Zona
    change   Ganti
    forget   Lepaskan
    others   Lainnya
    opt_Interf  "Antarmuka"
    optLANG   Bahasa
    optISOLATIN1   "Buat karakter"
    optDELETE   "DEL menghapus karakter terakhir"
    optMWINDOWSCONF  "Jendela utama"
    optGPSREC   "GPS Model"
    opt_GPSRecConf   "Parameter pesawat"
    optACCEPTALLCHARS "Terima semua karakter"
    optNAMELENGTH   "Panjang nama maksimal"
    optINTERVAL "Interval pengambilan data"
    optCOMMENTLENGTH   "Panjang maks komentar"
    optMAXWPOINTS   "Jumlah maks WPoint"
    optMAXROUTES   "Jumlah maks Rute"
    optMAXWPINROUTE   "Jumlah maks WP dalam Rute"
    optMAXTPOINTS   "Jumlah maks titik trek"
    optCREATIONDATE   "Data mempunyai tanggal pembuatan"
    optNOLOWERCASE   "Data tanpa huruf kecil"
    optDEFAULTSYMBOL "Simbol default WP"
    optDEFAULTDISPOPT "Opsi tampilan default WP"
    opt_Data "Data"
    optEQNAMEDATA "Data dengan nama sama"
    optKEEPHIDDEN "Biarkan data tersembunyi"
    optDatum   Datum
    optTimeOffset   "Ofset waktu"
    optACCFORMULAE "Rumus tepat"
    optASKPROJPARAMS "Konfirmasi parameter proyeksi"
    optBalloonHelp "Panduan"
    optTRNUMBERINTVL "Tampilkan nomor TP pada peta setiap"
    opt_Formats   "Satuan dan format"
    optDISTUNIT   "Jarak"
    KM    km
    NAUTMILE    "mil laut"
    STATMILE    "mil darat"
    optPositionFormat   "Format posisi"
    optDateFormat   "Format tanggal"
    opt_Geom   "Geometri jendela"
    opt_MapGeom   "Geometri peta"
    optMAPWIDTH   "Lebar peta"
    optMAPHEIGHT   "Tinggi peta"
    optMAPSCLENGTH   "Panjang sekala peta"
    optMAPSCALE   "Sekala peta"
    optMAXMENUITEMS   "Jumlah maks item menu"
    optLPOSX   "Posisi x jendela daftar file"
    optLPOSY   "Posisi y jendela daftar file"
    optMPOSX   "Posisi x jendela peta"
    optMPOSY   "Posisi y jendela peta"
    optRPOSX   "Posisi x jendela pesawat"
    optRPOSY   "Posisi y jendela pesawat"
    optEPOSX   "Posisi x jendela kesalahan"
    optEPOSY   "Posisi y jendela kesalahan"
    optDPOSX   "Posisi x dialog"
    optDPOSY   "Posisi x dialog"
    optDPOSRTMAP "Ofset dialog RT/peta"
    optLISTWIDTH   "Lebar daftar file"
    optLISTHEIGHT   "Tinggi daftar file"
    optCOLOUR   Warna
    optCOLOUR,fg   Depan
    optCOLOUR,bg   Belakang
    optCOLOUR,messbg   "Latar belakang kesalahan"
    optCOLOUR,confbg   "Latar belakang konfirmasi"
    optCOLOUR,selbg   "Latar belakang aktif"
    optCOLOUR,dialbg   "Latar belakang input"
    optCOLOUR,offline   "Pesawat tidak tersambung"
    optCOLOUR,online   "Pesawat tersambung"
    optCOLOUR,check   "Tombol cek terpilih"
    optCOLOUR,ballbg   "Latar belakang panduan"
    optCOLOUR,ballfg   "Warna depan panduan"
    optMAPCOLOUR  "Warna peta"
    optMAPCOLOUR,mapsel   "Item peta aktif"
    optMAPCOLOUR,WP   "Waypoint pada peta"
    optMAPCOLOUR,RT   "Rute pada peta"
    optMAPCOLOUR,mkRT "Rute terdefinisi pada peta"
    optMAPCOLOUR,TR   "Trek pada peta"
    optMAPCOLOUR,TP   "Titik trek pada peta"
    optMAPCOLOUR,mapleg   "Legenda peta"
    optMAPCOLOUR,anim  "Animasi pada peta"
    optMAPCOLOUR,emptygrid  "Gambar tidak ada"
    optMAPCOLOUR,fullgrid   "Gambar yang ada"
    optMAPFONTSIZE "Ukuran huruf pada peta"
    optDEFMAPPROJ "Proyeksi peta"
    optDEFMAPPFRMT "Koordinat peta"
    opt_Files "Alat dan file"
    optDEFSPORT "Alat"
    optSAVESTATE "Simpan keadaan saat keluar"
    optDELSTATE "Hapus file setelah mengembalikan keadaan"
    optPERMS   "Hak akses file"
    optPRINTCMD "Perintah pencetakan"
    optPAPERSIZE "Ukuran kertas"
    red   Merah
    green   Hijau
    blue   Biru
    owner   Pemilik
    permgroup   Grup
    others   Lainnya
    fread   Baca
    fwrite   Tulis
    fexec   Eksekusi
    YYYYMMDD   YYYYMMDD
    MMDDYYYY   MMDDYYYY
    DDMMMYYYY   DDMMMYYYY
    YYYY-MM-DD  YYYY-MM-DD
    mainwd   "Jendela utama"
    distazim   "Jarak dan arah"
    nearestWPs   "WP terdekat"
    fromto   "Dari %s ke %s"
    degrees   derajat
    nameWPDistBear   "jarak dan arah"
    nameWPNearest   "WP terdekat"
    inrect   "Dalam kotak"
    forthisWP   "untuk WP ini"
    formappedWPs   "untuk WP yang dipetakan"
    group   Grup
    element   Elemen
    insert   Sisipkan
    joinGR   "Gabungkan Grup"
    TRtoRT   "Konversi dari TR ke RT"
    TRRTnpoints   "Tidak ada point yang bisa disimpan"
    TRTRdispl   "Tampilkan TR sekarang"
    WP   WP
    RT   RT
    TR   TR
    GR   GR
    commrec   "Komunikasi dengan pesawat GPS"
    abort   Batalkan
    ACKs   ACK
    NAKs   NAK
    packets   paket
    unnamed   "(tak diketahui)"
    fromTR    "Dari TR: %s"
    mapload "Gambar ber-georeferens"
    loadmback Buka
    savemback "Simpan info geo-ref"
    chgmback Ganti
    clearmback Kosongkan
    backgrnd "Latar belakang"
    nameMapBkInfo "Info Latar belakang"
    nameMapInfo "Konfirguasi Peta"
    mpbkchg "Ganti Latar belakang"
    mpbkgrcs "Posisi Grid"
    nameImage Gambar
    symbol Simbol
    SYCATgeneral "Penggunaan Umum"
    SYCATland Daratan
    SYCATwater Perairan
    SYCATaviation Penerbangan
    SY1st_aid "P3K"
    SYWP_buoy_white "Pelampung, putih"
    SYWP_dot "WP"
    SYairport "Bandara"
    SYamusement_park "Taman Hiburan"
    SYanchor "Jangkar"
    SYavn_danger "Bahaya"
    SYavn_faf "1st approach fix"
    SYavn_lom "Penanda luar lokal"
    SYavn_map "Titik pendekatan salah"
    SYavn_ndb "Menara ND"
    SYavn_tacan "TACAN"
    SYavn_vor "Jangkauan omni VHF"
    SYavn_vordme "VOR-DME"
    SYavn_vortac "VOR/TACAN"
    SYball "Bola"
    SYbeach "Pantai"
    SYbell "Bel"
    SYboat "Perahu"
    SYboat_ramp "Lompatan perahu"
    SYborder "Lintas batas"
    SYbowling "Boling"
    SYbridge "Jembatan"
    SYbuilding "Bangunan"
    SYbuoy_amber "Pelampung, kuning"
    SYbuoy_black "Pelampung, hitam"
    SYbuoy_blue "Pelampung, biru"
    SYbuoy_green "Pelampung, hijau"
    SYbuoy_green_red "Pelampung, hijau merah"
    SYbuoy_green_white "Pelampung, hijau putih"
    SYbuoy_orange "Pelampung, jingga"
    SYbuoy_red "Pelampung, merah"
    SYbuoy_red_green "Pelampung, merah hijau"
    SYbuoy_red_white "Pelampung, merah putih"
    SYbuoy_violet "Pelampung, ungu"
    SYbuoy_white "Pelampung, putih"
    SYbuoy_white_green "Pelampung, putih hijau"
    SYbuoy_white_red "Pelampung, putih merah"
    SYcamping "Lokasi kemah"
    SYcapitol_city "Kota, metropolitan"
    SYcar "Mobil"
    SYcar_rental "Penyewaan mobil"
    SYcar_repair "Bengkel"
    SYcasino "Kasino"
    SYcastle "Istana"
    SYcemetery "Pekuburan"
    SYchapel "Kapel"
    SYchurch "Gereja"
    SYcircle_x "Tanda X terlingkar"
    SYcivil "Lokasi sipil"
    SYcontrolled "Wilayah terawasi"
    SYcrossing "Persilangan"
    SYdam "Bendungan"
    SYdanger "Bahaya"
    SYdeer "Rusa"
    SYdiamond_green "Berlian, hijau"
    SYdiamond_red "Berlian, merah"
    SYdiver_down_1 "Penyelaman 1"
    SYdiver_down_2 "Penyelaman 2"
    SYdollar "Dolar"
    SYdot "Titik"
    SYdrinking_water "Air minum"
    SYduck "Bebek"
    SYelevation "Ketinggian"
    SYexit_no_serv "Keluar, tanpa layanan"
    SYexit "Keluar"
    SYfactory "Pabrik"
    SYfastfood "Resto cepat saji"
    SYfish "Ikan"
    SYfitness "Fitness"
    SYflag "Bendera"
    SYfreeway "Jalan Negara"
    SYfuel "Pompa Bensin"
    SYfuel_store "Pom Bensin & toko"
    SYgeo_name_land "Nama tempat, daratan"
    SYgeo_name_man "Nama tempat, buatan manusia"
    SYgeo_name_water "Nama tempat, perairan"
    SYglider "Glider"
    SYgolf "Golf"
    SYheliport "Helipad"
    SYhorn "Klakson"
    SYhouse "Rumah"
    SYice_skating "Ice skating"
    SYinfo "Info"
    SYintersection "Persimpangan"
    SYis_highway "Jalan Tol"
    SYknife_fork "Makanan"
    SYladder "Tangga"
    SYlanding "Pendaratan"
    SYlarge_city "Kota, besar"
    SYlarge_exit_ns "Keluar tanpa layanan, besar"
    SYlarge_ramp_int "Ramp int, large"
    SYlevee "Tanggul"
    SYlight "Mercu suar"
    SYlodging "Penebangan kayu"
    SYmany_fish "Daerah ikan"
    SYmany_tracks "Banyak jejak"
    SYmark_x "Tanda, x"
    SYmedium_city "Kota, sedang"
    SYmile_marker "Rambu km"
    SYmilitary "Daerah Militer"
    SYmine "Pertambangan"
    SYMOB "Man over board"
    SYmonument "Monumen"
    SYmountains "Pegunungan"
    SYmovie "Bioskop"
    SYmug "Mug"
    SYmuseum "Museum"
    SYntl_highway "Jalan Negara"
    SYnull "(transparan)"
    SYnull_2 "(void)"
    SYoil_field "Tambang minyak"
    SYparachute "Parasit"
    SYpark "Taman"
    SYparking "Parkir"
    SYpharmacy "Apotik"
    SYphone "Telpon umum"
    SYpicnic "Piknik"
    SYpizza "Pizza"
    SYpolice "Kantor polisi"
    SYpost_office "Kantor pos"
    SYprivate "Daerah pribadi"
    SYradio_beacon "Menara radio"
    SYramp_int "Persimpangan miring"
    SYrestricted "Daerah terbatas"
    SYrestrooms "WC"
    SYRV_park "Tempat parkir karavan"
    SYscenic "Permai"
    SYschool "Sekolah"
    SYseaplane "Pendaratan pesawat terbang air"
    SYshopping_cart "Perbelanjaan"
    SYshort_tower "Menara, rendah"
    SYshowers "Tempat mandi"
    SYskiing "Ski air"
    SYskull "Tengkorak"
    SYsmall_city "Kota, kecil"
    SYsnow_skiing "Ski es"
    SYsoft_field "Daerah tanah labil"
    SYsquare_green "Kotak, hijau"
    SYsquare_red "Kotak, merah"
    SYst_highway "Jalan Propinsi"
    SYstadium "Stadion"
    SYstore "Toko"
    SYstreet_int "Persimpangan jalan"
    SYsummit "Puncak"
    SYswimming "Renang"
    SYtake_off "Tinggal landas"
    SYtall_tower "Menara, tinggi"
    SYtheater Teater
    SYtoll Tol
    SYtow_truck "Truk gandeng"
    SYtraceback "Trace-back"
    SYtracks Jejak
    SYtrail_head "Jalan setapak"
    SYtree "Pohon"
    SYtruck_stop "Pemberhentian truk"
    SYtunnel "Terowongan"
    SYultralight "Ultra ringan"
    SYus_highway "US highway"
    SYweight_station "Jemb. timbang"
    SYwreck "Puing perahu"
    SYzoo "Kebun binatang"
    psvisible "Hanya bagian yang terlihat"
    DISPsymbol "Simbol saja"
    DISPs_name "Simbol & nama"
    DISPs_comment "Simbol & komentar"
    DISPname "Nama saja"
    DISPcomment "Komentar saja"
    dispopt Tampilan
    mapitems "Tampilkan item pada peta"
    nameIC Ikon
    prod Produk
    WPCapac "Kapasitas Waypoint"
    ICCapac "Kapasitas Ikon"
    RTCapac "Kapasitas Rute"
    TRCapac "Kapasitas Trek"
    protcl "Protokol"
    ICGraph "Gambar ikon"
    WPperRT "Waypoint per Rute"
    notinGR "tidak dalam (sub-)grup"
    onlyinGR "hanya dalam (sub-)grup"
    loadgrels "Buka elemen"
    importgrels "Impor elemen"
    about "Tentang GPSMan..."
    contrib "Kontributor"
    errorsto "Laporkan kesalahan program ke:"
    obsTRToRT "WP dibuat dengan konversi TR ke RT"
    nameLists "Lihat daftar"
    nameData "Data"
    MWCmap "Map"
    MWClists "Daftar"
    MWCsingle "Satu jendela"
    search "Cari"
    rmrk "NB"
    closeto "Dekat ke"
    with "Dengan"
    srchres "DITEMUKAN"
    database "Database"
    where "Dimana"
    what "Apa"
    list "daftar"
    distance "Jarak"
    fromWP "dari Waypoint"
    fromPos "dari posisi"
    azimuth "Arah"
    any "semua"
    opening "Awalan"
    suggested "disarankan"
    another "Another"
    srchdd1 "Cari di"
    srchdd2Data "semua item"
    srchdd2GR "Grup"
    from "dari"
    started "dimulai pada"
    transf  "Transf Koord"
    TRNSFAffine    "Affine"
    TRNSFAffineConf  "Aff Conformal"
    TRNSFNoRot      "Conf No Rot"
    projection "Proyeksi"
    lat0  "Lintang bagian tengah"
    long0 "Bujur bagian tengah"
    lat1  "Lintang paralel st 1"
    lat2  "Lintang paralel st 2"
    latF  "Lintang awal semu"
    longF "Bujur awal semu"
    k0 "Faktor sekala"
    NTFzone zona
    PRJUTM "UTM/UPS"
    PRJTM "Transverse Mercator"
    PRJCMP "Portuguese Mil Map"
    PRJBNG "British National Grid"
    PRJBWI "British West Indies"
    PRJITM "Irish Transv Mercator"
    PRJGKK "German Grid"
    PRJLCC1 "Lambert Conic Conf 1"
    PRJLCC2 "Lambert Conic Conf 2"
    PRJKKJP "Basic Finnish Grid"
    PRJKKJY "Uniform Finnish Grid"
    PRJSEG "Swedish Grid"
    PRJMerc1 "Mercator 1"
    PRJMerc2 "Mercator 2"
    PRJCS "Cassini-Soldner"
    PRJAPOLY "American Polyconic"
    PRJStereogr Stereografik
    PRJTWG "Taiwan Grid"
    PRJSOM "Swiss Oblique Mercator"
    PRJLV03 "Swiss LV03 Grid"
    PRJIcG "Iceland Grid"
    PRJRDG "The Netherlands Grid"
    PRJBMN "Grid BMN Austria"
    PRJCTR "Carta Tecnica Reg (I)"
    PRJLamb93 "Lambert 93"
    PRJLambNTFe "NTF IIet"
    PRJLambNTF "NTF"
    NTFzone zona
    dontaskagain "Jangan tanya lagi"
    rename "Buat nama baru"
    oname "Nama asli"
    never "Tidak"
    ask "Tanyakan"
    always "Selalu"
    stage "Tahap"
    label "Label"
    alt "Ketinggian"
    optALTUNIT "Ketinggian"
    locate "Lokasikan"
    animation  Animasi
    fast Cepat
    slow Lambat
    start Mulai
    pause Jeda
    speed Kecepatan
    centred "Tetap di tengah"
    state Keadaan
    animinit "di awal/akhir"
    animon "aktif.."
    animpause "jeda.."
    animabort "membatalkan"
    realtimelog "Log trek real-time"
    garmin Garmin
    nmea "NMEA 0183"
    stext "File teks"
    simul "Simulator"
    lowrance Lowrance
    getlog "Ambil Log"
    stop Henti
    dolog Catat
    show Tampilkan
    hide Sembunyikan
    posfixerror error
    posfix_  ?
    posfix2D 2D
    posfix3D 3D
    posfix2D-diff "2D d"
    posfix3D-diff "3D d"
    posfixGPS GPS
    posfixDGPS DGPS
    posfixAuto ok
    posfixsimul simul
    restart "Ulang Program"
    mkTR "Buat TR"
    PVTflds "# t ltg bjr tgi fix EPE EPH EPV vel_x vel_y vel_z TRK"
    namePVTData "Log data"
    mkavgWP "Buat WP ratarata"
    move Pindahkan
    define Definisikan
    open Buka
    defs "Definisi"
    baseproj "Proyeksi dasar"
    abbrev "Nama singkat"
    grid Grid
    use Gunakan
    unit Satuan
    feasting "Timur semu"
    fnorthing "Utara semua"
    bounds Ikatan
    max Maks
    min Min
    easting Easting
    northing Northing
    fixeddatum "datum tetap"
    elevation Ketinggian
    usewps "Gunakan WPs"
    chgpfrmt "Ganti format posisi"
    changegroupsymbol "Ganti simbol"
    here "Di sini"
    atprevwp "Pada WP sebelumnya"
    prevwp "WP sebelumnya"
    firstwp "WP pertama"
    chglstrs "Edit tahap sebelumnya"
    chgnxtrs "Edit tahap selanjutnya"
    contnend "Tambahkan di akhir"
    closemenu "Tutup menu"
    ellpsd Elipsoid
    datum Datum
    userdefs "Definisikan sendiri"
    edmap "Edit pada peta"
    actual aktual
    rtimelogintv "Log interval"
    inca "Sertakan setelah"
    invert "Balikkan"
    recwindow "Jendela penerimaan"
    volume "Volume"
    latS "L Selatan"
    latN "L Utara"
    longW "B Barat"
    longE "B Timur"
    no Tidak
    computations Penghitungan
    comparea "Hitung luas"
    cluster Kelompok
    centre  Tengah
    mkclusters "Buat kelompok"
    quadr "Segi empat"
    dlat "Rentang garis lintang"
    dlong "Rentang garis bujur"
    collcntr "Mengumpulkan tengah..."
    compclstr "Menghitung kelompok..."
    crtgclstrgrs "Membuat grup..."
    chgdatum "Ganti datum..."
    print Cetak
    prevctr "Sebelumnya"
    printopt "Pilihan pencetakan"
    chgmfsize "Ganti ukuran huruf pada peta"
    numberid "Nomor/Id"
    hiddendata "Data tersembunyi"
    YYYY/MM/DD  YYYY/MM/DD
    cwpsdef "WP kontrol yang akan diisi"
    nextTP "TP selanjutnya"
    generate Buat
    optSERIALBAUD "Baud Rate"
    optDEFTRTWIDTH "Lebar garis RT"
    optDEFTTRWIDTH "Lebar garis TR"
    width Lebar
    TRtoTR   "Penyederhanaan TR"
    TRlinedispl   "Lihat hasil sekarang"
    obsTRsimpl "Hasil TR dari penyederhanaan"
    simplTRto "Sederhanakan menjadi"
    exstglog "Catatan yang ada"
    contnsly "Terus menerus"
    animate "buat animasi"
    animabbrev "anim."
    noanabbr "tanpa anim."
    zelev "skala Z"
    xyelev "skala XY"
    notext "tanpa teks"
    view "Tampilan"
    sideview "Tampak samping"
    persptv "Perspektif"
    SYgeocache "Geocache"
    SYgeocache_fnd "Ditemukan Geocache"
    optMAPCOLOUR,trvtrk "Panah TRK"
    optMAPCOLOUR,trvtrn "Panah TRN"
    optMAPCOLOUR,trvcts "Panah CTS"
    optMAPCOLOUR,trvcts2 "Panah CTS ke-2"
    optMAPCOLOUR,trvvel_z "Panah turun/naik"
    optMAPCOLOUR,trvwrnimportant "Peringatan penting (nav)"
    optMAPCOLOUR,trvwrnnormal "Peringatan (nav)"
    optMAPCOLOUR,trvwrninfo "Informasi (nav)"
    travel Perjalanan
    notravel "Hentikan Perjalanan"
    optTRAVELFONTSIZE "Ukuran huruf pada Perjalanan"
    travdisplay "Konfigurasi tampilan"
    travchgdisplay "Ganti ke tampilan %s"
    travdsetup "Konfigurasi tampilan Perjalanan"
    navMOB "MOB: Man Over Board!"
    startnav "Navigasi"
    navWP "Pergi ke WP"
    goback "Kembali"
    follow "Ikuti %s"
    exactly "sama persis"
    fromnrst "dari yang terdekat"
    inrvrs "kebalikan"
    forgetgoal "Lupakan tujuan"
    suspend "Tunda"
    resume "Lanjutkan"
    TRVfix "Fix"
    TRVhour "Waktu"
    TRVspeed "Kecepatan"
    TRVpos "Pos"
    TRValt "Alt"
    TRVtrk "TRK"
    TRVnxtWP "Ke"
    TRVprvWP "Dari"
    TRVete "ETE"
    TRVeta "ETA"
    TRVvmg "VMG"
    TRVxtk "XTK"
    TRVcts "CTS"
    TRVtrn "TRN"
    TRVvel_z "Kecepatan V"
    TRVtrkcts "TRK, CTS"
    TRVdist "Jarak"
    TRVc_trkcts "Panah TRK/CTS"
    TRVc_trn "Panah TRN"
    TRVc_vel_z "Panah turun/naik"
    add "Tambahkan"
    addlabelled "Tambahkan dengan label"
    remove "Hapus"
    mindist "Jarak kedatangan"
    chginggoal "Mengubah tujuan selanjutnya"
    chggoalhlp "Kapan mengubah tujuan\ndari tujuan saat ini ke tujuan berikutnya\nsaat mengikuti/menelusuri RT/TR"
    soon segera
    late nanti
    warnings "Peringatan"
    dowarn "Peringatan"
    warnconf "Konfigurasi peringatan"
    priority Prioritas
    high penting
    medium biasa
    low "kurang penting"
    warnprox "Jarak ke WP <"
    warnanchor "Jarak ke WP >"
    warnspeed "Kecepatan >"
    warntrn "TRN (abs)>"
    warnvspeed "Kecepatan vertikal"
    warnxtk "XTK (abs)>"
    fntsize "Ukuran huruf"
    trvhlpbox "Gunakan klik-kanan untuk mengubah urutan elemen di bawah ini"
    trvhlpbxs "Gunakan klik-kanan untuk mengubah daftar elemen"
    trvwarrv "Tiba pada %s!"
    trvwleave "Berangkat dari %s!"
    trvwspeed "Kecepatan > %s!"
    trvwtrn "TRN > %s!"
    trvwvspeed "Kec. vertikal bukan pada [%s,%s]!"
    trvwxtk "XTK > %s!"
    trvwnolog "Pencatatan Real-time dimatikan!"
    trvwnopos "Posisi sebelumnya tidak ada"
    trvwuwps "RT memiliki WP tidak jelas"
    trvwchg "Sekarang berangkat ke %s"
    TP TP
    drivesim "simulasi kendara"
    startfrom "Mulai dari..."
    outofctrl "Di luar kontrol!"
    right Kanan
    left Kiri
    straight Lurus
    rthlpdsim "Tombol panah: mengubah arah, mengatur kecepatan\nSpace bar: lurus"
    chggoal "Ganti ke tujuan berikutnya"
    Ghidden_class Kelas
    Ghidden_subclass Sub-kelas
    Ghidden_lnk_ident "ID Tahapan"
    Ghidden_colour Warna
    Ghidden_attrs Atribut
    Ghidden_depth Kedalaman
    Ghidden_state Propinsi
    Ghidden_country Negara
    Ghidden_facility Fasilitas
    Ghidden_city Kota
    Ghidden_addr Alamat
    Ghidden_int_road "Simpang jalan"
    Ghidden_dtyp "Tampilkan opt+jenis"
    Ghidden_ete "ETE"
    Ghidden_display "Tampilkan?"
    Ghidden_yes Ya
    Ghidden_no Tidak
    Ghidden_user "User"
    Ghidden_user_symbol "User (simbol saja)"
    Ghidden_non_user "Non-user"
    Ghidden_avn_airport "Bandara"
    Ghidden_avn_inters "Simpang Avn"
    Ghidden_avn_NDB "NDB"
    Ghidden_avn_VOR "VOR"
    Ghidden_avn_airp_rway "Wilayah landas pacu"
    Ghidden_avn_airp_int "Simpangan bandara"
    Ghidden_avn_airp_NDB "NDB bandara"
    Ghidden_map_pt "Titik"
    Ghidden_map_area "Wilayah"
    Ghidden_map_int "Persimpangan"
    Ghidden_map_addr "Alamat"
    Ghidden_map_line "Garis"
    Ghidden_locked Terkunci
    Ghidden_default "Default"
    Ghidden_black Hitam
    Ghidden_white Putih
    Ghidden_red Merah
    Ghidden_dark_red "Merah tua"
    Ghidden_green Hijau
    Ghidden_dark_green "Hijau tua"
    Ghidden_blue Biru
    Ghidden_dark_blue "Biru tua"
    Ghidden_yellow Kuning
    Ghidden_dark_yellow "Kuning tua"
    Ghidden_magenta Ungu
    Ghidden_dark_magenta "Ungu gelap"
    Ghidden_cyan "Biru cerah"
    Ghidden_dark_cyan "Biru laut"
    Ghidden_light_gray "Abu-abu muda"
    Ghidden_dark_gray "Abu-abu gelap"
    Ghidden_line Garis
    Ghidden_link Tahap
    Ghidden_net Jaringan
    Ghidden_direct Langsung
    Ghidden_snap Snap
    forgetGRcs "Abaikan GR&Els"
    renres "SUDAH DIGANTI NAMA"
    undo "Gak jadi"
    UTMzone zona
    tfwfile  "File TFW"
    ok Ok
    newWPatdb "WP baru pada..."
    opinprogr "Operasi dalam proses"
    working "Bekerja"
    aborted "Gugurkan!"
    errwarn "Kesalahan/peringatan!"
    SYbiker "Peng. motor"
    SYbox_blue "Kotak, biru"
    SYbox_green "Kotak, hijau"
    SYbox_red "Kotak, merah"
    SYflag_pin_blue "Pin bendera, biru"
    SYflag_pin_green "Pin bendera, hijau"
    SYflag_pin_red "Pin bendera, merah"
    SYhouse_2 "Rumah 2"
    SYpin_blue "Pin, biru"
    SYpin_green "Pin, hijau"
    SYpin_red "Pin, merah"
    ozimapfile "File Ozi Map"
    info "Informasi"
    BGAfeature  "Fitur BGA"
    BGAfindblty "Ketercarian BGA"
    BGAairact   "Aktivitas Udara BGA"
    optUSESLOWOPWINDOW "Jendela untuk kontrol operasi lambat"
    optDEFTRECPROTOCOL "Protokol default"
    optLNSREACT "Reaksi LN thd mouse"
    syhlpbx "Gunakan klik-kanan untuk\nmengubah daftar elemen"
    lstsqsfile "File Least Squares"
    totdstng   "Jarak total, tanpa gap"
    tottimeng  "Waktu total, tanpa gap"
    SYcross "Tanda plus"
    SYcross_3p "Tanda plus 3p"
    mapfitWPs "Tampilkan WP yang masuk peta"
    showfitinfo "Tampilkan informasi yang masuk peta"
    rmsxydev "rms(deviasi x,y)"
    resstderr "std error residual"
    chgdev "Ganti alat"
    maxalt "Ketinggian maksimal"
    minalt "Ketinggian minimal"
    alt_cumula "Ketinggian pendakian kumulatif"
    alt_cumuld "Ketinggin penurunan kumulatif"
    optSHOWFILEITEMS "Secara default tampilkan pada item peta yang dibaca"
    vertgridlines "Garis Grid Vert"
    convert  "Konversi"
    split    "Pisahkan"
    bysel  "menurut titik terpilih"
    byseg  "menurut segmen"
    openits "Buka %s"
}

    # the following definitions must be coherent with $TXT
array set INVTXT {
    DMS   DMS
    DMM   DMM
    DDD   DDD
    Tingkat   GRA
    UTM/UPS   UTM/UPS
    MH   MH
    WP   WP
    RT   RT
    TR   TR
    GR   GR
}

# changes by Miguel Filgueiras to RM contribution
set TXT(srChainage) $TXT(totdst)
set TXT(srShowRest) $TXT(gpSRests)
set TXT(gpSym) $TXT(symbol)
set TXT(srRest) $TXT(gpRest)
