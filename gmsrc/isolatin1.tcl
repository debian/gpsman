#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: isolatin1.tcl
#  Last change:  6 October 2013
#

### ISO Latin1 characters; mainly from procs written by Lu�s Damas

# consulting this file may cause problems in non-western Linux installations

proc ISOBindings {w} {
    # set bindings to force isolatin1 character interpretation
    global tcl_platform

    if { $tcl_platform(platform) == "windows" } {
	# no dead keys in MS-Windows, it seems
	set binds {{asciitilde ~ {a �} {c �} {n �} {o �}
	                         {A �} {C �} {N �} {O �}}
	           {quoteright ' {a �} {e �} {i �} {o �} {u �}
		                 {A �} {E �} {I �} {O �} {U �}}
	           {asciicircum ^ {a �} {e �} {o �}}
	           {quoteleft ` {a �} {e �}}}
    } else {
	# not supported:
	#    dead_diaresis
	set binds {{asciitilde ~ {a �} {c �} {n �} {o �} {asciitilde �}
	                         {A �} {C �} {N �} {O �}}
		   {dead_cedilla � {c �} {C �}}
	           {dead_tilde ~ {a �} {c �} {n �} {o �} {dead_tilde �}
	                         {A �} {C �} {N �} {O �}}
		   {quoteright ' {a �} {c �} {e �} {i �} {o �} {u �}
		                 {A �} {C �} {E �} {I �} {O �} {U �}}
		   {dead_acute ' {a �} {e �} {i �} {o �} {u �}
		                 {A �} {E �} {I �} {O �} {U �}}
	           {apostrophe ' {a �} {e �} {i �} {o �} {u �}
		                 {A �} {E �} {I �} {O �} {U �}}
		   {dead_circumflex ^ {A �} {a �} {E �} {e �} {I �}
                                 {i �} {O �} {o �} {U �} {u �}}
	           {asciicircum ^ {A �} {a �} {E �} {e �} {I �}
                                 {i �} {O �} {o �} {U �} {u �}}
		   {grave ` {A �} {a �} {E �} {e �} {I �} {i �} {O �} {o �}
                            {U �} {u �}}
	           {dead_grave ` {A �} {a �} {E �} {e �} {I �} {i �}
		            {O �} {o �} {U �} {u �}}
		   {quotedbl \\\" {a �} {e �} {i �} {o �} {u �} {s �}
                                {quotedbl �}
  		                {A �} {E �} {I �} {O �} {U �}}
		   {slash / {a �} {e �} {o �} {A �} {E �} {O �}}
			   }
    }
    foreach k $binds {
	set keysym [lindex $k 0]
	bind $w <Key-$keysym> "$w insert insert [lindex $k 1] ; break"
	foreach p [lrange $k 2 end] {
	    bind $w <Key-$keysym><Key-[lindex $p 0]> \
		    "ReplaceChar $w [lindex $p 1] ; break"
	}
    }
    return
}

proc ReplaceChar {w char} {
    # replace last char in entry or text widget $w by $char

    switch [winfo class $w] {
	Entry {
	    if { [set ix [expr [$w index insert]-1]] < 0 } { return }
	    $w delete $ix
	    $w insert insert $char
	}
	Text {
	    if { ! [regexp {^([0-9]+)\.([0-9]+)$} [$w index insert] a l c] || \
		$c == 0 } {
		return
	    }
	    $w delete $l.[expr $c-1]
	    $w insert insert $char	    
	}
    }
    return
}




