#
#  Russian support for gpsman
#
#  Copyright (c) 2007 Nikolai Kosyakov ()
#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: langru.tcl
#  Last change:  6 October 2013
# Russian language file by Nikolai Kosyakov, 21 October 2007
#
# Support by Alexander B. Preobrazhenskiy (modul_AT_ihome.ru) since October 2009
#  last input from him inserted:  27 December 2009
#

# file langengl.tcl is consulted first; no need for duplicating entries here

# only 3 chars long names; check also ALLMONTH in file i18n-utf8.tcl
set MONTHNAMES "Янв Фев Мар Апр Май Июл Июн Авг Сен Окт Ноя Дек"

set DLUNIT(KM,dist) км
set DLUNIT(KM,subdist) м
set DLUNIT(KM,speed) "км/ч"
set DLUNIT(KM,area) "кв.км"
set DLUNIT(NAUTMILE,dist) "м.миля"
set DLUNIT(NAUTMILE,subdist) фут
set DLUNIT(NAUTMILE,speed) узел 
set DLUNIT(NAUTMILE,area) "кв м.миля"
set DLUNIT(STATMILE,dist) миля
set DLUNIT(STATMILE,subdist) фут
set DLUNIT(STATMILE,speed) миль/ч

set DLUNIT(M,dist) $DLUNIT(KM,subdist)
set DLUNIT(FT,dist) $DLUNIT(STATMILE,subdist)

set DTUNIT $DLUNIT($DISTUNIT,dist)
set SPUNIT $DLUNIT($DISTUNIT,speed)
set ARUNIT $DLUNIT($DISTUNIT,area)
set ALUNIT $DLUNIT($ALTUNIT,dist)

set MESS(RTcomp) "#\tПТ\t\t$DTUNIT\tгр\t$ALUNIT"
set MESS(TRcomp) "#\t\t\t\t$ALUNIT\t$DTUNIT\t$DTUNIT\tч:м:с\t$SPUNIT\tгр"
set MESS(WPNearest) "ПТ\t\t$DTUNIT\tгр"

array set MESS {
    badscale "Неверное значение масштаба карты"
    oktoexit "Да, выйти (не сохранённые данные будут утрачены)"
    okclrmap "Да, очистить карту"
    namelgth "Имя не должно быть больше %d символов"
    namelgth "Комментарий не должен быть больше %d символов"
    namevoid "Имя не может быть пустым"
    baddate  "Недопустимая дата; строка"
    badhdg   "Недопустимое начало %s: должно быть %s или +/-"
    badcoord "Недопустимые границы координат или формат: \"%s\" должен быть %s"
    outofrng "Число выходит за допустимые границы"
    UTMZN    "Должна быть буква из интервалов A..H, J..N, или P..Z"
    badstrg  "Недопустимый символ или символы"
    strgvoid "Строка не может быть пустой"
    nan      "\"%s\" не число"
    written  "Записано"
    undefWP  "Маршрут %s содержит не определённые WP точки; не сохранён/не экспортирован"
    bigRT    "Маршрут %s > максимального номера; не экспортирован"
    unkndatum   "Неизвестный датум"
    badcommdWP  "Недопустимая команда при загрузке ПТ; строка"
    notabsWP    "Недопустимое определение ПТ точки; нет табуляций, строка"
    undefinedWP   "Ссылка на неизвестную ПТ-точку: %s"
    undefinedTR   "Неизвестный ТР"
    nofieldsWP   "Недопустимая ПТ, недостаточно полей; строка"
    excfieldsWP  "Недопустимая ПТ, слишком много полей; строка"
    badWPsRT   "МШ с недопустимым списком путевых точек; строка"
    toomuchWPs   "Внимание: в МШ ПТ больше %d "
    badcommdTP   "Недопустимая команда при загрузке ТТ; строка"
    badcommdLP   "Недопустимая команда при загрузке ЛТ; строка"
    badTP   "Недопустимая точка в ТР; строка"
    badTPsTR   "ТР с неверным списком точек трека; строка"
    badLP   "Недопустимая точка в ЛН; строка"
    badLPsLN   "ЛН с неверным списком точек трека; строка"
    noheader   "Отсутствует стандартный заголовок файла"
    loaderr   "Ошибка загрузки из файла; строка"
    unkncommd   "Недопустимая команда при загрузке из файла; строка"
    noformat   "В начале файла отсутствует Форматная строка"
    badformat   "Недопустимый формат строки"
    badRT   "Некорректное определение МШ; строка"
    badRTargs   "Недопустимые аргументы МШ; строка"
    badTR   "Некорректное определение ТР; строка"
    fileact   "%s %s файла?"
    filexists   "Файл существует!"
    GPSok   "Успешное соединение"
    toomany   "Слишком много %s (> %d)"
    cantfgt   "Не могу удалить %s: принадрежит картированному элементу"
    cantfgted   "Не могу удалить %s: редактируется"
    cantmapRTunkn   "Маршрут не картирован; отсутствует информация по ПТ"
    cantmapRTed   "Маршрут не картирован; ПТ редактируется"
    cantrun   "Не могу выполнить"
    inprogr   "Операция уже выполняется"
    cantread   "Не могу прочитать дополнительный файл"
    cantwrtopt   "Не могу записать файл настроек"
    voidRT   "Маршрут не содержит путевых точек"
    activeRT   "Активен маршрут 0; продолжить?"
    voidTR   "Трек не содержит точек"
    voidLN   "Полилиния не содержит точек"
    idinuse   "Идентификатор уже используется"
    cantunmap   "Не могу убрать с карты %s: принадлежит картированному элементу"
    askrevert   "Вернуться к исходным значениям?"
    askforget   "Удалить %s?"
    notimpl   "Незавершено"
    forgetall   "Удалить все элементы в списке %s ?"
    counted   "Определено %d %s"
    notlisted   "Элемент отсутствует в списке"
    wrgval   "Недопустимое значение для"
    voidGR   "В группе нет элементов"
    initselfGR   "Группа содержит себя саму"
    GRelout   "Команда для элемента вне определения ГР; строка"
    badGRels   "Группа с недопустимыми элементами списка; строка"
    badcommdGRel   "Недопустимая команда при загрузке элементов ГР; строка"
    notypeforGRel   "Элемент без типа; строка"
    badGRel   "Недопустимое определение элемента; строка"
    check   "Проверка соединения"
    toomanyerr   "Слишком много ошибок; операция прервана..."
    getWP   "Чтение ПТочек"
    getRT   "Чтение Маршрутов"
    getTR   "Чтение Треков"
    getLAP  "Чтение Кругов"
    getAL   "Чтение альманаха"
    putWP   "Запись ПТочек"
    putRT   "Запись Маршрутов"
    putTR   "Запись Треков"
    fillbuffWP  "Загрузка буфера ПТ"
    fillbuffRT  "Загрузка буфера МТ"
    noACKNAK  "При ожидании ACK/NAK получен пакет данных; проверьте соединение с GPS-приёмником"
    badplatform   "На данной платформе невозможен ввод/вывод через последовательный порт"
    badserial "Не могу соединиться с %s"
    nodata    "Нет %s в приёмнике"
    badimage "Не корректный графический файл"
    mapadjust "Пожалуйста разместите на карте ПТ; щёлкните на Ok, когда закончите"
    duplicate "Объект %s уже выбран"
    clrcurrmap "Очистить текущую карту?"
    mbkbaddatum "Датум отсутствует или неизвестен"
    mbkbadscale "Масштаб не может быть отрицательным"
    mbkbadat "Недопустимые аргументы"
    edityourrisk "Редактируйте на свою ответственность!"
    okclrbkmap   "Ok для очистки фона карты"
    okclrbkim    "Ok для очистки изображения"
    badattrval "У дополнительного поля %s неверное значение: %s"
    badattr "У дополнительного поля %s неизвестное имя: %s"
    badSYMBOLcode "Недопустимый код символа"
    badDISPOPTcode "Недопустимый код настройки дисплея"
    goingdown "Предварительные настройки сохранены; пожалуйста перезапустите программу"
    putwhat  "Какие типы элементов сохранить?"
    readwhat "Какие типы элементов прочитать?"
    noWPinx "Слишком много ПТ для приёмника"
    noICinx "Слишком много IC (иконок) для приёмника"
    getIC "Чтение Иконок"
    serial1 "Некорректная Преамбула Заголовка"
    serial2 "Возвращена некорректная Команда"
    checksum1 "Некорректная Контрольная сумма Заголовка"
    checksum2 "Ошибка Контрольной суммы данных"
    receiver "Не соединен с прибором!"
    importonly1 "Могу проимпортировать только 1 вид данных"
    exportonly1 "Могу проэкспортировать только 1 вид данных"
    outdatedprefs "Файл предварительных настроек устарел; пожалуйста проверьте его сейчас"
    mustchoose1type "Должен быть выбран хотя бы 1 вид элементов"
    nosuchitems "Нет элементов, удовлетворяющих описанию"
    resultsin "Поиск результатов в Группе"
    badWP "ПТ не получена или не определена"
    badangle "Угол должен быть в пределах от 0 до 360 градусов"
    georefhow  "Метод геодезической привязки"
    cantsolve  "Не могу решить уравнения"
    transfcantscale "При текущем преобразовании масштабирование невозможно"
    oldfilefmt "Старый формат файла; пожалуйста сохраните его в новом формате!"
    unknProj "Неизвестная проекция карты"
    unknTransf "Неизвестное преобразование карты"
    badProjargs "Неверные аргументы для проекции"
    badTransfargs "Неверные аргументы для преобразования"
    badfield "Не корректная пара атрибут=значение"
    badattr "Неверное имя атрибута"
    missattrs "Отсутствуют атрибут/атрибуты"
    mbkbadproj "Некорректная проекция карты"
    mbkbadtransf "Некорректное преобразование карты"
    notUTMproj "Информация по UTM сохранена; переходим к использованию новых параметров проекции"
    projchg    "Пожалуйста подтвердите параметры проекции"
    badparam "Недопустимое значение для Bad value for %s"
    connectedto "Подсоединён к %s"
    recnotsuppd "Данная модель приёмника не поддерживается"
    gotprots "Описание протокола получено"
    badprots "Некорректное описание протокола"
    defprots "Использование таблицы протоколов"
    nohidden "Отказаться от скрытых данных?"
    badRS "Часть маршрута за границами из описания МШ; строка"
    badWPsRSs "Часть маршрута до 1-й или после последней ПТ; строка"
    windowdestr "Окно уничтожено!"
    badhidden "Недопустимый формат скрытого значения"
    replname "Замените \"%s\" на имя, содержащее не более чем %d, из следующих символов: %s"
    badalt "Недопустимое значение высоты"
    baddistunit "Недопустимая единица длины в файле настроек для масштаба карты"
    badgridzone "Недопустимая зона для системы координат"
    outofgrid "Положение вне допустимых значений для системы координат"
    timeoffnotint "Временное смещение должен быть целочисленным значением, либо оканчиваться на .5"
    cantchkprot "Невозможно проверить при текущем протоколе"
    mustconn1st "Проверьте сначала соединение с проёмником"
    rltmnotsupp "Этот приёмник не поддерживает запись журнала в реальном времени"
    createdir "Пожалуйста создайте каталог %s и перезапустите программу"
    oktomkdir "Создаём каталог %s?"
    projnameabbr "Пожалуйста дайте новой проекции имя и сокращённое имя"
    abbrevinuse "Сокращённое имя уже определено"
    nameinuse "Имя уже определено"
    projinuse "Проекция используется; изменения будут отменены"
    gridneedsdatum "Некорректное определение системы координат %s; не введён датум"
    badgriddatum "Датум для системы координат %s должен быть %s"
    cantchggriddatum "Для этой системы координат необходим датум %s"
    gridinuse "Нельзя удалить систему координат. Она используется %s; продолжить?"
    gridinusenochg "Эта система координат используется %s; нельзя произвести изменения"
    cantwrtprgr "Невозможно записать файл проекций пользователя"
    cantwrtdtel "Невозможно записать файл данных пользователя"
    movingWP "Разместите %s нажатием левой кнопки мыши\nОтказ правой кнопкой"
    missingdata "Недостаточно данных!"
    needs1wp "Маршрут должен содержать хотя бы одну путевую точку"
    emptypos "Настройка отображения положения на карте с незаполнеными полями"
    cantwrtsstate "Не могу записать файл сохранения состояния системы: %s"
    cantrdsstate "Не могу прочитать файл сохранения состояния системы: %s"
    corruptsstate "Нарушенный файл сохранения состояния системы: %s"
    editrisk "Редактируйте под свою ответственность!"
    savestate "Сохранить текущее состояние системы?"
    delsstate "Удалить файл сохраненного состояния системы?"
    badmapinfo "Недопустимый файл параметров карты"
    badMHloc "Неверный Maidenhead locator (сист.коорд. для радиолюбителей)"
    areais "Площадь полигона (без самопересечений) равнв %.3f%s"
    areatoosmall "Площадь слишком мала (<%s кв.км)"
    projarea "Расчёт площади в проекции"
    selfintsct "Повторяющиеся путевые точки: МШ не может самопересекаться!"
    badinvmdist "Апроксимационная ошибка при инвертировании меридионального расстояния"
    badinvproj "Апроксимационная ошибка при инвертировании проекции %s"
    negdlatlong "Широта/долгота не может быть отрицительной!"
    allundef "Отсутствуют описания для ПТ в ГР"
    badfloats "Преобразования с правающей точкой работают не корректно; Вы уверены, что хотите подключиться?"
    noprintcmd "Отсутствует команда печати; включите её в настройки"
    cantexecasroot "Нельзя запускать GPSMan от root-а"
    badargtofunc "Недопустимый аргумент для функции %s"
    redefproj "Определённая пользователем проекция %s замещает ранее определённую с тем же коротким именем; пожалуйста измените Ваше описание!"
    couldntcd "Неудачная попытка изменить каталог на %s"
    shpext "Недопустимое расширение %s; разрешены - .shp, .shx, .dbf?"
    shpcntopen "Не могу создать/открыть файлы формата Shape"
    shpcntwrtfs "Не могу создать .dbf поля для Shape файла"
    shpoutmem "Недостаточно памяти!"
    shpemptyfile "Пустой файл"
    shpwrongfile "Формат файла не Shape "
    shplessdim "Размерность в Shape файле меньше требуемой; продолжаем?"
    shpbadWPinRT "%d-ая ПТ с некорректными координатами в МШ %s проигнорирована"
    errorGTMread "Ошибка во время чтения GTrackMaker файла"
    badGTMvers "Неподдерживаемая версия GTrackMaker файла"
    badGTMfile "Некорректная начальная строка в GTrackMaker файле"
    badGTMcounts "Отрицательный счётчик/счётчики в GTrackMaker файле"
    badGTMlat "Широта вне допустимых значений  в GTrackMaker файле"
    badGTMlong "Долгота вне допустимых значений  в GTrackMaker файле"
    badGTMdatum "Некорректный датум в GTrackMaker файле"
    unobscmap "Возможно окно или иконка за границей карты; попытайтесь снова после её удаления?"
    cantwrtimg "Ошибка записи изображения в файл формата %s"
    cantsaveRTid "%d МШ не сохранён: не числовой идентификатор"
    cantsaveTRid "%d ТР не сохранён: не числовой идентификатор"
    badtrvconf "Испорченная конфигурация; перезапуск с пустой конфигурацией"
    drvsimoff "Симулятор вождения: ещё не запущен!"
    needWP "Симулятор вождения: пожалуйста сначала загрузите или определите путевые точки"
    chgrecprot "Пожалуйста измените протокол приёмника"
    clrtrvlog "Очистить журнал путешествия?"
    frgetGRcs "удалить группу со всеми элементами?!"
    nmeainuse "Текущие данные принимаются в реальном времени или идёт чтение NMEA файла"
    badfile "Ошибка чтения из файла"
    RTnoWPname "В маршрутах МШ более не поддерживаются ПТ, определяемые только по имени"
    distlarge "Расстояние слишком большое!"
    convres "Результат конвертирования %s сохранён под именем %s"
    outrngproj "Точка за допустимыми пределами для проекции!"
    badconv "Не могу преобразовать значение для параметра %s: %s"
    badprmval "Некорректное преобразованное значение для параметра %s: %s"
    timeconsmg "Слишком длительная операция: продолжаем?"
    badtimeval "Некорректное значение времени"
    badLAP "Некорректный КРУГ; строка"
    lapncnsdrd "Этот КРУГ не учитывается"
    emptymenu "Пустое меню; продолжить?"
    cantwrtsymenu "Не могу записать файл меню знаков"
    cantwrtmbaks "Не могу записать фоновый файл для карты пользователя"
    abbrevhasspaces "Короткое имя не может быть пустым"
    needNpoints "Вы должны указать не менее %s точек!"
    samelgth "Обе строки должны иметь одинаковое кол-во символов"
    rschkargs "Функция regsub завершилась с ошибкой, проверьте аргументы"
    emptyrmeth "Метод переименования не может быть пустым"
    cantwrtdefs "Невозможно записать файл пользовательских настроек"
    xcantbey "%s не может быть %s"
    mustselftfam "Выберите группу шрифтов"
    badpluginwhere "Неверный агрумент плугина \"where\"; должен быть списком"
    badpluginparam "Неверные параметры плугина: список NAME EXPR, где NAME должно начинаться с _"
    badpluginparamexec "При запуске плугина %s: неверный параметр %s"
    pluginfailed "Плугин %s завершился ошибкой: %s"
    canwrtaux "Невозможно записать вспомогательный файл"
    twotimeoffsets "Различные временные зоны в файле"
    notimeoffset "Не указана часовой пояс, принимается 0"
    baddateas "Плохая дата: %s"
    unknownenc "Неизвестная кодировка символа %s"
    chgbaudto "Смена скорости обмена с прибором на %s; подождите..."
    baudchgfailed "Невозможно сменить скорость порта"
    busytrylater "Выполняются другие операции, попробуйте позже"
    obssplit "Результат разбиения %s, названного \"%s\""
}

set TXT(RTcompflds) "# ПТ {$DTUNIT} гр {$ALUNIT} метка этапа"
set TXT(TRcompflds) \
    "TP {} {} {} {$ALUNIT} {$DTUNIT} {$DTUNIT} ч:м:с {$SPUNIT} deg"
set TXT(starttoend) "Начало: до конца %s $DTUNIT;"
set TXT(startmax) "макс. %s $DTUNIT;"
set TXT(WPnearflds) "Путевая точка {$DTUNIT} гр"
set TXT(within) "Без (${DTUNIT}s)"
set TXT(between) "Между (${DTUNIT})"

array set TXT {
    GMtit   "GPS Manager - версия"
    exit   Выход
    map   Карта
    load   Загрузить
    loadfrm   "Загрузка из "
    save   Сохранить
    saveels "Сохранить элементы"
    saveto   "Сохранить в "
    clear   Очистить
    clearall   "Очистить Всё"
    newWP   "Новая ПТ"
    newRT   "Новый МР"
    newTR   "Новый ТР"
    newLN   "Новая ЛН"
    newGR   "Новая ГР"
    import   Импорт
    importfrm   "Импорт из "
    export   Экспорт
    exportels "Экспорт Элементов"
    exportto   "Экспорт в "
    count   Количество
    trueN   "Действительный север"
    automagn   "Auto Magnetic"
    usrdef   "Определённое пользователем"
    nameWP   ПТ
    nameRT   МР
    nameTR   ТР
    nameLN   ЛН
    nameLP   ТЛ
    nameGR   ГР
    namePlot      Чертёж
    nameMap       Карта
    nameRTComp   "Обсчёт Маршрута"
    nameTRComp   "Обсчёт Трека"
    nameInfo Информация
    GPSrec   "GPS приёмн"
    turnoff  "Выкл"
    get   Получить
    put   Передать
    all   Все
    select    Выбрать
    selection Выбор
    options   Настройки
    DMS   DMS
    DMM   DMM
    DDD   DDD
    GRA   Град
    UTM/UPS   UTM/UPS
    MH    MH
    message  Сообщение
    cancel   Отменить
    file     Файл
    ovwrt    Перезаписать
    app      Добавить
    online   online
    offline  автоном.
    check    проверка
    create   Создать
    revert   Обратить
    colour   цвет
    Colour   Цвет
    grey     серый
    mono     монохр
    portr    портрет
    landsc   ландшафт
    legend   Легенда
    incscale "Добавить шкалу"
    more     далее
    waypoint "Путевая точка"
    name     Имя
    created  Создано
    cmmt     Комментарий
    withWP  "С данной WP:"
    displ   "Показать на карте"
    startRT "Начать маршрут МШ"
    route    Маршрут
    number   Номер
    numberid "Номер/Идентификатор"
    insb     "Вставить перед"
    insa     "Вставить после"
    del      Удалить
    repl     "Заменить на"
    comp     Вычислить
    RTcomp   "Обсчёт маршрута"
    savecomp "Сохранить расчёты"
    totdst   "Общее расстояние"
    tottime  "Суммарное время"
    track    Трек
    chophd   "Укоротить начало"
    choptl   "Укоротить окончание"
    incb     "Добавить перед"
    date     Дата
    newdate  "Новая дата для следующей точки"
    endprTR  "Окончание предыдущего трека"
    begnxt   "Начало для следующего"
    date1st  "Дата для первой следующей точки"
    TRcomp   "Обсчёт трека"
    avgsp    "Средн скорость"
    maxsp    "Мин скорость"
    minsp    "Макс скорость"
    lat   Шир
    long  Долг
    ze    ZE
    zn    ZN
    eastng   Восточнее
    nrthng   Севернее
    zone     Зона
    change   Изменить
    forget   Забыть
    others   Другие
    opt_Interf        "Пользовательский интерфейс"
    optLANG           Язык
    optISOLATIN1      "Используемые символы"
    optDELETE         "DEL удалить последний символ"
    optMWINDOWSCONF   "Главное окно"
    optSERIALBAUD     "Скорость последовательного порта"
    optGPSREC         "GPS Модель"
    opt_GPSRecConf    "Параметры приёмника"
    optACCEPTALLCHARS "Разрешить все символы"
    optNAMELENGTH     "Максимальная длина имени"
    optINTERVAL       "Интервал для образца"
    optCOMMENTLENGTH  "Максимальная длина комментария"
    optMAXWPOINTS     "Максимальное кол-во путевых точек"
    optMAXROUTES      "Максимальное кол-во маршрутов"
    optMAXWPINROUTE   "Максимальное кол-во точек в маршруте"
    optMAXTPOINTS     "Максимальное кол-во точек в треке"
    optCREATIONDATE   "Запить содержит дату создания"
    optNOLOWERCASE     "В записи нет прописных букв"
    optDEFAULTSYMBOL   "Символ по умолчанию для точки пути"
    optDEFAULTDISPOPT  "Настройки по умолчанию для показа точек пути WP"
    opt_Data           "Данные"
    optEQNAMEDATA      "Данные с совпадающим именем"
    optKEEPHIDDEN      "Сохранить скрытые данные"
    optDatum            Данные
    optTimeOffset      "Сдвиг по времени"
    optACCFORMULAE     "Формула точности"
    optASKPROJPARAMS   "Подтверждение правильности параметров проекции"
    optBalloonHelp     "Облоко помощи"
    optTRNUMBERINTVL   "Показать на карте информацию о каждой точке трека"
    optTRINFO          "Отображаемая информация о точке трека TP"
    opt_Formats        "Единицы и форматы"
    optDISTUNIT        "Расстояние"
    optALTUNIT         "Высота"
    optUSESLOWOPWINDOW "Окно контроля за медленными операциями"
    optDEFTRECPROTOCOL "Протокол по умолчанию"
    optLNSREACT        "Линии на карте реагируют на мышь"
    M     м
    KM    км
    NAUTMILE    "морская миля"
    STATMILE    "миля"
    FT    фут
    optPositionFormat   "Формат положения"
    optDateFormat   "Формат даты"
    opt_Geom   "Геометрия окна"
    opt_MapGeom   "Геометрия карты"
    optMAPWIDTH   "Ширина карты"
    optMAPHEIGHT   "Высота карты"
    optMAPSCLENGTH   "Длина масштаба карты"
    optMAPSCALE   "Масштаб карты"
    optMAXMENUITEMS   "Максимальное кол-во элементов меню"
    optLPOSX   "Списки X-положений окна"
    optLPOSY   "Списки Y-положений окна"
    optMPOSX   "X-положение окна карты"
    optMPOSY   "Y-положение окна карты"
    optRPOSX   "X-положение окна Записей"
    optRPOSY   "Y-положение окна Записей"
    optEPOSX   "X-положение окна Ошибок"
    optEPOSY   "Y-положение окна Ошибок"
    optDPOSX   "X-положение Диалога"
    optDPOSY   "Y-положение Диалога"
    optDPOSRTMAP "Смещение диалога Маршрут/карта"
    optLISTWIDTH   "Ширина списка"
    optLISTHEIGHT   "Высота списка"
    optCOLOUR   Цвета
    optCOLOUR,fg   "Передний план"
    optCOLOUR,bg   Фон
    optCOLOUR,messbg   "Фон сообщ об ошибке"
    optCOLOUR,confbg   "Фон подтверждения"
    optCOLOUR,selbg    "Фон выбора"
    optCOLOUR,dialbg   "Фон поля ввода"
    optCOLOUR,offline  "Приёмник неподключен"
    optCOLOUR,online   "Приёмник подключен"
    optCOLOUR,check    "Установленная кнопка-флажёк"
    optCOLOUR,ballbg   "Фон облака помощи"
    optCOLOUR,ballfg   "Текст облака помощи"
    optMAPCOLOUR       "Цвета карты"
    optMAPCOLOUR,mapsel   "Выбранный элемент карты"
    optMAPCOLOUR,WP    "Путевые точки на карте"
    optMAPCOLOUR,RT    "Маршруты на карте"
    optMAPCOLOUR,mkRT  "Маршрут, определённый на карте"
    optMAPCOLOUR,TR    "Треки на карте"
    optMAPCOLOUR,TP    "Точки треков на карте"
    optMAPCOLOUR,LN    "Линии на карте"
    optMAPCOLOUR,mapleg   "Легенда"
    optMAPCOLOUR,anim  "Анимация на карте"
    optMAPCOLOUR,emptygrid  "Пустое изображение"
    optMAPCOLOUR,fullgrid   "Существующее изображение"
    opt_Fonts "Шрифты"
    optDEFTRTWIDTH     "Ширина линии для МР"
    optDEFTTRWIDTH     "Ширина линии для ТР"
    optDEFTLNWIDTH     "Щирина линии ЛН"
    opt_Files "Прибор и файлы"
    optDEFSPORT "Прибор"
    optSAVESTATE       "Сохранить при выходе состояние"
    optDELSTATE        "Удалить файлы после восстановления состояния"
    optPERMS           "Права на файл"
    optPRINTCMD        "Комманда на печать"
    optPAPERSIZE       "Размер бумаги"
    optMapGuideVersion "Версия MapGuide"
    optICONSIZE        "Размер иконки"
    red                Красный
    green              Зелёный
    blue               Голубой
    owner       Владелец
    permgroup   Группа
    others      Остальные
    fread       Чтение
    fwrite      Запись
    fexec       Исп
    YYYYMMDD   YYYYMMDD
    MMDDYYYY   MMDDYYYY
    DDMMMYYYY   DDMMMYYYY
    YYYY-MM-DD  YYYY-MM-DD
    YYYY/MM/DD  YYYY/MM/DD
    ISO8601     "ISO8601"
    mainwd      "Главное окно"
    distazim    "Расст. и азимут"
    nearestWPs  "Ближайшие ПТ"
    fromto      "От %s до %s"
    degrees      градусы
    nameWPDistBear   "расст. и азимут"
    nameWPNearest   "ближайшие ПТ"
    inrect      "В прямоугольник"
    forthisWP   "для этой ПТ"
    formappedWPs   "для картированной ПТ"
    group     Группа
    element   Элемент
    insert    Вставка
    joinGR   "Объединить группы"
    TRtoRT   "Преобразование из ТР в МШ"
    TRtoLN   "Преобразование из ТР в ЛН"
    TRtoTR   "Упрощение ТР"
    TRRTnpoints   "Кол-во сохраняемых точек"
    TRlinedispl   "Показать результат сейчас же"
    TRTRdispl   "Показать ТР сейчас же"
    WP   ПТ
    RT   РТ
    TR   ТР
    TP   ТТ
    LN   ЛН
    LP   ЛП
    GR   ГР
    LAP  КРУГ
    commrec   "Связь с приёмником"
    abort      Прервать
    ACKs       ACKs
    NAKs       NAKs
    packets    пакеты
    unnamed   "(неизвестн.)"
    fromTR    "От ТР: %s"
    mapload   "Географически привязанное изображение"
    loadmback  Загрузить
    savemback "Сохранить гео-привязку"
    chgmback   Изменить
    clearmback Очистить
    backgrnd   Фон
    nameMapBkInfo "Описание фона"
    nameMapInfo "Настройки карты"
    mpbkchg     "Изменить фон"
    mpbkgrcs    "Положение сетки"
    nameImage    Изображение
    symbol       Символ
    SY1st_aid   "Первая помощь"
    SYCATaviation Авиация
    SYCATgeneral "Общего использования"
    SYCATland     Суша
    SYCATwater    Вода
    SYMOB        "Человек за бортом"
    SYRV_park    "Парк отдыха"
    SYWP_buoy_white "Буй, белый"
    SYWP_dot     "ПТ"
    SYairport        "Аэропорт"
    SYamusement_park "Парк аттракционов"
    SYanchor         "Якорная стоянка"
    SYanchor_prohib  "Якорная стоянка запрещена"
    SYavn_danger     "Опасность (авиа)"
    SYavn_faf        "Точка нач. захода на посадку"
    SYavn_lom        "Дальний маркерный маяк"
    SYavn_map        "Высота принятия решения"
    SYavn_ndb        "Всенаправленный радиомаяк"
    SYavn_tacan      "TACAN"
    SYavn_vor        "VHF omni-range"
    SYavn_vordme     "VOR-DME"
    SYavn_vortac     "VOR/TACAN"
    SYbait_tackle    "Рыбалка и охота"
    SYball           "Шар"
    SYbeach          "Берег"
    SYbeacon         "Маяк"
    SYbell           "Колокол"
    SYbiker          "Байкер"
    SYboat           "Лодка"
    SYboat_ramp      "Лодочный причал"
    SYborder "Пересечение границы"
    SYbowling "Боулинг"
    SYbox_blue "Ящик, голубой"
    SYbox_green "Ящик, зелёный"
    SYbox_red "Ящик, красный"
    SYbridge "Мост"
    SYbuilding "Здание"
    SYbuoy_amber "Буй, янтарный"
    SYbuoy_black "Буй, чёрный"
    SYbuoy_blue "Буй, голубой"
    SYbuoy_green "Буй, зелёный"
    SYbuoy_green_red "Буй, зелёно-красный"
    SYbuoy_green_white "Буй, зелёно-белый"
    SYbuoy_orange "Буй, оранжевый"
    SYbuoy_red "Буй, красный"
    SYbuoy_red_green "Буй, красно-зелёный"
    SYbuoy_red_white "Буй, красно-белый"
    SYbuoy_violet "Буй, фиолетовый"
    SYbuoy_white "Буй, белый"
    SYbuoy_white_green "Буй, бело-зелёный"
    SYbuoy_white_red "Буй, бело-красный"
    SYcamping "Место для привала"
    SYcapitol_city "Город, звезда"
    SYcar "Машина"
    SYcar_rental "Прокат машин"
    SYcar_repair "Автосервис"
    SYcasino "Казино"
    SYcastle "Замок"
    SYcemetery "Кладбище"
    SYchapel "Часовня"
    SYchurch "Церковь"
    SYcircle_blue "Круг, голубой"
    SYcircle_green "Круг, зелёный"
    SYcircle_red "Круг, красный"
    SYcircle_x "Закруглённая Х"
    SYcivil "Почтовый адрес"
    SYcntct_afro "Афро"
    SYcntct_alien "Инопланетяне"
    SYcntct_ball_cap "Бейсболка"
    SYcntct_big_ears "Большое ухо"
    SYcntct_biker "Байкер"
    SYcntct_bug "Жук"
    SYcntct_cat "Кошка"
    SYcntct_dog "Собака"
    SYcntct_dreads "Пугающее"
    SYcntct_female1 "Женщина 1"
    SYcntct_female2 "Женщина 2"
    SYcntct_female3 "Женщина 3"
    SYcntct_goatee "Эспаньолка"
    SYcntct_kung_fu "Кунг фу"
    SYcntct_pig "Свинья"
    SYcntct_pirate "Пираты"
    SYcntct_ranger "Лесничий"
    SYcntct_smiley "Улыбка"
    SYcntct_spike "Шип"
    SYcntct_sumo "Сумо"
    SYcoast_guard "Береговая охрана"
    SYcontrolled "Контролируемая зона"
    SYcrossing "Переход"
    SYdam "Дамба"
    SYdanger "Опасность"
    SYdeer "Олень"
    SYdiamond_blue "Алмаз, голубой"
    SYdiamond_green "Алмаз, зелёный"
    SYdiamond_red "Алмаз, красный"
    SYdiver_down_1 "Место дайвинга 1"
    SYdiver_down_2 "Место дайвинга 2"
    SYdock "Док"
    SYdollar "Доллар"
    SYdot "Точка"
    SYdrinking_water "Питьевая вода"
    SYdropoff "Трамплин для байка"
    SYduck "Утка"
    SYelevation "Превышение"
    SYexit "Выход"
    SYexit_no_serv "Выход, нет обслуживания"
    SYfactory "Фабрика"
    SYfastfood "Закусочная"
    SYfish "Рыба"
    SYfitness "Фитнесс"
    SYflag "Флаг"
    SYflag_pin_blue "Флажок, голубой"
    SYflag_pin_green "Флажок, зелёный"
    SYflag_pin_red "Флажок, красный"
    SYfuel "АЗС"
    SYfuel_store "АЗС с магазином"
    SYgeo_name_land "Топоним"
    SYgeo_name_water "Гидроним"
    SYglider "Планёр"
    SYgolf "Гольф"
    SYheliport "Вертолётная площадка"
    SYhorn "Горн"
    SYhouse "Дом"
    SYhouse_2 "Дом 2"
    SYhydrant "Гидрант"
    SYice_skating "Каток"
    SYinfo "Информ"
    SYintersection "Пересечение"
    SYis_highway "Скоростное шоссе"
    SYknife_fork "Трактир"
    SYladder "Лестница"
    SYlanding "Место приземления"
    SYlarge_city "Город, большой"
    SYlarge_exit_ns "Выход,нет обслуж, Большой"
    SYletter_a_blue "A, голубой"
    SYletter_a_green "A, зелёный"
    SYletter_a_red "A, красный"
    SYletter_b_blue "B, голубой"
    SYletter_b_green "B, зелёный"
    SYletter_b_red "B, красный"
    SYletter_c_blue "C, голубой"
    SYletter_c_green "C, зелёный"
    SYletter_c_red "C, красный"
    SYletter_d_blue "D, голубой"
    SYletter_d_green "D, зелёный"
    SYletter_d_red "D, красный"
    SYlevee "Набережная"
    SYlight "Освещение"
    SYlodging "Жилище"
    SYmany_fish "Рыбное место"
    SYmany_tracks "Скопление грузовиков"
    SYmarina "Гавань для катеров"
    SYmark_x "Метка, х"
    SYmedium_city "Город, средний"
    SYmile_marker "Километровый столб (миля)"
    SYmilitary "Расположение военных"
    SYmine "Мина"
    SYmonument "Монумент"
    SYmountains "Горы"
    SYmovie "Кино"
    SYmug "Грязь"
    SYmuseum "Музей"
    SYntl_highway "Федеральная трасса"
    SYnull "(прозрачный)"
    SYnull_2 "(пустой)"
    SYnumber_0_blue "0, голубой"
    SYnumber_0_green "0, зелёный"
    SYnumber_0_red "0, красный"
    SYnumber_1_blue "1, голубой"
    SYnumber_1_green "1, зелёный"
    SYnumber_1_red "1, красный"
    SYnumber_2_blue "2, голубой"
    SYnumber_2_green "2, зелёный"
    SYnumber_2_red "2, красный"
    SYnumber_3_blue "3, голубой"
    SYnumber_3_green "3, зелёный"
    SYnumber_3_red "3, красный"
    SYnumber_4_blue "4, голубой"
    SYnumber_4_green "4, зелёный"
    SYnumber_4_red "4, красный"
    SYnumber_5_blue "5, голубой"
    SYnumber_5_green "5, зелёный"
    SYnumber_5_red "5, красный"
    SYnumber_6_blue "6, голубой"
    SYnumber_6_green "6, зелёный"
    SYnumber_6_red "6, красный"
    SYnumber_7_blue "7, голубой"
    SYnumber_7_green "7, зелёный"
    SYnumber_7_red "7, красный"
    SYnumber_8_blue "8, голубой"
    SYnumber_8_green "8, зелёный"
    SYnumber_8_red "8, красный"
    SYnumber_9_blue "9, голубой"
    SYnumber_9_green "9, зелёный"
    SYnumber_9_red "9, красный"
    SYoil_field "Нефтянное поле"
    SYopen_24hr "Открыто круглосуточно"
    SYoval_blue "Овал, голубой"
    SYoval_green "Овал, зелёный"
    SYoval_red "Овал, красный"
    SYparachute "Парашют"
    SYpark "Парк"
    SYparking "Парковка"
    SYpharmacy "Аптека"
    SYphone "Телефон"
    SYpicnic "Место для пикника"
    SYpin_blue "Булавка, голубой"
    SYpin_green "Булавка, зелёный"
    SYpin_red "Булавка, красный"
    SYpizza "Пицца"
    SYpolice "Полиция"
    SYpost_office "Почта"
    SYprivate "Частный аэродром"
    SYradio_beacon "Радиомаяк"
    SYramp_int "Разноуровневая развязка"
    SYrect_blue "Блок, голубой"
    SYrect_green "Блок, зелёный"
    SYrect_red "Блок, красный"
    SYreef "Риф"
    SYrestricted "Запретная зона"
    SYrestrooms "Туалет"
    SYscenic "Живописное место"
    SYschool "Школа"
    SYseaplane "База гидропланов"
    SYshopping_cart "Рынок"
    SYshort_tower "Башня, короткая"
    SYshowers       "Душ"
    SYskiing        "Водные лыжы"
    SYskull         "Череп"
    SYsmall_city    "Город, маленький"
    SYsnow_skiing   "Катание на лыжах"
    SYsoft_field    "Мягкое поле (пашня)"
    SYsquare_blue   "Квадрат, голубой"
    SYsquare_green  "Квадрат, зелёный"
    SYsquare_red    "Квадрат, красный"
    SYst_highway    "Областная трасса"
    SYstadium       "Стадион"
    SYstore         "Магазин"
    SYstreet_int    "Перекрёсток"
    SYstump         "Пень"
    SYsummit        "Вершина"
    SYswimming      "Место для плавания"
    SYtake_off      "Точка взлёта"
    SYtall_tower    "Башня, высокая"
    SYtheater        Театр
    SYtide_pred_stn "Гидрологическая станция"
    SYtoll           Таможня
    SYtow_truck "Эвакуатор"
    SYtraceback "Обратный путь"
    SYtracks "Просёлочная дорога"
    SYtrail_head "Начало пешеходной тропы"
    SYtree "Дерево"
    SYtriangle_blue "Треугольник, голубой"
    SYtriangle_green "Треугольник, зелёный"
    SYtriangle_red "Треугольник, красный"
    SYtruck_stop "Стоянка грузовиков"
    SYtunnel "Тунель"
    SYultralight "Сверхлёгкий"
    SYweight_station "Весовая"
    SYwreck "Место аварии"
    SYzoo "Зоопарк"
    psvisible "Только видимая часть"
    DISPsymbol "Только символ"
    DISPs_name    "Символ и имя"
    DISPs_comment "Символ и комментарий"
    DISPname      "Только имя"
    DISPcomment   "Только комментарий"
    dispopt        Показ 
    mapitems      "Показ на карте"
    nameIC         Иконка
    prod           Продукт
    WPCapac "Waypoint Capacity"
    ICCapac "Icon Capacity"
    RTCapac "Route Capacity"
    TRCapac "Track Capacity"
    protcl "Протокол"
    ICGraph "Icon Graphics"
    WPperRT "Кол-во точек пути на маршрут"
    notinGR "не в (под-)группе"
    onlyinGR "только в (под-)группе"
    loadgrels "Загрузка элементов"
    importgrels "Импорт элементов"
    about       "О GPSMan..."
    contrib "При участии"
    errorsto "Отчёты об ошибках в:"
    obsTRToRT "ПТ-и, созданные при преобразовании"
    obsTRsimpl "Результирующий ТР после упрощения"
    nameLists "Списки"
    nameData "Данные"
    MWCmap "Карта"
    MWClists "Списки"
    MWCsingle "Отдельное окно"
    search "Поиск"
    rmrk "Прим"
    closeto "Близко к"
    with "С"
    srchres "НАЙДЕНО"
    database "База Данных"
    where "Где"
    what "Что"
    list "список"
    distance "Расстояние"
    fromWP "от Точки пути"
    fromPos "от места"
    azimuth "Азимут"
    any "любой"
    opening "Открывание"
    suggested "предложенный"
    another "Другое"
    srchdd1 "Искать с"
    srchdd2Data "все элементы"
    srchdd2GR "Группа(ы)"
    from "от"
    started "начать с"
    transf  "Преобразование коорд"
    TRNSFAffine    "Аффинное"
    TRNSFAffineConf  "Конф Аффин"
    TRNSFNoRot      "Конф без повор"
    projection "Проекция"
    lat0  "Шир центра"
    long0 "Долг центра"
    lat1  "Шир 1ой станд параллели"
    lat2  "Шир 2ой станд параллели"
    latF  "Lat false origin"
    latF  "Шир условного нач координат"
    longF "Long false origin"
    longF "Долг условного нач координат"
    k0 "Коэф масштабирования"
    PRJUTM "UTM/UPS"
    PRJTM "Transverse Mercator"
    PRJBMN "Austrian BMN Grid"
    PRJBNG "British National Grid"
    PRJBWI "British West Indies"
    PRJCMP "Portuguese Mil Map"
    PRJCTR "Carta Tecnica Reg (I)"
    PRJITM "Irish Transv Mercator"
    PRJGKK "German Grid"
    PRJLCC1 "Lambert Conic Conf 1"
    PRJLCC2 "Lambert Conic Conf 2"
    PRJKKJP "Basic Finnish Grid"
    PRJKKJY "Uniform Finnish Grid"
    PRJSEG "Swedish Grid"
    PRJMerc1 "Mercator 1"
    PRJMerc2 "Mercator 2"
    PRJCS "Cassini-Soldner"
    PRJAPOLY "American Polyconic"
    PRJStereogr Stereographic
    PRJTWG "Taiwan Grid"
    PRJSOM "Swiss Oblique Mercator"
    PRJLV03 "Swiss LV03 Grid"
    PRJIcG "Iceland Grid"
    PRJRDG "The Netherlands Grid"
    PRJSchreiber Schreiber
    dontaskagain "Остановить опрос"
    rename "Используйте новое имя"
    oname "Исходное имя"
    never "Никогда"
    ask "Запрос"
    always "Всегда"
    stage "Этап"
    label "Подпись"
    alt "Высота"
    locate "Местоположение"
    animation  Анимация
    fast Быстро
    slow Медленно
    start Начало
    pause Пауза
    speed Скорость
    centred "Удерживать в центре"
    state Штат
    animinit "в нач/кон"
    animon "запуск"
    animpause "пауза"
    animabort "отмена"
    realtimelog "Показ в реальном времени"
    garmin Garmin
    garmin_usb "Garmin USB"
    nmea "NMEA 0183"
    stext "Просной Текст"
    simul "симулятор"
    lowrance Lowrance
    magellan Magellan
    getlog "Получить журнал"
    stop Стоп
    dolog Запись
    show Показать
    hide Спрятать
    posfixerror error
    posfix_  ?
    posfix2D 2D
    posfix3D 3D
    posfix2D-diff "2D d"
    posfix3D-diff "3D d"
    posfixGPS GPS
    posfixDGPS DGPS
    posfixAuto ok
    posfixsimul simul
    restart Перезапуск
    mkTR "Создать ТР"
    PVTflds "# t lat long alt fix EPE EPH EPV vel_x vel_y vel_z TRK"
    namePVTData "Журнальные данные"
    mkavgWP "Создать средн ПТ"
    move Переместить
    define Определить
    open Открыть
    defs "Определения"
    baseproj "Базовая проекция"
    abbrev "Сокращённое имя"
    grid "Координатная сетка"
    use Использовать
    unit "Ед.изм."
    feasting "Смещение на восток"
    fnorthing "Смещение на север"
    bounds Границы
    max Макс
    min Мин
    easting Восточнее
    northing Севернее
    fixeddatum "Уточнённый датум"
    elevation   Высота
    usewps     "Использовать ПТи"
    chgpfrmt   "Изменить формат местоположения"
    here        Здесь
    atprevwp   "К предыдущей ПТ"
    prevwp     "Предыдущая ПТ"
    firstwp    "Первая ПТ"
    chglstrs   "Редактировать предыдущее ребро"
    chgnxtrs   "Редактировать следующее ребро"
    contnend   "Добавить в конец"
    closemenu  "Закрыть меню"
    ellpsd      Эллипсоид
    datum       Датум
    userdefs   "Пользовательские настройки"
    edmap      "Редактировать карту"
    actual     действующие
    rtimelogintv "Интервал журналирования"
    inca      "Добавить после"
    invert    "Инвертировать"
    recwindow "Окно приёмника"
    volume    "Объём"
    latS      "Шир Юг"
    latN      "Шир Св"
    longW     "Долг Зп"
    longE     "Долг Вс"
    no         Нет
    computations Вычисления
    comparea  "Вычислить площадь"
    cluster    Кластер
    centre     Центр
    mkclusters "Создать кластеры"
    quadr      Трапеция
    dlat      "Разность широт"
    dlong     "Разность долгот"
    collcntr  "Сбор центров..."
    compclstr "Расчёт кластеров..."
    crtgclstrgrs "Создание групп..."
    chgdatum  "Изменение датума..."
    print      Печать
    prevctr   "Предыдущий центр"
    printopt  "Печать настроек"
    cwpsdef   "Непосредственное определение ПТк"
    nextTP    "Следующая ТТ"
    generate   Сгенерировать
    generateall "Сгенерировать все"
    width      Ширина
    simplTRto "Упростить до"
    exstglog  "Существующий журнал"
    contnsly  "Непрерывно"
    animate   "анимация"
    animabbrev "аним."
    noanabbr  "не аним."
    zelev     "Z-шкала"
    xyelev    "XY-шкала"
    notext    "без текста"
    view      "Просмотр"
    sideview  "Вид с боку"
    persptv   "Перспектива"
    optMAPCOLOUR,trvtrk "TRK стрелка"
    optMAPCOLOUR,trvtrn "TRN стрелки"
    optMAPCOLOUR,trvcts "CTS стрелка"
    optMAPCOLOUR,trvcts2 "2ая CTS стрелка"
    optMAPCOLOUR,trvvel_z "Стрелка верх/низ"
    optMAPCOLOUR,trvwrnimportant "Важное предупреждение (нав)"
    optMAPCOLOUR,trvwrnnormal "Предупреждение (нав)"
    optMAPCOLOUR,trvwrninfo "Информация (нав)"
    travel Путешествие
    notravel "Остановить путешествие"
    travdisplay "Настроить экран"
    travchgdisplay "Изменить на экран"
    travdsetup "Настройна экрана путешествия"
    navMOB "MOB: Человек за бортом!"
    startnav "Навигация"
    navWP "Перейти к ПТ"
    goback "Вернуться назад"
    follow "Следовать за %s"
    exactly "точно"
    fromnrst "от ближайшей"
    inrvrs "в обратном напр."
    forgetgoal "Забыть цель"
    suspend "Приостановиться"
    resume "Продолжить"
    TRVfix "Исправить"
    TRVhour "Время"
    TRVspeed "Скорость"
    TRVpos "Pos"
    TRValt "Alt"
    TRVtrk "TRK"
    TRVnxtWP "К"
    TRVprvWP "От"
    TRVete "ETE"
    TRVeta "ETA"
    TRVvmg "VMG"
    TRVxtk "XTK"
    TRVcts "CTS"
    TRVtrn "TRN"
    TRVvel_z "V скорость"
    TRVtrkcts "TRK, CTS"
    TRVdist "Расст"
    TRVc_trkcts "TRK/CTS стрелки"
    TRVc_trn "TRN стрелки arrows"
    TRVc_vel_z "Стрелка верх/низ"
    add "Добавить"
    addlabelled "Добавить с меткой"
    remove "Удалить"
    mindist "Расстояние прибытия"
    chggoal "Изменить на следующую цель"
    chginggoal "Изменеие на след. цель"
    chggoalhlp "Когда переходить с текущей\nна следующую цель при\nследовании по МР/ТР"
    soon ранний
    late поздний
    warnings "Предупреждения"
    dowarn "Печатать предупреждения"
    warnconf "Сконфигурировать предупреждения"
    priority Приоритет
    high высокий
    medium средний
    low низкий
    warnprox "Расстояние до ПТ <"
    warnanchor "Расстояние до ПТ>"
    warnspeed "Скорость >"
    warntrn "TRN (abs)>"
    warnvspeed "Вертикальная скорость"
    warnxtk "XTK (abs)>"
    trvhlpbox "Используйте прав.кнопку для переупорядочивания элементов снизу"
    trvhlpbxs "Используйте прав.кнопку для переупорядочивания элементов списка"
    trvwarrv "Прибытие в %s!"
    trvwleave "Убытие из %s!"
    trvwspeed "Скорость > %s!"
    trvwtrn "TRN > %s!"
    trvwvspeed "Верт скорость на между [%s,%s]!"
    trvwxtk "XTK > %s!"
    trvwnolog "Журнал реального времени отключен!"
    trvwnopos "Предыдущее положение нериемлемо"
    trvwuwps "МТ содержит неопределённые точки"
    trvwchg "Перейти немедленно к %s"
    drivesim "симулятор водителя"
    startfrom "Начать с..."
    outofctrl "Вне контроля!"
    right Право
    left Лево
    straight Прямо
    rthlpdsim "Клавиши Стрелок: управление, изменение скорости \nПробел: прямо"
    hiddendata "Скрытые данные"
    Ghidden_class Класс
    Ghidden_subclass Подкласс
    Ghidden_lnk_ident "Ид этапа"
    Ghidden_colour Цвет
    Ghidden_attrs Аттрибуты
    Ghidden_depth Глубина
    Ghidden_state Штат
    Ghidden_country Страна
    Ghidden_facility Предприятие
    Ghidden_city Город
    Ghidden_addr Адрес
    Ghidden_int_road "Пересечение дорог"
    Ghidden_dtyp "Показать опц+тип"
    Ghidden_ete "ETE"
    Ghidden_display "Показать?"
    Ghidden_yes Да
    Ghidden_no Нет
    Ghidden_user "Пользователь"
    Ghidden_user_symbol "Пользователь (только символ)"
    Ghidden_non_user "Не пользователь"
    Ghidden_avn_airport "Аэропорт"
    Ghidden_avn_inters "Авиа пересечение"
    Ghidden_avn_NDB "Ненапр.радиомаяк"
    Ghidden_avn_VOR "VOR"
    Ghidden_avn_airp_rway "Начало взл-посад полосы"
    Ghidden_avn_airp_int "Пересечение взлётн. полос"
    Ghidden_avn_airp_NDB "Ненапр.радиомаяк в аэропорте"
    Ghidden_map_pt "Точка на карте"
    Ghidden_map_area "Область на карте"
    Ghidden_map_int "Пересечение на карте"
    Ghidden_map_addr "Адрес на карте"
    Ghidden_map_line "Линия на карте"
    Ghidden_locked Блокировано
    Ghidden_default "Исходное"
    Ghidden_black Чёрный
    Ghidden_white Белый
    Ghidden_red Красный
    Ghidden_dark_red "Тёмнокрасный"
    Ghidden_green Зелёный
    Ghidden_dark_green "Тёмнозелёный"
    Ghidden_blue Голубой
    Ghidden_dark_blue "Синий"
    Ghidden_yellow Жёлтый
    Ghidden_dark_yellow "Тёмножёлтый"
    Ghidden_magenta Маджента
    Ghidden_dark_magenta "Тёмн.маджента"
    Ghidden_cyan Циан
    Ghidden_dark_cyan "Тёмн.циан"
    Ghidden_light_gray "Светлозелёный"
    Ghidden_dark_gray "Тёмнозелёный"
    Ghidden_transparent Прозрачный
    Ghidden_line Линия
    Ghidden_link Этап
    Ghidden_net Сеть
    Ghidden_direct Прямой
    Ghidden_snap Снимок
    Ghidden_temp Температура
    Ghidden_time "Временная метка"
    Ghidden_cat Категория
    renres "ПЕРЕИМЕНОВАНО"
    forgetGRcs "Удалить ГР и Элементы"
    optDEFMAPPROJ "Проекция карты"
    optDEFMAPPFRMT "Система координат"
    optDEFMAPPFDATUM "Датум системы координат"
    undo Отменить
    UTMzone зона
    tfwfile  "TFW файл"
    ok Ok
    newWPatdb "Новая ПТ возле..."
    PRJLamb93 "Lambert 93"
    PRJLambNTFe "Lambert NTF IIet"
    PRJLambNTF "Lambert NTF"
    NTFzone зона
    loop Петля
    crtLN "Создать ЛН"
    PRJAEA "Albers Equal Area"
    PRJLEAC "Lambert Eq Area Conic"
    PRJTAlbers "Teale Albers"
    polasp "Polar aspect"
    north Север
    south Юг
    distunit "Ед.изм. расстояния"
    altunit "Ед.изм. высоты"
    params "Параметры"
    dimens "Размеры"
    version "Версия"
    opinprogr "Операция выполняется"
    working "Работаю"
    aborted "Прервано!"
    errwarn "Ошибки/предупреждения!"
    changegroupsymbol "Изменить символ"
    ozimapfile "Ozi Map файл"
    info "Информация"
    climbrate  "Скорость подъёма"
    BGAfeature  "Признак"
    BGAfindblty "Возможность поиска"
    BGAairact   "Активность в воздухе"
    dispitems "Отображаемые элементы"
    hiditems "Скрытые элементы"
    mkgrp "Создать Группу"
    optAutoNumRts "Авто-нумерация МРов при поступлении"
    numberfrom0 "Установить счётчик на 1"
    items "Элементы"
    optSUPPORTLAPS "Поддержка для КРУГОВ"
    nameLAP "КРУГ"
    duration "Продолжительность"
    calrs "Калории"
    syusrmenu "Настраиваемое меню символов"
    cfgsymenu "Настройка меню символов"
    insmnb   "Вставить под-меню перед"
    insmna   "Вставить под-меню после"
    opensbmn "Открыть под-меню"
    clssbmn  "Закрыть под-меню"
    syhlpbx "Используйте прав-кнопку для\nпереупорядочивания списка элементов"
    lapsrun Запуск
    fromfile "из файла"
    fromdef "из предустановок"
    mbaktoload "Фон для загрузки"
    none "пусто"
    nameAL "Альманах"
    alm_svid "ид спутника"
    alm_week "неделя"
    alm_datatime "временная метка данных"
    alm_clockc1 "кэфф поправки часов сек"
    alm_clockc2 "кэфф поправки часов сек/сек"
    alm_ecc "эксцентриситет"
    alm_sqrta "квадратный корень"
    alm_mnanom "средняя аномалия"
    alm_argprgee "аргумент перигея"
    alm_rightasc "прямое восхождение"
    alm_rtrightasc "скорость пр. восхождения"
    alm_inclin "склонение"
    alm_health "здоровье"
    lstsqs  "Наименьшие Квадраты"
    lstsqsfile "Файл Наименьших Квадратов"
    PRJEOV  "Аварийная машина (EOV)"
    chgname "Изменить имя"
    clicktoedit "Нажмите для редактирования"
    renamethod "Метод переименования"
    operators "Операторы"
    keep1st "сохранить 1й символ"
    keep1st_hlp "Сохранить первый символ\nдальнейшие операции производятся над оставшимися символами"
    reset "сброс"
    reset_hlp "Перезапустить с начальным именем, отменить все преддущие изменения"
    case "сменить регистр"
    case_hlp "Все буквы в верхний/нижний регистр"
    maxlgth "макс. длина"
    maxlgth_hlp "Текущая строка обрезаются до указанной длины"
    inslt "вставить слева"
    inslt_hlp "Строка вставляется перед текущим левым символом"
    insrt "вставить справа"
    insrt_hlp "Строкаа вставляется в конец"
    replc "заменить"
    replc_hlp "Любой символ в 1п строке будет заменен соответствующим символом из 2й"
    delany "удалить любой"
    delany_hlp "Все символы в данной строке будут удалены из имени"
    rsub "подстановка"
    rsub_hlp "Указать регулярное выражение и спецификацию подстановки\nподробнее - см. руководство"
    accifnew "применить, если новое"
    accifnew_hlp "Применить результат, если имя не используется"
    guntilnew "добавлять, пока новое"
    guntilnew_hlp "Перебирать все возможные значения числа, пока новое имя не будет найдено"
    ndigits "Количество цифр"
    gennames "Генерация имен..."
    rentest_hlp "Проверить метод на указанном имени"
    renmove_hlp "Используйте правую кнопку для перераспределения строк"
    tolower "нижн.регистр"
    toupper "верх.регистр"
    applyto "Применить к"
    forall "для всех"
    failed "неудача"
    selfont "Выбранный шрифт"
    default "По умолчанию"
    size "Размер"
    units "Единицы"
    points "точки"
    pixels "пиксели"
    weight "Вес"
    normal "нормальный"
    bold "полужирный"
    slant   "наклонный"
    roman "римский"
    italic "курсив"
    underline "Подчеркнутый"
    overstrike "Перечеркнутый"
    optDEFAULTFONT "Шрифт по умолчанию"
    optFIXEDFONT "Фиксированный шрифт"
    optMAPFONT "Шрифт карты"
    optTRAVELFONT "Шрифт экрана путешествия"
    optPLOTFONT "Шрифт графика"
    plugin  "Плугин"
    unavailif  "Недоступно, если"
    tclcode  "Tcl код"
    totdstng   "Общая дистанция, без разрывов"
    tottimeng  "Общее время, без разрывов"
    SYcross "Курсор"
    SYcross_3p "Курсор 3 пикс"
    mapfitWPs "Отобразить ближайшие ПТ на карте"
    showfitinfo "Показать адаптированные данные"
    xtcoord "xt"
    ytcoord "yt"
    delta   "d"
    residual "rt"
    rmsxydev "rms(x,y-отклонения)"
    resstderr "остатоная ошибка"
    chgdev "Сменить прибор"
    maxalt "Максимальная высота"
    minalt "Минимальная высота"
    alt_cumula "Нарастающая высота восхождения"
    alt_cumuld "Нарастающая величина спуска"
    optSHOWFILEITEMS "По умолчанию отображать на карте читаемые элементы"
    vertgridlines "Вертикальные линии сетки"
    convert  "Конвертировать"
    split    "Разделить"
    bysel  "по выделенным точкам"
    byseg  "по сегментам"
    openits "Открыть %s"
    uname "Имя пользователя"
    pword "Пароль"
    remember "Запомнить"
    wptotwitter "ПТ в Twitter"
    wptotwitternb "Послать позицию, дату, высоту, имя и комментарий путевой точки в Twitter с тегами #GPSMan #waypoint"
    exportTFW "Экспортировать в TFW файл"
    nametfwfile "TFW файл"
}

    # the following definitions must be coherent with $TXT
array set INVTXT {
    DMS   DMS
    DMM   DMM
    DDD   DDD
    Град  GRA
    UTM/UPS   UTM/UPS
    MH   MH
    ПТ   WP
    МШ   RT
    ТР   TR
    ЛН   LN
    ГР   GR
    КРУГ LAP
    м    M
    фут   FT
}

# changes by Miguel Filgueiras to RM contribution
set TXT(srChainage) $TXT(totdst)
set TXT(srShowRest) $TXT(gpSRests)
set TXT(gpSym) $TXT(symbol)
set TXT(srRest) $TXT(gpRest)

