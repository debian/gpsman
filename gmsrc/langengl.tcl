#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: langengl.tcl
#  Last change:  6 October 2013
#
# Includes contributions by
#  - Brian Baulch (baulchb _AT_ onthenet.com.au)
#  - Matt Martin (matt.martin _AT_ ieee.org)
#  - Jean H Theoret (ve2za _AT_ rac.ca)
#  - Valere Robin (valere.robin _AT_ wanadoo.fr)
#  - David Gardner (djgardner _AT_ users.sourceforge.net)
#  - Rudolf Martin (rudolf.martin _AT_ gmx.de)
#

# only 3 chars long names; check also ALLMONTH in file i18n-utf8.tcl
set MONTHNAMES "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec"

set DLUNIT(KM,dist) km
set DLUNIT(KM,subdist) m
set DLUNIT(KM,speed) kph
set DLUNIT(KM,area) "sq km"
set DLUNIT(NAUTMILE,dist) "nm"
set DLUNIT(NAUTMILE,subdist) ft
set DLUNIT(NAUTMILE,speed) knot
set DLUNIT(NAUTMILE,area) "sq nm"
set DLUNIT(STATMILE,dist) "st ml"
set DLUNIT(STATMILE,subdist) ft
set DLUNIT(STATMILE,speed) mph
set DLUNIT(STATMILE,area) "section"

set DLUNIT(M,dist) $DLUNIT(KM,subdist)
set DLUNIT(FT,dist) $DLUNIT(STATMILE,subdist)

set DTUNIT $DLUNIT($DISTUNIT,dist)
set SPUNIT $DLUNIT($DISTUNIT,speed)
set ARUNIT $DLUNIT($DISTUNIT,area)
set ALUNIT $DLUNIT($ALTUNIT,dist)

set MESS(RTcomp) "#\tWP\t\t$DTUNIT\tdeg\t$ALUNIT"
set MESS(TRcomp) "#\t\t\t\t$ALUNIT\t$DTUNIT\t$DTUNIT\th:m:s\t$SPUNIT\tdeg"
set MESS(WPNearest) "WP\t\t$DTUNIT\tdeg"

array set MESS {
    badscale   "Bad value for map scale"
    oktoexit   "Ok to exit (unsaved data will be lost)"
    okclrmap   "Ok to clear map"
    namelgth   "Name must not exceed %d characters"
    cmmtlgth   "Comment must not exceed %d characters"
    namevoid   "Name cannot be void"
    baddate   "Bad date; line"
    badhdg   "Bad heading %s: must be %s or +/-"
    badcoord   "Bad coordinate range or format: \"%s\" must be %s"
    outofrng   "Number out of range"
    UTMZN   "Must be a letter in A..H, J..N, or P..Z"
    badstrg "Bad character(s)"
    strgvoid   "String cannot be empty"
    nan   "\"%s\" is not a number"
    written   "Written by"
    undefWP   "Route %s has undefined WP(s); not saved/exported"
    bigRT   "Route %s > max number; not exported"
    unkndatum   "Unknown datum"
    badcommdWP   "Bad command when loading WPs; line"
    notabsWP   "Bad WP definition; no tabs, line"
    undefinedWP   "Reference to unknown WP: %s"
    undefinedTR   "Unknown TR"
    nofieldsWP   "Bad WP, not enough fields; line"
    excfieldsWP  "Bad WP, too many fields; line"
    badWPsRT   "RT with bad waypoints list; line"
    toomuchWPs   "Warning: more than %d WPs in RT"
    badcommdTP   "Bad command when loading TPs; line"
    badcommdLP   "Bad command when loading LPs; line"
    badTP   "Bad point in TR; line"
    badTPsTR   "TR with bad trackpoints list; line"
    badLP   "Bad point in LN; line"
    badLPsLN   "LN with bad points list; line"
    noheader   "File does not start with header"
    loaderr   "Error when loading from file; line"
    unkncommd   "Unknown command when loading from file; line"
    noformat   "File does not start with Format line"
    badformat   "Bad format line"
    badRT   "Bad RT definition; line"
    badRTargs   "Bad RT arguments; line"
    badTR   "Bad TR definition; line"
    fileact   "%s %s file?"
    filexists   "File exists!"
    GPSok   "Connection ok"
    toomany   "Too many %ss (> %d)"
    cantfgt   "Cannot forget %s: belongs to mapped item"
    cantfgted   "Cannot forget %s: being edited"
    cantmapRTunkn   "Route not mapped; no info for WP"
    cantmapRTed   "Route not mapped; WP beig edited"
    cantrun   "Cannot run"
    inprogr   "Operation already in progress"
    cantread   "Cannot read auxiliary file"
    cantwrtopt   "Cannot write preferences file"
    voidRT   "Route contains no waypoints"
    activeRT   "Route 0 is active route; continue?"
    voidTR   "Track contains no points"
    voidLN   "Line contains no points"
    idinuse   "Identifier already in use"
    cantunmap   "Cannot unmap %s: belongs to mapped item"
    askrevert   "Revert to initial data?"
    askforget   "Forget this %s?"
    notimpl   "Not implemented"
    forgetall   "Forget all items in %s list?"
    counted   "There are %d %ss defined"
    notlisted   "Item not listed"
    wrgval   "Bad value for"
    voidGR   "Group has no elements"
    initselfGR   "Group contains itself via"
    GRelout   "Command for element outside GR definition; line"
    badGRels   "Group with bad elements list; line"
    badcommdGRel   "Bad command when loading GR elements; line"
    notypeforGRel   "Element with no type; line"
    badGRel   "Bad element definition; line"
    check   "Checking connection"
    toomanyerr   "Too many errors; aborting..."
    getWP   "Getting WPoints"
    getRT   "Getting Routes"
    getTR   "Getting Tracks"
    getLAP  "Getting Laps"
    getAL   "Getting almanac"
    putWP   "Putting WPoints"
    putRT   "Putting Routes"
    putTR   "Putting Tracks"
    fillbuffWP  "Loading WP buffer"
    fillbuffRT  "Loading RT buffer"
    noACKNAK  "Got packet when expecting ACK/NAK; check GPS connection"
    badplatform   "No I/O with serial port in this platform"
    badserial "Cannot open device %s"
    nodata    "No %ss in receiver"
    badimage "Bad image file"
    mapadjust "Please place WPs on map; click Ok when finished"
    duplicate "%s already selected"
    clrcurrmap "Clear current map?"
    mbkbaddatum "No or unknown datum"
    mbkbadscale "Scale must be non-negative"
    mbkbadat "Bad arguments"
    edityourrisk "Edit at your own risk!"
    okclrbkmap   "Ok to clear map background"
    okclrbkim    "Ok to clear image at"
    badattrval "Wrong %s optional field value: %s"
    badattr "Unknown %s optional field name: %s"
    badSYMBOLcode "Bad symbol code"
    badDISPOPTcode "Bad display option code"
    goingdown "Preferences saved; please restart"
    putwhat "Put what item types?"
    readwhat "Read what item types?"
    noWPinx "Too many WPs for receiver"
    noICinx "Too many ICs for receiver"
    getIC "Getting Icons"
    serial1 "Incorrect Header Preamble"
    serial2 "Incorrect Command returned"
    checksum1 "Incorrect Header Checksum"
    checksum2 "Data Checksum error"
    receiver "Not connected to device!"
    importonly1 "Can only import 1 kind of data"
    exportonly1 "Can only export 1 kind of data"
    outdatedprefs "Preferences file outdated; please check it now"
    mustchoose1type "At least 1 kind of items must be selected"
    nosuchitems "No item corresponds to description"
    resultsin "Search results in Group"
    badWP "WP not given or undefined"
    badangle "Angle must be >0 and <360 degrees"
    georefhow  "Geo-referencing method"
    cantsolve  "Cannot solve equations"
    transfcantscale "No scaling with current transformation"
    oldfilefmt "Old file format; please save under new format!"
    unknProj "Unknown map projection"
    unknTransf "Unknown map transformation"
    badProjargs "Wrong arguments for projection"
    badTransfargs "Wrong arguments for transformation"
    badfield "Bad attribute=value pair"
    badattr "Bad attribute name"
    missattrs "Missing attribute(s)"
    mbkbadproj "Bad map projection"
    mbkbadtransf "Bad map transformation"
    notUTMproj "Saved info for UTM; proceed with projection set to"
    projchg    "Please confirm projection parameters"
    badparam "Bad value for %s"
    connectedto "Connected to %s"
    recnotsuppd "Receiver model not supported"
    gotprots "Got protocols definition"
    badprots "Bad protocols definition"
    defprots "Using table of protocols"
    nohidden "Discard hidden data?"
    badRS "Route stage outside RT definition; line"
    badWPsRSs "RT stage before 1st or after last WP; line"
    windowdestr "The window has been destroyed!"
    badhidden "Bad format for hidden value"
    replname "Replace \"%s\" by a name having at most %d of the following characters: %s"
    badalt "Bad value for altitude"
    baddistunit "Bad distance unit for map scale in preferences file"
    badgridzone "Invalid grid zone"
    outofgrid "Position out of grid scope"
    timeoffnotint "Time offset must be an integer or end in .5"
    cantchkprot "Cannot check under current protocol"
    mustconn1st "Check connection with receiver first"
    rltmnotsupp "Real time log not supported by protocol in this receiver"
    createdir "Please create directory %s and restart"
    oktomkdir "Ok to create directory %s?"
    projnameabbr "Please give a name and a short name to the new projection"
    abbrevinuse "Short name already taken"
    nameinuse "Name already taken"
    projinuse "Projection is being used; changes will be discarded"
    gridneedsdatum "Bad grid definition for %s; no datum given"
    badgriddatum "Datum for the grid %s must be %s"
    cantchggriddatum "This grid needs the datum %s"
    gridinuse "Grid, used by %s, cannot be deleted; continue?"
    gridinusenochg "This grid is used by %s; no changes made"
    cantwrtprgr "Cannot write user projections file"
    cantwrtdtel "Cannot write user datums file"
    movingWP "Place %s with left-click\nCancel with right-click"
    missingdata "Not enough data!"
    needs1wp "Route must have at least 1 waypoint"
    emptypos "Position with empty field(s)"
    cantwrtsstate "Cannot write saved state file: %s"
    cantrdsstate "Cannot read saved state file: %s"
    corruptsstate "Corrupt saved state file: %s"
    editrisk "Edit at your own risk!"
    savestate "Save current state?"
    delsstate "Delete saved state files?"
    badmapinfo "Bad map parameters file"
    badMHloc "Wrong Maidenhead locator"
    areais "Area of (non-self intersecting) polygon is %.3f%s"
    areatoosmall "Area is too small (<%s sq km)"
    projarea "Computing projected area"
    selfintsct "Repeated WPs: RT cannot intersect itself!"
    badinvmdist "Approximation error when inverting meridional distance"
    badinvproj "Approximation error when inverting %s projection"
    negdlatlong "Lat/long range cannot be negative!"
    allundef "There are no definitions for the WPs in the GR"
    badfloats "Floating-point conversions not working correctly; are you sure you want to connect?"
    noprintcmd "No print command; give one in the options"
    cantexecasroot "GPSMan cannot be executed by root"
    badargtofunc "Bad argument to %s function"
    redefproj "User defined %s projection deletes a pre-defined projection under the same short name; please change your definition!"
    couldntcd "Failed to change to directory %s"
    shpext "Bad extension %s; ok to use .shp, .shx, .dbf?"
    shpcntopen "Could not create/open Shapefile files"
    shpcntcrtfs "Could not create Shapefile .dbf fields"
    shpcntwrtfs "Could not write Shapefile .dbf fields"
    shpoutmem "Out of memory!"
    shpemptyfile "Empty file"
    shpwrongfile "File type invalid"
    shplessdim "Dimension in file less than required; go on?"
    shpbadWPinRT "%d-th WP with bad coordinates ignored in RT %s"
    errorGTMread "Error when reading from GTrackMaker file"
    badGTMvers "GTrackMaker file version not acceptable"
    badGTMfile "Wrong initial string for GTrackMaker file"
    badGTMcounts "Negative count(s) in GTrackMaker file"
    badGTMlat "Latitude out of range in GTrackMaker file"
    badGTMlong "Longitude out of range in GTrackMaker file"
    badGTMdatum "Bad datum in GTrackMaker file"
    unobscmap "Error probably because there is a window/icon over the map; retry after delay?"
    cantwrtimg "Error writing image to file in %s format"
    cantsaveRTid "%d RT(s) not saved: non-numeric identifier"
    cantsaveTRid "%d TR(s) not saved: non-numeric identifier"
    badtrvconf "Corrupt configuration; restarting with empty one"
    drvsimoff "Driving simulator: not started yet!"
    needWP "Driving simulator: please load or define some WPs first"
    chgrecprot "Please change the receiver protocol"
    clrtrvlog "Clear travel log?"
    frgetGRcs "Forget group and all its elements?!"
    nmeainuse "Real-time log being captured or another NMEA file being read"
    badfile "Error when reading from file"
    RTnoWPname "WP given by name only in RT is no longer supported"
    distlarge "Distance too large!"
    convres "%s resulting from conversion created under name %s"
    outrngproj "Point out of range of projection!"
    badconv "Could not convert value for parameter %s: %s"
    badprmval "Wrong converted value for parameter %s: %s"
    timeconsmg "Time consuming operation: go on?"
    badtimeval "Bad time value"
    badLAP "Bad LAP; line"
    lapncnsdrd "LAP not considered"
    emptymenu "Empty menu; continue?"
    cantwrtsymenu "Cannot write symbol menu file"
    abbrevhasspaces "Short name cannot have blanks"
    needNpoints "You must give at least %s points!"
    twotimeoffsets "Different time offsets in file"
    notimeoffset "No time offset in file; assuming 0"
    baddateas "Bad date: %s"
    unknownenc "Unknown character encoding %s"
    chgbaudto "Trying to change baud rate to %s; please wait..."
    busytrylater "Other operations under way; please try later"
    baudchgfailed "Could not change baud rate"
    obssplit "Results of splitting %s named \"%s\""
    samelgth "The two strings must have the same number of characters"
    rschkargs "Calling regsub gives an error; check the arguments"
    emptyrmeth "Renaming method cannot be void"
    cantwrtdefs "Cannot write file with user definitions"
    xcantbey "%s cannot be %s"
    mustselftfam "A font family must be selected"
    badpluginwhere "Bad plug-in \"where\" argument; must be list of triples"
    badpluginparam "Bad plug-in parameters: list of NAME EXPR, NAME starts with _"
    badpluginparamexec "When running plug-in %s, bad parameter: %s"
    pluginfailed "Plug-in %s failed with message: %s"
    loginto "Login %s"
    cantwrtdir "Cannot write on %s; please change its permissions"
    cantcleandir "Could not clear directory %s"
    defTERMCMD "The option for the command that opens a terminal window must be defined first"
    gpOutfile "Output is in file \"%s\" in your working directory."
    gpQuitgnuplot "Press Enter in xterm window to quit gnuplot."
    gpModgnuplot "Modify the viewing in the Gnuplot-window\
                  by dragging the mouse (left and middle button).\n\
                  After closing the gnuplot-window you will find\
                  the output in file \"%s\" in your working directory."
    htResult1 "Track=\"%s\""
    htResult2 "Total distance=%s  Total ascent=%s  Total descent=%s"
    htResult3 "Used total time=%s  Resting Time=%s(%s)  Time in motion=%s"
    htResult4 "%s  Cap.plain=%s  Cap.asc=%s  Cap.desc=%s  Est. hiking time=%s"
    srResult1 "Rest periods (>5min) of track \"%s\""
    badcumuls "Difference in altitude %s > total ascent %s and/or descent %s; altitude threshold %s %s option should be changed"
}

set TXT(RTcompflds) "# WP {$DTUNIT} deg {$ALUNIT} stage label"
set TXT(TRcompflds) \
    "TP {} {} {} {$ALUNIT} {$DTUNIT} {$DTUNIT} h:m:s {$SPUNIT} deg"
set TXT(starttoend) "Start: to end %s $DTUNIT;"
set TXT(startmax) "max %s $DTUNIT;"
set TXT(WPnearflds) "WP {$DTUNIT} deg"
set TXT(within) "Within (${DTUNIT}s)"
set TXT(between) "Between (${DTUNIT}s)"

array set TXT {
    GMtit   "GPS Manager - version"
    exit   Exit
    map   Map
    load   Load
    loadfrm   "Load from"
    save   Save
    saveels "Save elements"
    saveto   "Save to"
    clear   Clear
    clearall   "Clear All"
    newWP   "New WP"
    newRT   "New RT"
    newTR   "New TR"
    newLN   "New LN"
    newGR   "New GR"
    import   Import
    importfrm   "Import from"
    export   Export
    exportels "Export elements"
    exportto   "Export to"
    count   Count
    trueN   "True North"
    automagn   "Auto Magnetic"
    usrdef   "User Defined"
    nameWP   WPoint
    nameRT   Route
    nameTR   Track
    nameLN   Line
    nameLP   "Line Point"
    nameGR   Group
    namePlot   Plot
    nameMap   Map
    nameRTComp   "Route Computation"
    nameTRComp   "Track Computation"
    nameInfo Information
    GPSrec   "GPS receiver"
    turnoff   "Turn Off"
    get   Get
    put   Put
    all   All
    select   Select
    selection   Selection
    options   Options
    DMS   DMS
    DMM   DMM
    DDD   DDD
    GRA   Grades
    UTM/UPS   UTM/UPS
    MH    MH
    message   Message
    cancel   Cancel
    file   File
    ovwrt   Overwrite
    app   Append
    online   online
    offline   offline
    check   check
    create   Create 
    revert   Revert
    colour   colour
    Colour   Colour
    grey   grey
    mono   mono
    portr   portrait
    landsc   landscape
    legend   Legend
    incscale   "Include scale"
    more   More
    waypoint   Waypoint
    name   Name
    created   Created
    cmmt   Comment
    withWP  "With this WP:"
    displ   "Display on map"
    startRT "Start RT"
    route   Route
    number   Number
    numberid "Number/Id"
    insb   "Insert before"
    insa   "Insert after"
    del   Delete
    repl   "Replace by"
    comp   Compute
    RTcomp   "Route Computation"
    savecomp "Save computation"
    totdst   "Total distance"
    tottime   "Total time"
    totdstng   "Total distance, no gaps"
    tottimeng  "Total time, no gaps"
    totresttime  "Total time of resting (>5min)"
    avgspmot  "Average speed in motion"
    track   Track
    chophd   "Chop head"
    choptl   "Chop tail"
    incb   "Include before"
    date   Date
    newdate   "New date for next point"
    endprTR   "End of previous track"
    begnxt   "Beginning of next"
    date1st   "Date for 1st point of next"
    TRcomp   "Track Computation"
    avgsp   "Average speed"
    maxsp   "Max speed"
    minsp   "Min speed"
    lat   Lat
    long   Long
    ze   ZE
    zn   ZN
    eastng   Easting
    nrthng   Northing
    zone   Zone
    change   Change
    forget   Forget
    others   Others
    opt_Interf  "User interface"
    optLANG   Language
    optISOLATIN1   "Compose chars"
    optDELETE   "DEL deletes last char"
    optMWINDOWSCONF  "Main window"
    optSERIALBAUD "Baud Rate"
    optGPSREC   "GPS Model"
    opt_GPSRecConf   "Receiver parameters"
    optACCEPTALLCHARS "Accept all characters"
    optNAMELENGTH   "Max name length"
    optINTERVAL "Sampling interval"
    optCOMMENTLENGTH   "Max comment length"
    optMAXWPOINTS   "Max # WPoints"
    optMAXROUTES   "Max # Routes"
    optMAXWPINROUTE   "Max # WPs in Route"
    optMAXTPOINTS   "Max # Trackpoints"
    optCREATIONDATE   "Rec has creation date"
    optNOLOWERCASE   "Rec has no lower case"
    optDEFAULTSYMBOL "Default WP symbol"
    optDEFAULTDISPOPT "Default WP display option"
    opt_Data "Data"
    optEQNAMEDATA "Data with same name"
    optKEEPHIDDEN "Keep hidden data"
    optDatum   Datum
    optTimeOffset   "Time offset"
    optACCFORMULAE "Accurate formulae"
    optASKPROJPARAMS "Confirm proj parameters"
    optBalloonHelp "Balloon help"
    optTRNUMBERINTVL "Show TP info on map at each"
    optTRINFO "TP info to show"
    opt_Formats   "Units and formats"
    optDISTUNIT   "Distance"
    optALTUNIT "Altitude"
    optUSESLOWOPWINDOW "Window to control slow operations"
    optDEFTRECPROTOCOL "Default protocol"
    optLNSREACT "Mapped LNs react to mouse"
    M     m
    KM    km
    NAUTMILE    "nautical mile"
    STATMILE    "statute mile"
    FT    ft
    optPositionFormat   "Position format"
    optDateFormat   "Date format"
    opt_Geom   "Window geometry"
    opt_MapGeom   "Map geometry"
    optMAPWIDTH   "Map width"
    optMAPHEIGHT   "Map height"
    optMAPSCLENGTH   "Map scale length"
    optMAPSCALE   "Map scale"
    optMAXMENUITEMS   "Max # menu items"
    optLPOSX   "Lists window x-pos"
    optLPOSY   "Lists window y-pos"
    optMPOSX   "Map window x-pos"
    optMPOSY   "Map window y-pos"
    optRPOSX   "Rec window x-pos"
    optRPOSY   "Rec window y-pos"
    optEPOSX   "Error window x-pos"
    optEPOSY   "Error window y-pos"
    optDPOSX   "Dialog x-pos"
    optDPOSY   "Dialog y-pos"
    optDPOSRTMAP "RT/map dialog offset"
    optLISTWIDTH   "List width"
    optLISTHEIGHT   "List height"
    optCOLOUR   Colours
    optCOLOUR,fg   Foreground
    optCOLOUR,bg   Background
    optCOLOUR,messbg   "Error background"
    optCOLOUR,confbg   "Confirmation background"
    optCOLOUR,selbg   "Selection background"
    optCOLOUR,dialbg   "Input background"
    optCOLOUR,offline   "Receiver offline"
    optCOLOUR,online   "Receiver online"
    optCOLOUR,check   "Selected checkbutton"
    optCOLOUR,ballbg   "Balloon help background"
    optCOLOUR,ballfg   "Balloon help foreground"
    optMAPCOLOUR  "Map colours"
    optMAPCOLOUR,mapsel   "Selected map item"
    optMAPCOLOUR,WP   "Wayponts on map"
    optMAPCOLOUR,RT   "Routes on map"
    optMAPCOLOUR,mkRT "Route defined on map"
    optMAPCOLOUR,TR   "Tracks on map"
    optMAPCOLOUR,TP   "Track points on map"
    optMAPCOLOUR,LN   "Lines on map"
    optMAPCOLOUR,mapleg   "Map legends"
    optMAPCOLOUR,anim  "Animation on map"
    optMAPCOLOUR,emptygrid  "Empty image"
    optMAPCOLOUR,fullgrid   "Existing image"
    opt_Fonts "Fonts"
    optDEFTRTWIDTH "RT line width"
    optDEFTTRWIDTH "TR line width"
    optDEFTLNWIDTH "LN width"
    opt_Files "Device and files"
    optDEFSPORT "Device"
    optSAVESTATE "Save state on exit"
    optDELSTATE "Delete files after restoring state"
    optPERMS   "File permissions"
    optPRINTCMD "Print command"
    optPAPERSIZE "Paper size"
    optMapGuideVersion "MapGuide version"
    optICONSIZE "Icon size"
    red   Red
    green   Green
    blue   Blue
    owner   Owner
    permgroup   Group
    others   Others
    fread   Read
    fwrite   Write
    fexec   Exec
    YYYYMMDD   YYYYMMDD
    MMDDYYYY   MMDDYYYY
    DDMMMYYYY   DDMMMYYYY
    YYYY-MM-DD  YYYY-MM-DD
    YYYY/MM/DD  YYYY/MM/DD
    ISO8601 "ISO8601"
    mainwd   "Main window"
    distazim   "Dist and bearing"
    nearestWPs   "Nearest WPs"
    fromto   "From %s to %s"
    degrees   degrees
    nameWPDistBear   "dist and bearing"
    nameWPNearest   "nearest WPs"
    inrect   "In rectangle"
    forthisWP   "for this WP"
    formappedWPs   "for mapped WPs"
    group   Group
    element   Element
    insert   Insert
    joinGR   "Add GR Els"
    TRtoRT   "Conversion from TR to RT"
    TRtoLN   "Conversion from TR to LN"
    TRtoTR   "TR simplification"
    TRRTnpoints   "No. points to keep"
    TRlinedispl   "Display result now"
    TRTRdispl   "Display TR now"
    WP   WP
    RT   RT
    TR   TR
    TP   TP
    LN   LN
    LP   LP
    GR   GR
    LAP  LAP
    commrec   "Communication with receiver"
    abort   Abort
    ACKs   ACKs
    NAKs   NAKs
    packets   packets
    unnamed   "(unknown)"
    fromTR    "From TR: %s"
    mapload "Geo-referencing image"
    loadmback Load
    savemback "Save geo-ref info"
    chgmback Change
    clearmback Clear
    backgrnd Background
    nameMapBkInfo "Background info"
    nameMapInfo "Map settings"
    mpbkchg "Change background"
    mpbkgrcs "Grid position"
    nameImage Image
    symbol Symbol
    SY1st_aid "First aid"
    SYCATaviation Aviation
    SYCATgeneral "General use"
    SYCATland Land
    SYCATwater Water
    SYMOB "Man over board"
    SYRV_park "Recreational Vehicle park"
    SYWP_buoy_white "Buoy, white"
    SYWP_dot "WP"
    SYairport "Airport"
    SYamusement_park "Amusement park"
    SYanchor "Anchor"
    SYanchor_prohib "Anchor prohibited"
    SYavn_danger "Danger (avn)"
    SYavn_faf "1st approach fix"
    SYavn_lom "Localizer outer marker"
    SYavn_map "Missed approach point"
    SYavn_ndb "ND beacon"
    SYavn_tacan "TACAN"
    SYavn_vor "VHF omni-range"
    SYavn_vordme "VOR-DME"
    SYavn_vortac "VOR/TACAN"
    SYbait_tackle "Bait and tackle"
    SYball "Ball"
    SYbeach "Beach"
    SYbeacon "Beacon"
    SYbell "Bell"
    SYbiker "Biker"
    SYboat "Boat"
    SYboat_ramp "Boat ramp"
    SYborder "Border crossing"
    SYbot_cond "Bottom conditions"
    SYbowling "Bowling"
    SYbox_blue "Box, blue"
    SYbox_green "Box, green"
    SYbox_red "Box, red"
    SYbridge "Bridge"
    SYbuilding "Building"
    SYbuoy_amber "Buoy, amber"
    SYbuoy_black "Buoy, black"
    SYbuoy_blue "Buoy, blue"
    SYbuoy_green "Buoy, green"
    SYbuoy_green_red "Buoy, green red"
    SYbuoy_green_white "Buoy, green white"
    SYbuoy_orange "Buoy, orange"
    SYbuoy_red "Buoy, red"
    SYbuoy_red_green "Buoy, red green"
    SYbuoy_red_white "Buoy, red white"
    SYbuoy_violet "Buoy, violet"
    SYbuoy_white "Buoy, white"
    SYbuoy_white_green "Buoy, white green"
    SYbuoy_white_red "Buoy, white red"
    SYcamping "Camp site"
    SYcapitol_city "City, star"
    SYcar "Car"
    SYcar_rental "Rent-a-car"
    SYcar_repair "Car repair"
    SYcasino "Casino"
    SYcastle "Castle"
    SYcemetery "Cemetery"
    SYchapel "Chapel"
    SYchurch "Church"
    SYcircle_blue "Circle, blue"
    SYcircle_green "Circle, green"
    SYcircle_red "Circle, red"
    SYcircle_x "Circled X"
    SYcivil "Civil location"
    SYcntct_afro "Afro"
    SYcntct_alien "Alien"
    SYcntct_ball_cap "Ball cap"
    SYcntct_big_ears "Big ear"
    SYcntct_biker "Biker"
    SYcntct_bug "Bug"
    SYcntct_cat "Cat"
    SYcntct_dog "Dog"
    SYcntct_dreads "Dreads"
    SYcntct_female1 "Female 1"
    SYcntct_female2 "Female 2"
    SYcntct_female3 "Female 3"
    SYcntct_goatee "Goatee"
    SYcntct_kung_fu "Kung fu"
    SYcntct_pig "Pig"
    SYcntct_pirate "Pirate"
    SYcntct_ranger "Ranger"
    SYcntct_smiley "Smiley"
    SYcntct_spike "Spike"
    SYcntct_sumo "Sumo"
    SYcoast_guard "Coast guard"
    SYcontrolled "Controlled Area"
    SYcross "Cross hair"
    SYcross_3p "Cross hair 3p"
    SYcrossing "Crossing"
    SYdam "Dam"
    SYdanger "Danger"
    SYdeer "Deer"
    SYdiamond_blue "Diamond, blue"
    SYdiamond_green "Diamond, green"
    SYdiamond_red "Diamond, red"
    SYdiver_down_1 "Diver down 1"
    SYdiver_down_2 "Diver down 2"
    SYdock "Dock"
    SYdollar "Dollar"
    SYdot "Dot"
    SYdrinking_water "Drinking water"
    SYdropoff "Dropoff"
    SYduck "Duck"
    SYelevation "Elevation"
    SYexit "Exit"
    SYexit_no_serv "Exit, no services"
    SYfactory "Factory"
    SYfastfood "Fast food"
    SYfhs_facility "FHS facility"
    SYfish "Fish"
    SYfitness "Fitness"
    SYflag "Flag"
    SYflag_pin_blue "Flag pin, blue"
    SYflag_pin_green "Flag pin, green"
    SYflag_pin_red "Flag pin, red"
    SYfreeway "Freeway"
    SYfuel "Fuel"
    SYfuel_store "Fuel & store"
    SYgeo_name_land "Geo name, land"
    SYgeo_name_man "Geo name, man-made"
    SYgeo_name_water "Geo name, water"
    SYgeocache "Geocache"
    SYgeocache_fnd "Geocache found"
    SYglider "Glider"
    SYgolf "Golf"
    SYheliport "Heliport"
    SYhorn "Horn"
    SYhouse "House"
    SYhouse_2 "House 2"
    SYhydrant "Water hydrant"
    SYice_skating "Ice skating"
    SYinfo "Info"
    SYintersection "Intersection"
    SYis_highway "Highway"
    SYknife_fork "Food"
    SYladder "Ladder"
    SYlanding "Landing"
    SYlarge_city "City, large"
    SYlarge_exit_ns "Exit no serv, large"
    SYlarge_ramp_int "Ramp int, large"
    SYletter_a_blue "A, blue"
    SYletter_a_green "A, green"
    SYletter_a_red "A, red"
    SYletter_b_blue "B, blue"
    SYletter_b_green "B, green"
    SYletter_b_red "B, red"
    SYletter_c_blue "C, blue"
    SYletter_c_green "C, green"
    SYletter_c_red "C, red"
    SYletter_d_blue "D, blue"
    SYletter_d_green "D, green"
    SYletter_d_red "D, red"
    SYlevee "Levee"
    SYlight "Light"
    SYlodging "Lodging"
    SYmany_fish "Fish bank"
    SYmany_tracks "Many tracks"
    SYmarina "Marina"
    SYmark_x "Mark, x"
    SYmedium_city "City, medium"
    SYmile_marker "Mile marker"
    SYmilitary "Military location"
    SYmine "Mine"
    SYmonument "Monument"
    SYmountains "Mountains"
    SYmovie "Movie"
    SYmug "Mug"
    SYmuseum "Museum"
    SYntl_highway "National highway"
    SYnull "(transparent)"
    SYnull_2 "(void)"
    SYnumber_0_blue "0, blue"
    SYnumber_0_green "0, green"
    SYnumber_0_red "0, red"
    SYnumber_1_blue "1, blue"
    SYnumber_1_green "1, green"
    SYnumber_1_red "1, red"
    SYnumber_2_blue "2, blue"
    SYnumber_2_green "2, green"
    SYnumber_2_red "2, red"
    SYnumber_3_blue "3, blue"
    SYnumber_3_green "3, green"
    SYnumber_3_red "3, red"
    SYnumber_4_blue "4, blue"
    SYnumber_4_green "4, green"
    SYnumber_4_red "4, red"
    SYnumber_5_blue "5, blue"
    SYnumber_5_green "5, green"
    SYnumber_5_red "5, red"
    SYnumber_6_blue "6, blue"
    SYnumber_6_green "6, green"
    SYnumber_6_red "6, red"
    SYnumber_7_blue "7, blue"
    SYnumber_7_green "7, green"
    SYnumber_7_red "7, red"
    SYnumber_8_blue "8, blue"
    SYnumber_8_green "8, green"
    SYnumber_8_red "8, red"
    SYnumber_9_blue "9, blue"
    SYnumber_9_green "9, green"
    SYnumber_9_red "9, red"
    SYoil_field "Oil field"
    SYopen_24hr "Open 24 hours"
    SYoval_blue "Block, blue"
    SYoval_green "Block, green"
    SYoval_red "Block, red"
    SYparachute "Parachute"
    SYpark "Park"
    SYparking "Parking"
    SYpharmacy "Pharmacy"
    SYphone "Phone"
    SYpicnic "Picnic"
    SYpin_blue "Pin, blue"
    SYpin_green "Pin, green"
    SYpin_red "Pin, red"
    SYpizza "Pizza"
    SYpolice "Police"
    SYpost_office "Post-office"
    SYprivate "Private field"
    SYradio_beacon "Radio beacon"
    SYramp_int "Ramp intersection"
    SYrect_blue "Block, blue"
    SYrect_green "Block, green"
    SYrect_red "Block, red"
    SYreef "Reef"
    SYrestricted "Restricted Area"
    SYrestrooms "WC"
    SYscenic "Scenic"
    SYschool "School"
    SYseaplane "Seaplane base"
    SYshopping_cart "Shopping"
    SYshort_tower "Tower, short"
    SYshowers "Showers"
    SYskiing "Water skiing"
    SYskull "Skull"
    SYsmall_city "City, small"
    SYsnow_skiing "Snow skiing"
    SYsoft_field "Soft field"
    SYsquare_blue "Square, blue"
    SYsquare_green "Square, green"
    SYsquare_red "Square, red"
    SYst_highway "State highway"
    SYstadium "Stadium"
    SYstore "Store"
    SYstreet_int "Street intersection"
    SYstump "Stump"
    SYsummit "Summit"
    SYswimming "Swimming"
    SYtake_off "Take-off"
    SYtall_tower "Tower, tall"
    SYtheater Theater
    SYtide_pred_stn "Tide/current pred station"
    SYtoll Toll
    SYtow_truck "Tow truck"
    SYtraceback "Trace-back"
    SYtracks Tracks
    SYtrail_head "Trail head"
    SYtree "Tree"
    SYtriangle_blue "Triangle, blue"
    SYtriangle_green "Triangle, green"
    SYtriangle_red "Triangle, red"
    SYtruck_stop "Truck stop"
    SYtunnel "Tunnel"
    SYultralight "Ultralight"
    SYus_highway "US highway"
    SYweedbed "Weedbed"
    SYweight_station "Weight station"
    SYwreck "Wreck"
    SYzoo "Zoo"
    psvisible "Only visible part"
    DISPsymbol "Symbol only"
    DISPs_name "S & name"
    DISPs_comment "S & comment"
    DISPname "Name only"
    DISPcomment "Comment only"
    dispopt Display
    mapitems "Display items on map"
    nameIC Icon
    prod Product
    WPCapac "Waypoint Capacity"
    ICCapac "Icon Capacity"
    RTCapac "Route Capacity"
    TRCapac "Track Capacity"
    protcl "Protocol"
    ICGraph "Icon Graphics"
    WPperRT "Waypoints per Route"
    notinGR "not in (sub-)group"
    onlyinGR "only in (sub-)group"
    loadgrels "Load elements"
    importgrels "Import elements"
    about "On GPSMan..."
    contrib "With contributions by"
    errorsto "Error reports to:"
    obsTRToRT "WPs created by a TR to RT conversion"
    obsTRsimpl "TR resulting from simplification"
    nameLists "Lists"
    nameData "Data"
    MWCmap "Map"
    MWClists "Lists"
    MWCsingle "Single window"
    search "Search"
    rmrk "NB"
    closeto "Close to"
    with "With"
    srchres "FOUND"
    database "Database"
    where "Where"
    what "What"
    list "list"
    distance "Distance"
    fromWP "from Waypont"
    fromPos "from position"
    azimuth "Bearing"
    any "any"
    opening "Opening"
    suggested "suggested"
    another "Another"
    srchdd1 "Search on"
    srchdd2Data "all items"
    srchdd2GR "Group(s)"
    from "from"
    started "starting on"
    transf  "Coords Transf"
    TRNSFAffine    "Affine"
    TRNSFAffineConf  "Aff Conformal"
    TRNSFNoRot      "Conf No Rot"
    projection "Projection"
    lat0  "Lat of centre"
    long0 "Long of centre"
    lat1  "Lat 1st st parallel"
    lat2  "Lat 2nd st parallel"
    latF  "Lat false origin"
    longF "Long false origin"
    k0 "Scale factor"
    PRJUTM "UTM/UPS"
    PRJTM "Transverse Mercator"
    PRJBMN "Austrian BMN Grid"
    PRJBNG "British National Grid"
    PRJBWI "British West Indies"
    PRJCMP "Portuguese Mil Map"
    PRJCTR "Carta Tecnica Reg (I)"
    PRJITM "Irish Transv Mercator"
    PRJGKK "German Grid"
    PRJLCC1 "Lambert Conic Conf 1"
    PRJLCC2 "Lambert Conic Conf 2"
    PRJKKJP "Basic Finnish Grid"
    PRJKKJY "Uniform Finnish Grid"
    PRJSEG "Swedish Grid"
    PRJMerc1 "Mercator 1"
    PRJMerc2 "Mercator 2"
    PRJCS "Cassini-Soldner"
    PRJAPOLY "American Polyconic"
    PRJStereogr Stereographic
    PRJTWG "Taiwan Grid"
    PRJSOM "Swiss Oblique Mercator"
    PRJLV03 "Swiss LV03 Grid"
    PRJIcG "Iceland Grid"
    PRJRDG "The Netherlands Grid"
    PRJSchreiber Schreiber
    PRJSphMerc "Spherical Mercator"
    PRJEqCyl "Equidistant Cylindrical"
    PRJEPSG:3857 "EPSG:3857"
    PRJEPSG:32663 "EPSG:32663"
    dontaskagain "Stop asking"
    rename "Use new name"
    oname "Original name"
    never "Never"
    ask "Ask"
    always "Always"
    stage "Stage"
    label "Label"
    alt "Altitude"
    locate "Locate"
    animation  Animation
    fast Fast
    slow Slow
    start Start
    pause Pause
    speed Speed
    centred "Keep centred"
    state State
    animinit "at start/end"
    animon "running"
    animpause "paused"
    animabort "aborting"
    realtimelog "Real-time track log"
    garmin Garmin
    garmin_usb "Garmin USB"
    nmea "NMEA 0183"
    stext "Simple Text"
    simul "simulator"
    lowrance Lowrance
    magellan Magellan
    getlog "Get Log"
    stop Stop
    dolog Record
    show Show
    hide Hide
    posfixerror error
    posfix_  ?
    posfix2D 2D
    posfix3D 3D
    posfix2D-diff "2D d"
    posfix3D-diff "3D d"
    posfixGPS GPS
    posfixDGPS DGPS
    posfixAuto ok
    posfixsimul simul
    restart Restart
    mkTR "Make TR"
    PVTflds "# t lat long alt fix EPE EPH EPV vel_x vel_y vel_z TRK"
    namePVTData "Log data"
    mkavgWP "Make avg WP"
    move Move
    define Define
    open Open
    defs "Definitions"
    baseproj "Base projection"
    abbrev "Short name"
    grid Grid
    use Use
    unit Unit
    feasting "False easting"
    fnorthing "False northing"
    bounds Bounds
    max Max
    min Min
    easting Easting
    northing Northing
    fixeddatum "fixed datum"
    elevation Elevation
    usewps "Use WPs"
    chgpfrmt "Change position format"
    here Here
    atprevwp "At previous WP"
    prevwp "Previous WP"
    firstwp "First WP"
    chglstrs "Edit previous stage"
    chgnxtrs "Edit next stage"
    contnend "Add to end"
    closemenu "Close menu"
    ellpsd Ellipsoid
    datum Datum
    userdefs "User definitions"
    edmap "Edit on map"
    actual actual
    rtimelogintv "Log interval"
    inca "Include after"
    invert "Invert"
    recwindow "Receiver window"
    volume "Volume"
    latS "Lat S"
    latN "Lat N"
    longW "Long W"
    longE "Long E"
    no No
    computations Computations
    comparea "Compute area"
    cluster Cluster
    centre  Centre
    mkclusters "Make clusters"
    quadr Quadrangle
    dlat "Latitude range"
    dlong "Longitude range"
    collcntr "Collecting centres..."
    compclstr "Computing clusters..."
    crtgclstrgrs "Creating groups..."
    chgdatum "Change datum..."
    print Print
    prevctr "Previous centre"
    printopt "Print options"
    cwpsdef "Control WPs to define now"
    nextTP "Next TP"
    generate Generate
    generateall "Generate all"
    width Width
    simplTRto "Simplify to"
    exstglog "Existing log"
    contnsly "Continuously"
    animate "animate"
    animabbrev "anim."
    noanabbr "no anim."
    zelev "Z-scale"
    xyelev "XY-scale"
    notext "no text"
    view "View"
    sideview "Side-view"
    persptv "Perspective"
    optMAPCOLOUR,trvtrk "TRK arrow"
    optMAPCOLOUR,trvtrn "TRN arrows"
    optMAPCOLOUR,trvcts "CTS arrow"
    optMAPCOLOUR,trvcts2 "2nd CTS arrow"
    optMAPCOLOUR,trvvel_z "Up/down arrow"
    optMAPCOLOUR,trvwrnimportant "Important warning (nav)"
    optMAPCOLOUR,trvwrnnormal "Warning (nav)"
    optMAPCOLOUR,trvwrninfo "Information (nav)"
    travel Travel
    notravel "Stop travelling"
    travdisplay "Configure display"
    travchgdisplay "Change to display %s"
    travdsetup "Configure travel display"
    navMOB "MOB: Man Over Board!"
    startnav "Navigate"
    navWP "Go to WP"
    goback "Go back"
    follow "Follow %s"
    exactly "exactly"
    fromnrst "from nearest"
    inrvrs "in reverse"
    forgetgoal "Forget goal"
    suspend "Suspend"
    resume "Resume"
    TRVfix "Fix"
    TRVhour "Time"
    TRVspeed "Speed"
    TRVpos "Pos"
    TRValt "Alt"
    TRVtrk "TRK"
    TRVnxtWP "To"
    TRVprvWP "From"
    TRVete "ETE"
    TRVeta "ETA"
    TRVvmg "VMG"
    TRVxtk "XTK"
    TRVcts "CTS"
    TRVtrn "TRN"
    TRVvel_z "V speed"
    TRVtrkcts "TRK, CTS"
    TRVdist "Dist"
    TRVc_trkcts "TRK/CTS arrows"
    TRVc_trn "TRN arrows"
    TRVc_vel_z "Up/down arrow"
    add "Add"
    addlabelled "Add with label"
    remove "Remove"
    mindist "Arrival distance"
    chggoal "Change to next goal"
    chginggoal "Changing to next goal"
    chggoalhlp "When to change from\ncurrent to next goal\nwhen following RT/TR"
    soon soon
    late late
    warnings "Warnings"
    dowarn "Issue warnings"
    warnconf "Configure warnings"
    priority Priority
    high high
    medium medium
    low low
    warnprox "Distance to WP <"
    warnanchor "Distance to WP >"
    warnspeed "Speed >"
    warntrn "TRN (abs)>"
    warnvspeed "Vertical speed"
    warnxtk "XTK (abs)>"
    trvhlpbox "Use right-button to reorder elements below"
    trvhlpbxs "Use right-button to reorder list elements"
    trvwarrv "Arriving at %s!"
    trvwleave "Leaving %s!"
    trvwspeed "Speed > %s!"
    trvwtrn "TRN > %s!"
    trvwvspeed "Vert speed not in [%s,%s]!"
    trvwxtk "XTK > %s!"
    trvwnolog "Real-time logging is off!"
    trvwnopos "Previous positions unavailable"
    trvwuwps "RT has undefined WP(s)"
    trvwchg "Going now to %s"
    drivesim "driving simulator"
    startfrom "Start from..."
    outofctrl "Out of control!"
    right Right
    left Left
    straight Straight
    rthlpdsim "Arrow keys: steer, change speed\nSpace bar: straight"
    hiddendata "Hidden data"
    Ghidden_class Class
    Ghidden_subclass Subclass
    Ghidden_lnk_ident "Stage id"
    Ghidden_colour Colour
    Ghidden_attrs Attributes
    Ghidden_depth Depth
    Ghidden_state State
    Ghidden_country Country
    Ghidden_facility Facility
    Ghidden_city City
    Ghidden_addr Address
    Ghidden_int_road "Intersection road"
    Ghidden_dtyp "Display opt+type"
    Ghidden_ete "ETE"
    Ghidden_display "Display?"
    Ghidden_yes Yes
    Ghidden_no No
    Ghidden_user "User"
    Ghidden_user_symbol "User (symbol only)"
    Ghidden_non_user "Non-user"
    Ghidden_avn_airport "Airport"
    Ghidden_avn_inters "Avn intersection"
    Ghidden_avn_NDB "NDB"
    Ghidden_avn_VOR "VOR"
    Ghidden_avn_airp_rway "Airport runway threshold"
    Ghidden_avn_airp_int "Airport intersection"
    Ghidden_avn_airp_NDB "Airport NDB"
    Ghidden_map_pt "Map point"
    Ghidden_map_area "Map area"
    Ghidden_map_int "Map intersection"
    Ghidden_map_addr "Map address"
    Ghidden_map_line "Map line"
    Ghidden_locked Locked
    Ghidden_default "Default"
    Ghidden_black Black
    Ghidden_white White
    Ghidden_red Red
    Ghidden_dark_red "Dark red"
    Ghidden_green Green
    Ghidden_dark_green "Dark green"
    Ghidden_blue Blue
    Ghidden_dark_blue "Dark blue"
    Ghidden_yellow Yellow
    Ghidden_dark_yellow "Dark yellow"
    Ghidden_magenta Magenta
    Ghidden_dark_magenta "Dark magenta"
    Ghidden_cyan Cyan
    Ghidden_dark_cyan "Dark cyan"
    Ghidden_light_gray "Light grey"
    Ghidden_dark_gray "Dark grey"
    Ghidden_transparent Transparent
    Ghidden_line Line
    Ghidden_link Stage
    Ghidden_net Net
    Ghidden_direct Direct
    Ghidden_snap Snap
    Ghidden_temp Temperature
    Ghidden_time "Time stamp"
    Ghidden_cat Category
    renres "RENAMED"
    forgetGRcs "Forget GR&Els"
    optDEFMAPPROJ "Map projection"
    optDEFMAPPFRMT "Map coordinates"
    optDEFMAPPFDATUM "Map coordinates datum"
    undo Undo
    UTMzone zone
    tfwfile  "TFW file"
    ok Ok
    newWPatdb "New WP at..."
    PRJLamb93 "Lambert 93"
    PRJLambNTFe "Lambert NTF IIet"
    PRJLambNTF "Lambert NTF"
    NTFzone zone
    loop Loop
    crtLN "Create LN"
    PRJAEA "Albers Equal Area"
    PRJLEAC "Lambert Eq Area Conic"
    PRJTAlbers "Teale Albers"
    polasp "Polar aspect"
    north North
    south South
    distunit "Distance unit"
    altunit "Altitude unit"
    params "Parameters"
    dimens "Dimensions"
    version "Version"
    opinprogr "Operation in progress"
    working "Working"
    aborted "Aborted!"
    errwarn "Error/warning(s)!"
    changegroupsymbol "Change symbol"
    ozimapfile "Ozi Map file"
    info "Information"
    climbrate  "Climb rate"
    BGAfeature  "Feature"
    BGAfindblty "Findability"
    BGAairact   "Air activity"
    dispitems "Displayed items"
    hiditems "Hidden items"
    mkgrp "Make Group"
    optAutoNumRts "Auto-number RTs when putting"
    numberfrom0 "Set counter to 1"
    items "Items"
    optSUPPORTLAPS "Support for laps"
    nameLAP "Lap"
    duration "Duration"
    calrs "Calories"
    syusrmenu "Custom symbol menu"
    cfgsymenu "Configure custom symbol menu"
    insmnb   "Insert sub-menu before"
    insmna   "Insert sub-menu after"
    opensbmn "Open sub-menu"
    clssbmn  "Close sub-menu"
    syhlpbx "Use right-button to\nreorder list elements"
    lapsrun Run
    fromfile "from file"
    fromdef "from definition"
    mbaktoload "Background to load"
    none "none"
    nameAL "Almanac"
    alm_svid "satellite id"
    alm_week "week"
    alm_datatime "data ref time"
    alm_clockc1 "clock corr coeff s"
    alm_clockc2 "clock corr coeff s/s"
    alm_ecc "eccentricity"
    alm_sqrta "sqrt a"
    alm_mnanom "mean anomaly"
    alm_argprgee "arg perigee"
    alm_rightasc "right ascension"
    alm_rtrightasc "rate r ascension"
    alm_inclin "inclination"
    alm_health "health"
    lstsqs  "Least Squares"
    lstsqsfile "Least Squares file"
    mapfitWPs "Display fit WPs on map"
    showfitinfo "Show fit information"
    xtcoord "xt"
    ytcoord "yt"
    delta   "d"
    residual "rt"
    rmsxydev "rms(x,y-deviations)"
    resstderr "residual std error"
    chgdev "Change device"
    alt_cumula "Total ascent"
    alt_cumuld "Total descent"
    maxalt "Maximum altitude"
    minalt "Minimum altitude"
    optSHOWFILEITEMS "By default display on map items read in"
    vertgridlines "Vert Grid Lines"
    convert  "Convert"
    split    "Split"
    bysel  "by selected points"
    byseg  "by segments"
    openits "Open %s"
    PRJEOV  EOV
    chgname "Change name"
    clicktoedit "Click to edit"
    renamethod "Renaming method"
    operators "Operators"
    keep1st "keep first char"
    keep1st_hlp "Keep first char\nfurther operations act on remaining chars"
    reset "reset"
    reset_hlp "Restart with initial name undoing any previous changes"
    case "change case"
    case_hlp "All letters to upper/lower case"
    maxlgth "max length"
    maxlgth_hlp "Current chars trimmed to given number"
    inslt "insert at left"
    inslt_hlp "Given string to be inserted before current left char"
    insrt "insert at right"
    insrt_hlp "Given string to be inserted at the end"
    replc "replace"
    replc_hlp "Any char in the first string will be replaced by\nthe corresponding one in the second"
    delany "delete any"
    delany_hlp "All chars in the given string will be deleted from name"
    rsub "re. substitution"
    rsub_hlp "Give regular expression and substitution specification\nsee the manual for details"
    accifnew "accept if new"
    accifnew_hlp "Accept result if it is a new name"
    guntilnew "append number until new"
    guntilnew_hlp "Try all possible values of the number until a new name is found"
    ndigits "Number of digits"
    gennames "Generating names..."
    rentest_hlp "Test present method on given name"
    renmove_hlp "Use right-button to reorder lines"
    tolower "to lower"
    toupper "to upper"
    applyto "Apply to"
    forall "for all"
    selfont "Selecting font"
    default "Default"
    size "Size"
    units "Units"
    points "points"
    pixels "pixels"
    weight "Weight"
    normal "normal"
    bold "bold"
    slant   "Slant"
    roman "roman"
    italic "italic"
    underline "Underline"
    overstrike "Overstrike"
    optDEFAULTFONT "Default font"
    optFIXEDFONT "Fixed font"
    optMAPFONT "Map font"
    optTRAVELFONT "Travel display font"
    optPLOTFONT "Plot/graph font"
    failed "failed"
    plugin  "Plug-in"
    unavailif  "Unavailable if"
    tclcode  "Tcl code"
    uname "User name"
    pword "Password"
    remember "Remember"
    wptotwitter "WP to Twitter"
    wptotwitternb "Send WP position, datum, altitude, name and comment to Twitter with tags #GPSMan #waypoint"
    exportTFW "Export TFW file"
    nametfwfile "TFW file"
    gpTRtoGnuplot2d "Side-view (Gnuplot)"
    gpTRtoGnuplot2d_nb "Send Track data to external program GNUPLOT and display altitude chart"
    gpTRtoGnuplot3d "Perspective (Gnuplot)"
    gpTRtoGnuplot3d_nb "Send Track data to external program GNUPLOT and display 3D-chart"
    gpParam2d "Parameters for Gnuplot 2D-Chart"
    gpParam3d "Parameters for Gnuplot 3D-Chart"
    gpFilename "Filename"
    gpCanwidth   "Canvas width"
    gpCanheight  "Canvas height"
    gpSTotals  "Show totals"
    gpLeft  "Left side"
    gpRight "Right side"
    gpNone  "None"
    gpSRests "Show rests"
    gpRest  "Rest"
    gpSymText "Symbol+Text"
    gpSymTime "Symbol+Time"
    gpText  "Text"
    gpSHours "Show hours"
    gpTimetop "Time at top"
    gpTimebot "Time at bottom"
    gpSSpeed "Show speed"
    gpHike  "Hike"
    gpBike  "Bike"
    gpDrive "Drive"
    gpGlide "Glide"
    gpLimitDist "Max. Distance WP to Track (km)"
    gpLimitStat "Min. Lenght on Track for same WP (km)"
    gpDirection "Direction"
    gpForward "Forward"
    gpBackward "Backward"
    gp2D_hlp "If there is a groupname identical to the track name,\n\
              the waypoints of this group will be displayed,\n\
              in consideration of the given distances.\n\
              When no matching group name exists,\
              you can choose from a list or cancel."
    gpGenerated "Generated with gnuplot by GPSMan"
    gpModify "Modify viewing with mouse. Quit to save picture."
    htCalcHikeTime "Calculate Hiking Time"
    htCalcHikeTime_nb "Calculate Hiking Time with formula from DAV"
    htHeader "Estimated hiking time (without resting) for tracks\
               using the formula from \"Deutscher Alpenverein\" with different parameters"
    htFooter1 "Capacity refers to one hour."
    htFooter2 "Suggested resting time is 20min after two hours (15%)."
    srShowRest_nb "Show rest periods > 5min."
    srTRTime   "Track time"
    srTotals   "Totals"
    optALTHRESHOLD "Altitude threshold"
    optDISPLAYCMD "Command to display image file"
    optTERMCMD "Command to open terminal window"
    optMANHTTPADDR "User manual HTTP address"
}

    # the following definitions must be coherent with $TXT
array set INVTXT {
    DMS   DMS
    DMM   DMM
    DDD   DDD
    Grades   GRA
    UTM/UPS   UTM/UPS
    MH   MH
    WP   WP
    RT   RT
    TR   TR
    LN   LN
    GR   GR
    LAP  LAP
    m    M
    ft   FT
}

# changes by Miguel Filgueiras to RM contribution
set TXT(srChainage) $TXT(totdst)
set TXT(srShowRest) $TXT(gpSRests)
set TXT(gpSym) $TXT(symbol)
set TXT(srRest) $TXT(gpRest)





