#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: recdefs.tcl
#  Last change:  6 October 2013
#
# Includes contributions by Matt Martin (matt.martin_AT_ieee.org)
#  marked "MGM contribution"
#

# general definitions that may depend on the receiver trade-mark

set TYPES {WP RT TR LN GR}
set PROCTYPES {GMWPoint GMRoute GMTrack GMLine GMGroup}
set MAPTYPES {WP RT TR}

# types of other information that can be got from receiver (e.g., almanacs)
set RECINFO {}

set MAX(WP) $MAXWPOINTS
set MAX(RT) $MAXROUTES
set MAX(TR) $MAXTPOINTS

# GR must be the last element of $RECTYPES list

switch $MYGPS {
    Garmin {
	# RECTYPES and RECINFO may be changed by
	#  proc RecAdjustToProtocols (garmin.tcl)
	#  so they are saved here in ORIGRECTYPES and ORIGRECINFO
	if { $SUPPORTLAPS } {
	    set RECTYPES {WP RT TR LAP GR}
	} else { set RECTYPES {WP RT TR GR} }
	set RECINFO {AL}
	set ORIGRECTYPES $RECTYPES ; set ORIGRECINFO $RECINFO
	# possible baud rates
	set RECBAUDS [list 9600 19200 57600 115200 230400]
	# character set for names
	set RECNAMECHARS(Garmin) {^[-A-Z0-9]*$}
	set RECNAMECHARS(Garmin,mess) "A..Z, 0..9, \"-\""
	set UNDEFDATESECS [DateToSecs 1989 12 31 1 0 0]
    }
    Lowrance {
	set RECTYPES {WP IC RT TR GR}
	set RECNAMECHARS(Lowrance) {^[-A-Z0-9.'()/ ]*$}
	set RECNAMECHARS(Lowrance,mess) \
		"A..Z, 0..9, \"-\", \"'\", \".\", \"/\", \"(\", \")\""
	# default values
	set MAX(IC) 100
	set MAXICG 100
	set UNDEFDATESECS 0
    }
    Magellan {
	# MGM contribution
	set RECTYPES {WP RT TR GR}
	# MF contribution: from recmodels.tcl, added 4800 for Meridian Platinum
	set RECBAUDS [list 4800 9600 19200 57600 115200]
	# character set for names
	set RECNAMECHARS(Magellan) {.*}
	set RECNAMECHARS(Magellan,mess) $TXT(any)
	set UNDEFDATESECS 0
    }
}

## set the options that depend on the receiver

RecModelChange $MYGPS

## user may wish to have no tests...

proc RecCanSendAnything {brand} {
    # use an "accept anything from receiver" setting
    #  $brand in {Garmin, Lowrance, Magellan}
    global ACCEPTALLCHARS RECTYPES MAX MAXICG

    set ACCEPTALLCHARS 1
    foreach t $RECTYPES {
	set MAX($t) 1000000
    }
    foreach x "WPOINTS ROUTES WPINROUTE TPOINTS" {
	global MAX$x

	set MAX$x 10000000
    }
    switch $brand {
	Garmin {
	}
	Lowrance {
	    set MAXICG 1000000
	}
	Magellan {
	}
    }
    return
}
