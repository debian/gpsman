#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: config.tcl
#  Last change:  6 October 2013
#

## the following values can be set from the options dialog of the interface

 # language: see lang*.tcl files, the ENCODED array in metadata.tcl, the
    #   language names and the ALLMONTH array in i18n-utf8.tcl, and the
    #   KNOWNLANGS list in main.tcl
    # currently available: engl for English, port for Portuguese, deutsch for
    #   German, it for Italian, fr for French, nl for Dutch, id for Indonesian,
    #   es for Spanish, ru for Russian

set LANG engl

 # ISO latin1 character composition (accents, cedilla, ...)
set ISOLATIN1 1

 # Delete-key to delete last character
set DELETE 1

 # Configuration of main window in {map, lists} ("single" no longer supported)
set MWINDOWSCONF map

 # GPS receiver trade mark and model (if applicable): give trade mark
 #   (one of: Garmin, Lowrance, Magellan) followed by model name.
 #   See file recmodels.tcl for list of known models

set GPSREC "Garmin"

 # default baud rate for serial port
switch $GPSREC {
    Garmin {
	set SERIALBAUD 9600
    }
    Magellan {
	set SERIALBAUD 115200
    }
}

 # default receiver protocol (only used if $GPSREC=="Garmin"): see possible
 #  values in list RECPROTOCOLS defined for each trade mark in file
 #  recmodels.tcl
set DEFTRECPROTOCOL garmin

 # GPS receiver dependent values (set to Garmin Quest)

       # accept all characters in names/comments; this may be risky!
set ACCEPTALLCHARS 0

set NAMELENGTH 40
set COMMENTLENGTH 40
set MAXWPOINTS 500
set MAXROUTES 50
set MAXWPINROUTE 256
set MAXTPOINTS 10000
set CREATIONDATE 0
set NOLOWERCASE 0

   # DJG contribution: (only used if $GPSREC=="Garmin")
   # whether non-numeric route names should be automatically converted to
   #  numbers when sending them to a receiver requiring numbered routes; this
   #  may overwrite existing routes in the receiver
set AutoNumRts 0
   #--

   # support for laps: only used if $GPSREC=="Garmin"
set SUPPORTLAPS 0

   # sampling interval for Lowrance receivers in seconds
set INTERVAL 3

   # default symbol name (GPSMan convention; see symbols.tcl)
set DEFAULTSYMBOL WP_dot
    #  and symbol display options (GPSMan convention; see symbols.tcl)
set DEFAULTDISPOPT s_name

  # default for whether items read from a file should be displayed on map
set SHOWFILEITEMS 0

  # default line widths for routes, tracks and lines
set DEFTRTWIDTH 2
set DEFTTRWIDTH 2
set DEFTLNWIDTH 1

  # count to show information of track point on map; 0 means no numbers shown
set TRNUMBERINTVL 10
  # what information to show for a track point, one of: number, date
set TRINFO number

  # whether polylines on map react to mouse events (they should not if
  #  they are seen as part of the background)
set LNSREACT 0

 # data items with same name will be overwritten (ovwrt) or created under
 #  new name (rename):
set EQNAMEDATA ovwrt

 # keep hidden data (not available for editing) when changing item;
 #  one of: never, ask (at every change), always
set KEEPHIDDEN never

 # default values

         # possible distance units: KM NAUTMILE STATMILE
set DISTUNIT KM
         # altitude units: M FT
set ALTUNIT M
         # altitude threshold when computing total ascent/descent
         #  in user units; set to 2 when using a barometric altimeter
set ALTHRESHOLD 11

         # possible position formats: DMS (hdeg, min, sec), DMM (hdeg, min),
         #                            DDD (hdeg), UTM/UPS (ze, zn, e, n),
         #                            MH (Maidenhead locator, 6 characters)
         #                            BNG (British grid: zone, e, n),
         #                            CMP (Portuguese military grid: e, n),
         #                            GKK (German grid: zone, e, n),
         #                            ITM (Irish grid: zone, e, n)
         #                            SEG (Swedish grid: e, n)
         #  and maybe other grids (see projections.tcl)
set PositionFormat DMS
set Datum "WGS 84"
         # difference in hours to UTC (must be an integer)
set TimeOffset 1

         # possible date and time formats:
         #                        YYYYMMDD  (1998.03.20 09:35:23),
         #                        YYYY-MM-DD  (1998-03-20 09:35:23),
         #                        ISO8601   (2004-03-25T09:35:23Z),
         #                        MMDDYYYY  (03/20/1998 09:35:23),
         #                        DDMMMYYYY (20-Mar-1998 09:35:23),
         #                        YYYY/MM/DD (1998/03/20-09:35:23)
set DateFormat DDMMMYYYY

  # default map projection and cursor position format when starting
  #  (see projections.tcl)
set DEFMAPPROJ UTM
set DEFMAPPFRMT UTM/UPS
set DEFMAPPFDATUM $Datum

  # formulae: either more accurate (1) or faster (0)
  #  faster should only be used on very slow computers as these
  #  formulae can give rise to large errors when computing
  #  distances
set ACCFORMULAE 1

  # ask for confirmation of projection parameters
set ASKPROJPARAMS 1

  # use window to control slow operations
set USESLOWOPWINDOW 1

  # provide balloons with help
set BalloonHelp 1

  # default version for MapGuide (text) export files
set MapGuideVersion 2002

  # operating system commands considered undefined if set to ""
     # print file
set PRINTCMD ""
     # display image file
set DISPLAYCMD ""
     # open terminal window
set TERMCMD ""

  ## NOT YET IN USE
  # HTTP address of the HTML version of the user manual
  #  default in global DEFTMANHTTPADDR defined in gpsman.tcl
  #  set to "" if unavailable
set MANHTTPADDR $DEFTMANHTTPADDR

 # geometry

  # map dimensions in pixels
set MAPWIDTH 800 ; set MAPHEIGHT 550
         # length of scale arrow (pixels)
set MAPSCLENGTH 300
         # initial map scale: 1, 2, 4, 8, ... 512*$DISTUNIT/$MAPSCLENGTH pixel
         #  or, if $DISTUNIT==km: 50, 100, 200, 500 m/$MAPSCLENGTH pixel
         #      else: 150, 300, 600, 2400 ft/$MAPSCLENGTH pixel
         # actual distance and unit (from the values above) for the scale arrow
set MAPSCALE "4 km"

 # fonts: either "default" or a Tcl/Tk font description
  # default font
set DEFAULTFONT default
  # fixed font
set FIXEDFONT fixed
  # map font
set MAPFONT fixed
  # travel/navigation font
set TRAVELFONT {fixed 12}
  # plot/graph font
set PLOTFONT {fixed 12}

 # size of icons: either 15x15 (pixels) or 30x30
set ICONSIZE 15x15

  # menus
set MAXMENUITEMS 20

  # dimensions of lists
set LISTWIDTH 15
set LISTHEIGHT 22

  # initial positions
     # lists window
set LPOSX 40 ; set LPOSY 25
     # map window
set MPOSX 5 ; set MPOSY 0
     # receiver window
set RPOSX 10 ; set RPOSY 250
     # errors, messages, warnings
set EPOSX 340 ; set EPOSY 50
     # dialogs
set DPOSX 290 ; set DPOSY 50
     # offset for dialog for making route on map
set DPOSRTMAP -200

  # colours
       # general fore/background
set COLOUR(fg) #090980
set COLOUR(bg) #f7f7e0
       # error background
set COLOUR(messbg) #ff8d90
       # confirmation background
set COLOUR(confbg) #F0E9C0
       # selection background
set COLOUR(selbg) #F0E9C0
       # input dialog background
set COLOUR(dialbg) #dbead0
       # receiver offline
set COLOUR(offline) #d83232
       # receiver online
set COLOUR(online) green
       # selected checkbutton
set COLOUR(check) red
       # balloon help
set COLOUR(ballbg) #fef6e1
set COLOUR(ballfg) #8d7f56

  # colours on map
       # selected map item
set MAPCOLOUR(mapsel) red
       # wayponts on map
set MAPCOLOUR(WP) #ff6400
       # routes on map
set MAPCOLOUR(RT) #48C1BC
       # route being defined on map
set MAPCOLOUR(mkRT) #469e36
       # tracks on map
set MAPCOLOUR(TR) #EF9A00
       # track points on map
set MAPCOLOUR(TP) #CD69C9
       # line colour on map
set MAPCOLOUR(LN) #0000AA
       # map legends
set MAPCOLOUR(mapleg) #8B0000
       # lines in map animation
set MAPCOLOUR(anim) darkblue
       # slots in map image grid
set MAPCOLOUR(emptygrid) white
set MAPCOLOUR(selemptygrid) #ffe080
set MAPCOLOUR(fullgrid) #00d8d8
set MAPCOLOUR(selfullgrid) #ffd8d8
       # travel/navigation
       # TRK arrow
set MAPCOLOUR(trvtrk) #ffcc00
       # vertical speed arrows
set MAPCOLOUR(trvvel_z) #ffcc00
       # TRN arrows
set MAPCOLOUR(trvtrn)  red
       # CTS arrow
set MAPCOLOUR(trvcts)  red
       # 2nd CTS arrow
set MAPCOLOUR(trvcts2) #c5fbd5
       # warnings
set MAPCOLOUR(trvwrnimportant) red
set MAPCOLOUR(trvwrnnormal) blue
set MAPCOLOUR(trvwrninfo) darkgreen

 # save state on exit, one of: never, ask (when quitting), always
set SAVESTATE ask
 # delete state files after restoring, one of: never, ask, always
set DELSTATE ask

 # permissions of created files
set PERMS 0640

 # default paper size (one of those whose dimensions are given below)

set PAPERSIZE a4

### the following values can be configured if needs be

  # paper size without margins

set PAGEWIDTH(a4) 184m ; set PAGEHEIGHT(a4) 271m
set PAGEWIDTH(a3) 258m ; set PAGEHEIGHT(a3) 379m
set PAGEWIDTH(a5) 134m ; set PAGEHEIGHT(a5) 195m
set PAGEWIDTH(b4) 250m ; set PAGEHEIGHT(b4) 353m
set PAGEWIDTH(b5) 176m ; set PAGEHEIGHT(b5) 250m
set PAGEWIDTH(letter) 211m ; set PAGEHEIGHT(letter) 279m

  # output formats (in Tcl/Tk syntax, similar to C) for floating-point
  #  coordinates in seconds, minutes, degrees, and grades
  # changes here will not increase the data accuracy!
array set COUTFMT {
    sec    %04.1f
    min    %06.3f
    deg    %.5f
    GRA    %.5f
}

  # whether GPSMan data file can be appended to an existing file when saving
  # WARNING: this may create a file with conflicting time offsets that cannot
  #  be loaded by GPSMan
set APPGMDATAFILE 0

  # echo on password fields, either "none" or a single character
set PASSWDECHO *

  # options used when importing Kismet .network files
  #   types: which Kismet network types should be converted to waypoints
  #   esymbols: symbols (icons) to use for each encryption under each type
  #    list of pairs with type and list of encryption followed by symbols
  #    Tcl glob patterns can be used for the encryption
  #     symbol names must belong to the list SYMBOLS in GPSMan file symbols.tcl
  #   defsymbol: default symbol for encryption values not covered
  #   prename: which prefix to use for names if the ssid is missing or name
  #    is repeated
  #   namenumber: initial number to add to the prefix when forming name; names
  #    created in this way will only be used if they are acceptable, otherwise
  #    a default name generation procedure will be used
array set KISMETOPT {
    types    {infrastructure ad-hoc}
    esymbols {{infrastructure {None tall_tower * radio_beacon}}
              {ad-hoc {* beacon}}}
    defsymbol beacon
    prename   KNET
    namenumber   1
}



