#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: recmodels.tcl
#  Last change:  6 October 2013
#
# Includes contributions by
#  - Brian Baulch (baulchb_AT_onthenet.com.au) marked "BSB contribution"
#  - Matt Martin (matt.martin_AT_ieee.org) marked "MGM contribution"
#  - Miguel Filgueiras (to code by others) marked "MF contribution"
#

  # assuming trade mark is the first list element in $GPSREC
set MYGPS [lindex $GPSREC 0]
set MYGPSMODEL [lreplace $GPSREC 0 0]

# lists of known receivers models

 # Garmin and Lowrance and Magellan: no need for model name
array set RECMODELS {
    Garmin {}
    Lowrance {}
    Magellan {}
}

 # protocols supported for each trade mark
array set RECPROTOCOLS {
    Garmin   {garmin garmin_usb nmea stext simul drivesim}
    Lowrance {lowrance nmea}
    Magellan {magellan}
}

#####
# related procs

proc FillRecModelsMenu {menu commd} {
    # fill in menu with GPS receiver models, used when setting preferences
    #  $menu is the parent menu to fill in
    #  $commd is the callback to associate to each selection
    #   whose arguments will be the receiver name and $menu (even on sub-menus)
    # a selection will also have a callback to change the description of
    #  the options related to the receiver configuration
    global RECMODELS MAXMENUITEMS TXT GPSREC

    foreach tm [lsort -dictionary [array names RECMODELS]] {
	if { $RECMODELS($tm) != "" } {
	    $menu add cascade -label $tm -menu $menu.m
	    set mw $menu.m ; menu $mw -tearoff 0
	    set n 0 ; set m 0
	    foreach it $RECMODELS($tm) {
		if { $n > $MAXMENUITEMS } {
		    $mw add cascade -label "$TXT(more) ..." -menu $mw.m$m
		    set mw $mw.m$m ; menu $mw -tearoff 0
		    set n 0 ; incr m
		}
		$mw add command -label $it \
			-command "RecModelChange $tm ; \
			          [list $commd "$tm $it" $menu]"
		incr n
	    }
	} else {
	    $menu add command -label $tm \
		    -command "RecModelChange $tm ; $commd $tm $menu"
	}
    }
    return
}

proc RecModelChange {tm} {
    # change the description of the options related to the receiver
    #  configuration
    #  $tm is the receivers trade-mark in {Garmin, Lowrance, Magellan}
    global GPSREC OPTTYPE OPTGRPL OPTMENUPROC OPTMENUNAMES OPTMENUPREFIX \
	    OPTMENUCONTS DISPOPTS RECPROTOCOLS RECBAUDS

    set OPTTYPE(_GPSRecConf) group
    switch $tm {
	Garmin {
	    set OPTGRPL(_GPSRecConf) {DEFTRECPROTOCOL SERIALBAUD ACCEPTALLCHARS
		      NAMELENGTH COMMENTLENGTH
	              MAXWPOINTS MAXROUTES MAXWPINROUTE AutoNumRts MAXTPOINTS
		      CREATIONDATE NOLOWERCASE DEFAULTSYMBOL DEFAULTDISPOPT
	              SUPPORTLAPS}
            set OPTTYPE(DEFTRECPROTOCOL) fixedtextmenu
	    set OPTTYPE(SERIALBAUD) fixedmenu
	    set OPTMENUCONTS(SERIALBAUD) $RECBAUDS
            set OPTMENUCONTS(DEFTRECPROTOCOL) $RECPROTOCOLS(Garmin)
            set OPTMENUNAMES(DEFTRECPROTOCOL) TXT
            set OPTMENUPREFIX(DEFTRECPROTOCOL) {}
	    set OPTTYPE(ACCEPTALLCHARS) boolean
            set OPTTYPE(NAMELENGTH) nat
            set OPTTYPE(COMMENTLENGTH) nat
            set OPTTYPE(MAXWPOINTS) nat
            set OPTTYPE(MAXROUTES) nat
            set OPTTYPE(MAXWPINROUTE) nat
	    set OPTTYPE(AutoNumRts) boolean
            set OPTTYPE(MAXTPOINTS) nat
            set OPTTYPE(CREATIONDATE) boolean
            set OPTTYPE(NOLOWERCASE) boolean
            set OPTTYPE(DEFAULTSYMBOL) textmenu
            set OPTMENUPROC(DEFAULTSYMBOL) FillSymbolsMenu
            set OPTMENUNAMES(DEFAULTSYMBOL) TXT
            set OPTMENUPREFIX(DEFAULTSYMBOL) SY
            set OPTTYPE(DEFAULTDISPOPT) fixedtextmenu
            set OPTMENUCONTS(DEFAULTDISPOPT) $DISPOPTS
            set OPTMENUNAMES(DEFAULTDISPOPT) TXT
            set OPTMENUPREFIX(DEFAULTDISPOPT) DISP
	    set OPTTYPE(SUPPORTLAPS) boolean
        }
        Lowrance {
	    # BSB contribution
	    set OPTGRPL(_GPSRecConf) {NAMELENGTH INTERVAL COMMENTLENGTH
	              MAXWPOINTS MAXROUTES MAXWPINROUTE MAXTPOINTS CREATIONDATE
                      NOLOWERCASE DEFAULTSYMBOL DEFAULTDISPOPT}
            set OPTTYPE(NAMELENGTH) nat
            set OPTTYPE(INTERVAL) nat
            set OPTTYPE(COMMENTLENGTH) nat
            set OPTTYPE(MAXWPOINTS) nat
            set OPTTYPE(MAXROUTES) nat
            set OPTTYPE(MAXWPINROUTE) nat
            set OPTTYPE(MAXTPOINTS) nat
            set OPTTYPE(CREATIONDATE) boolean
            set OPTTYPE(NOLOWERCASE) boolean
            set OPTTYPE(DEFAULTSYMBOL) textmenu
            set OPTMENUPROC(DEFAULTSYMBOL) FillSymbolsMenu
            set OPTMENUNAMES(DEFAULTSYMBOL) TXT
            set OPTMENUPREFIX(DEFAULTSYMBOL) SY
            set OPTTYPE(DEFAULTDISPOPT) fixedtextmenu
            set OPTMENUCONTS(DEFAULTDISPOPT) $DISPOPTS
            set OPTMENUNAMES(DEFAULTDISPOPT) TXT
            set OPTMENUPREFIX(DEFAULTDISPOPT) DISP
        }
        Magellan {
	    # MGM contribution
	    set OPTGRPL(_GPSRecConf) {NAMELENGTH SERIALBAUD COMMENTLENGTH
	              MAXWPOINTS MAXROUTES MAXWPINROUTE MAXTPOINTS CREATIONDATE
                      NOLOWERCASE DEFAULTSYMBOL DEFAULTDISPOPT}
            set OPTTYPE(NAMELENGTH) nat
	    set OPTTYPE(SERIALBAUD) fixedmenu
	    # MF change: RECBAUDS is set in recdefs.tcl
	    set OPTMENUCONTS(SERIALBAUD) $RECBAUDS
            set OPTTYPE(INTERVAL) nat
            set OPTTYPE(COMMENTLENGTH) nat
            set OPTTYPE(MAXWPOINTS) nat
            set OPTTYPE(MAXROUTES) nat
            set OPTTYPE(MAXWPINROUTE) nat
            set OPTTYPE(MAXTPOINTS) nat
            set OPTTYPE(CREATIONDATE) boolean
            set OPTTYPE(NOLOWERCASE) boolean
            set OPTTYPE(DEFAULTSYMBOL) textmenu
            set OPTMENUPROC(DEFAULTSYMBOL) FillSymbolsMenu
            set OPTMENUNAMES(DEFAULTSYMBOL) TXT
            set OPTMENUPREFIX(DEFAULTSYMBOL) SY
            set OPTTYPE(DEFAULTDISPOPT) fixedtextmenu
            set OPTMENUCONTS(DEFAULTDISPOPT) $DISPOPTS
            set OPTMENUNAMES(DEFAULTDISPOPT) TXT
            set OPTMENUPREFIX(DEFAULTDISPOPT) DISP
        }
    }
    return
}

