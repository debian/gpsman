#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#  Copyright (c) 1999 Brian Baulch (baulchb@hotkey.net.au)
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: lowrance_symbols.tcl
#  Last change:  6 October 2013
#

proc LowranceStdSymbols {} {
    # set codes for set of standard Lowrance symbols
    global SYMBOLS SYMBOLCODE UNKNOWNSYMBOLS DEFAULTSYMBOL

    array set SYMBOLCODE {
	WP_dot        0
	mark_x        1
	1st_aid       2
	dot           3
	house         4
	car           5
	fuel          6
	phone         7
	knife_fork    8
	airport       9
	exit         10
	tree         11
	mountains    12
	camping      13
	picnic       14
	bridge       15
	ladder       16
	deer         17
	tracks       18
	many_tracks  19
	skull        20
	fish         21
	many_fish    22
	info         23
	wreck        24
	anchor       25
	boat_ramp    26
	flag         27
    }
    set UNKNOWNSYMBOLS $SYMBOLS
    foreach n [array names SYMBOLCODE] {
	set i [lsearch -exact $UNKNOWNSYMBOLS $n]
	if { $i != -1 } {
	    set UNKNOWNSYMBOLS [lreplace $UNKNOWNSYMBOLS $i $i]
	}
    }
    # codes of unknown symbols set to code of default symbol if possible,
    #  otherwise to code of WP_dot
    if { [lsearch -exact $DEFAULTSYMBOL $UNKNOWNSYMBOLS] == -1 } {
	set c $SYMBOLCODE($DEFAULTSYMBOL)
    } else { set c $SYMBOLCODE(WP_dot) }
    foreach n $UNKNOWNSYMBOLS {
	set SYMBOLCODE($n) $c
    }
    return
}



