#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: garmin_symbols.tcl
#  Last change:  6 October 2013
#

# symbols used by Magellan
#   from magellan.tcl Copyright (c) 2003 Matt Martin (matt.martin _AT_ ieee.org)
#   needed for importation/exportation of MapSend files (files_foreign.tcl)

set MAG_SYMTAB {WP_dot square_green house avn_vortac airport amusement_park
    casino car_repair boat camping exit 1st_aid avn_vordme buoy fuel deer
    golf lodging fish large_city light capitol_city boat_ramp medium_city
    museum danger park store knife_fork mountains diver_down_1 RV_park
    military scenic small_city oil_field stadium info truck_stop
    drinking_water wreck zoo}


# symbol and display option codes from Garmin GPS Interface Specification
#  Version 1 Rev. 3, Rev. A and Rev. B

proc GarminStdSymbols {} {
    # set codes for set of standard Garmin symbols
    global SYMBOLS SYMBOLCODE UNKNOWNSYMBOLS

    array set SYMBOLCODE {
	anchor             0
	bell               1
	diamond_green      2
	diamond_red        3
	diver_down_1       4
	diver_down_2       5
	dollar             6
	fish               7
	fuel               8
	horn               9
	house             10
	knife_fork        11
	light             12
	mug               13
	skull             14
	square_green      15
	square_red        16
	WP_buoy_white     17
	WP_dot            18
	wreck             19
	null              20
	MOB               21
	buoy_amber        22
	buoy_black        23
	buoy_blue         24
	buoy_green        25
	buoy_green_red    26
	buoy_green_white  27
	buoy_orange       28
	buoy_red          29
	buoy_red_green    30
	buoy_red_white    31
	buoy_violet       32
	buoy_white        33
	buoy_white_green  34
	buoy_white_red    35
	dot               36
	radio_beacon      37
	boat_ramp        150
	camping          151
	restrooms        152
	showers          153
	drinking_water   154
	phone            155
	1st_aid          156
	info             157
	parking          158
	park             159
	picnic           160
	scenic           161
	skiing           162
	swimming         163
	dam              164
	controlled       165
	danger           166
	restricted       167
	null_2           168
	ball             169
	car              170
	deer             171
	shopping_cart    172
	lodging          173
	mine             174
	trail_head       175
	truck_stop       176
	exit             177
	flag             178
	circle_x         179
	open_24hr        180
	fhs_facility     181
	bot_cond         182
	tide_pred_stn    183
        anchor_prohib    184
	beacon           185
	coast_guard      186
	reef             187
	weedbed          188
	dropoff          189
	dock             190
	marina           191
	bait_tackle      192
	stump            193
	is_highway      8192
	us_highway      8193
	st_highway      8194
	mile_marker     8195
	traceback       8196
	golf            8197
	small_city      8198
	medium_city     8199
	large_city      8200
	freeway         8201
	ntl_highway     8202
	capitol_city    8203
	amusement_park  8204
	bowling         8205
	car_rental      8206
	car_repair      8207
	fastfood        8208
	fitness         8209
	movie           8210
	museum          8211
	pharmacy        8212
	pizza           8213
	post_office     8214
	RV_park         8215
	school          8216
	stadium         8217
	store           8218
	zoo             8219
	fuel_store      8220
	theater         8221
	ramp_int        8222
	street_int      8223
	weight_station  8226
	toll            8227
	elevation       8228
	exit_no_serv    8229
	geo_name_man    8230
	geo_name_water  8231
	geo_name_land   8232
	bridge          8233
	building        8234
	cemetery        8235
	church          8236
	civil           8237
	crossing        8238
	monument        8239
	levee           8240
	military        8241
	oil_field       8242
	tunnel          8243
	beach           8244
	tree            8245
	summit          8246
	large_ramp_int  8247
	large_exit_ns   8248
	police          8249
	casino          8250
	snow_skiing     8251
	ice_skating     8252
	tow_truck       8253
	border          8254
	geocache        8255
	geocache_fnd    8256
	cntct_smiley    8257
	cntct_ball_cap  8258
	cntct_big_ears  8259
	cntct_spike     8260
	cntct_goatee    8261
	cntct_afro      8262
	cntct_dreads    8263
	cntct_female1   8264
	cntct_female2   8265
	cntct_female3   8266
	cntct_ranger    8267
	cntct_kung_fu   8268
	cntct_sumo      8269
	cntct_pirate    8270
	cntct_biker     8271
	cntct_alien     8272
	cntct_bug       8273
	cntct_cat       8274
	cntct_dog       8275
	cntct_pig       8276
	hydrant         8282
	flag_pin_blue   8284
	flag_pin_green  8285
	flag_pin_red    8286
	pin_blue        8287
	pin_green       8288
	pin_red         8289
	box_blue        8290
	box_green       8291
	box_red         8292
	biker           8293
	circle_red      8294
	circle_green    8295
	circle_blue     8296
	diamond_blue    8299
	oval_red        8300
	oval_green      8301
	oval_blue       8302
	rect_red        8303
	rect_green      8304
	rect_blue       8305
	square_blue     8308
	letter_a_red    8309
	letter_b_red    8310
	letter_c_red    8311
	letter_d_red    8312
	letter_a_green  8313
	letter_c_green  8314
	letter_b_green  8315
	letter_d_green  8316
	letter_a_blue   8317
	letter_b_blue   8318
	letter_c_blue   8319
	letter_d_blue   8320
	number_0_red    8321
	number_1_red    8322
	number_2_red    8323
	number_3_red    8324
	number_4_red    8325
	number_5_red    8326
	number_6_red    8327
	number_7_red    8328
	number_8_red    8329
	number_9_red    8330
	number_0_green  8331
	number_1_green  8332
	number_2_green  8333
	number_3_green  8334
	number_4_green  8335
	number_5_green  8336
	number_6_green  8337
	number_7_green  8338
	number_8_green  8339
	number_9_green  8340
	number_0_blue   8341
	number_1_blue   8342
	number_2_blue   8343
	number_3_blue   8344
	number_4_blue   8345
	number_5_blue   8346
	number_6_blue   8347
	number_7_blue   8348
	number_8_blue   8349
	number_9_blue   8350
	triangle_blue   8351
	triangle_green  8352
	triangle_red    8353
	airport         16384
	intersection    16385
	avn_ndb         16386
	avn_vor         16387
	heliport        16388
	private         16389
	soft_field      16390
	tall_tower      16391
	short_tower     16392
	glider          16393
	ultralight      16394
	parachute       16395
	avn_vortac      16396
	avn_vordme      16397
	avn_faf         16398
	avn_lom         16399
	avn_map         16400
	avn_tacan       16401
	seaplane        16402
    }
    set UNKNOWNSYMBOLS $SYMBOLS
    foreach n [array names SYMBOLCODE] {
	if { [set i [lsearch -exact $UNKNOWNSYMBOLS $n]] != -1 } {
	    set UNKNOWNSYMBOLS [lreplace $UNKNOWNSYMBOLS $i $i]
	}
    }
    return
}

proc SymbolsDOForProtocol {pid} {
    # change symbol and display option codes according to WPData protocol $pid
    #  (if needs be) 
    # change list of unknown symbols
    global SYMBOLS UNKNOWNSYMBOLS SYMBOLCODE DEFAULTSYMBOL \
	    DISPOPTS UNKNOWNDISPOPTS DISPOPTCODE DEFAULTDISPOPT \
	    GSym_Beg_Custom GSym_End_Custom

    # user customizable symbols; shown as $DEFAULTSYMBOL or WP_dot
    set GSym_Beg_Custom 7680
    set GSym_End_Custom 8191

    set UNKNOWNDISPOPTS ""
    # $garmin_dispopts sync-ed with $dispoptcodes; see also 2nd switch below
    set garmin_dispopts "s_name symbol s_comment"
    switch $pid {
	D103 -	D107 {
	    # non-standard symbols and display options
	    set dispoptcodes "0 1 2"
	    # these use the following new names: boat, exit, flag, duck
	    # index of name in $names is its code!
	    set names "WP_dot house fuel car fish boat anchor wreck exit \
		    skull flag camping circle_x deer WP_buoy_white traceback"
	    set UNKNOWNSYMBOLS $SYMBOLS
	    set c 0
	    foreach n $names {
		set SYMBOLCODE($n) $c ; incr c
		if { [set i [lsearch -exact $UNKNOWNSYMBOLS $n]] != -1 } {
		    set UNKNOWNSYMBOLS [lreplace $UNKNOWNSYMBOLS $i $i]
		}
	    }
	}
	D104 {
	    # standard symbols and non-standard display options
	    GarminStdSymbols
	    set dispoptcodes "3 1 5"
	    # 0 for "none" converted into "symbol"
	    # HOW???
	}
	D108 -  D109 -  D110 {
	    # standard symbols and non-standard display options (as D103)
	    GarminStdSymbols
	    set dispoptcodes "0 1 2"
	}
	D155 {
	    # standard symbols and display options
	    GarminStdSymbols
	    set dispoptcodes "3 1 5"
	}
	D100 -	D150 -	D151 -	D152 {
	    # with neither symbols nor display options
	    foreach n [set UNKNOWNSYMBOLS $SYMBOLS] {
		set SYMBOLCODE($n) 0
	    }
	    set dispoptcodes "0 0 0"
	    set UNKNOWNDISPOPTS $DISPOPTS
	}
	D101 -  D102 -	D105 -	D106 -	D154 {
	    # standard symbols but no display options
	    GarminStdSymbols
	    set dispoptcodes "0 0 0"
	    set UNKNOWNDISPOPTS $DISPOPTS
	}
    }
    # codes of unknown symbols set to code of default symbol if possible,
    #  otherwise to code of WP_dot
    if { [lsearch -exact $UNKNOWNSYMBOLS $DEFAULTSYMBOL] == -1 } {
	set ds $DEFAULTSYMBOL
    } else { set ds WP_dot }
    set c $SYMBOLCODE($ds)
    foreach n $UNKNOWNSYMBOLS {
	set SYMBOLCODE($n) $c
    }
    for { set u $GSym_Beg_Custom } { $u <= $GSym_End_Custom } { incr u } {
	set SYMBOLCODE(user:$u) $u
    }
    # display options
    set defcode -1
    foreach opt $garmin_dispopts c $dispoptcodes {
	if {! [string compare $opt $DEFAULTDISPOPT] } { set defcode $c }
	set DISPOPTCODE($opt) $c
    }
    if { $defcode == -1 } { set defcode $DISPOPTCODE(s_name) }
    foreach opt $DISPOPTS {
	switch $opt {
	    s_name -    symbol -    s_comment {
	    }
	    default {
		set DISPOPTCODE($opt) $defcode
		lappend UNKNOWNDISPOPTS $opt
	    }
	}
    }
    return
}


