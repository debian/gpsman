#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: geod.tcl
#  Last change:  6 October 2013
#

## Most of the geodetic information herein taken or adopted from
#  Kenneth Foote pages, Department of Geography, The University of
#    Colorado at Boulder
#    http://www.colorado.edu/geography/gcraft/notes/datum/edlist.html
#  geotrans - an open source coordinate transformation tool from the
#    USA National Imagery and Mapping Agency (NIMA)
#    http://www.remotesensing.org
#  gpstrans - a program to communicate with garmin gps
#      containing parts taken from John F. Waers (jfwaers _AT_ csn.net)
#      program MacGPS.
#      Copyright (c) 1995 by Carsten Tschach (tschach _AT_ zedat.fu-berlin.de)
#  International Institute for Aerospace Survey and Earth Sciences (ITC),
#      Enschede, http://kartoweb.itc.nl/geometrics
#  Land Information New Zealand (LINZ)
#      http://www.linz.govt.nz/rcs/linz/pub/web/root/core/SurveySystem/GeodeticInfo/geodeticdatums/index.jsp
##
#
## Some geodetic information kindly provided by
#      Luisa Bastos, Universidade do Porto
#      Attila Berenyi (berenyi.attila _AT_ gmail.com)
##
#
## Some more definitions taken from:
#      - the PROJ4.0 package by Gerald I. Evenden (gie _AT_ kai.er.usgs.gov)
#      - (Austrian Datum), http://homepage.ntlworld.com/anton.helm/bmn_mgi.html
#      - GPSTrackMaker by Odilon Ferreira (www.gpstm.com)
#      - Hungarian Datum 72: http://ftp/gps/honlap-jan2002.pdf, September 2008
##
#
## New way of representing datums with GDATUM and ELLPSDDEF arrays, and
#   proc EllipsdData based on contribution by Stefan Heinen
#   (stefan.heinen _AT_ djh-freeweb.de)
##

proc EllipsdData {datum} {
    # based on contribution by Stefan Heinen
    # yield the ellipsoid data for datum as a list with
    #    a - semi-major axis (as float)
    #    f - flattening (as float)
    #    dx, dy, dz - translation of center
    global GDATUM ELLPSDDEF

    set d [lrange $GDATUM($datum) 0 3]
    set def $ELLPSDDEF([lindex $d 0])
    return [lreplace $d 0 0 \
		[expr 1.0*[lindex $def 0]] [expr 1.0/[lindex $def 1]]]
}

proc EllipsdOf {datum} {
    # return ellipsoid name for $datum or "" on error
    global GDATUM

    if { [catch {set e [lindex $GDATUM($datum) 0]}] } { return "" }
    return $e
}

proc DatumRefId {datum} {
    # return reference id of $datum or -1 if unknown datum
    # if the reference id is a number it is the one used by GPStrans
    global GDATUM

    if { [lsearch -exact [array names GDATUM] $datum] == -1 } { return -1 }
    return [lindex $GDATUM($datum) 4]
}

proc DatumWithRefId {n} {
    # return name of datum whose reference id is $n or -1 if there is none
    # if the reference id is a number it is the one used by GPStrans
    global GDATUM

    foreach datum [array names GDATUM] {
	if { [lindex $GDATUM($datum) 4] == $n } { return $datum }
    }
    return -1
}

# definitions of known datums and ellipsoids

###### known datums
     # when changing this list make sure you update the definitions below

## description of predefined datum menu

set PREDEFDATUMDESCR \
    [list "WGS 84" "WGS 72" \
	 --- \
	 @ [list Europe \
                Austrian "Austrian (MGI)" "CH-1903" "Datum 73" \
	        "Deutsches Hauptdreiecksnetz" \
		"Estonia Coord System 1937" "ED73" \
		@ [list "European 1950" \
		   "European 1950" "European 1950; Belgium" \
		   "European 1950; Cyprus" \
		   "European 1950; Egypt" "European 1950; England Channel" \
		   "European 1950; England" "European 1950; Finland+Norway" \
		   "European 1950; Greece" "European 1950; Iran" \
		   "European 1950; Italy (Sardinia)" \
		   "European 1950; Italy (Sicily)" "European 1950; Malta" \
		   "European 1950; NW Europe" "European 1950; Middle East" \
		   "European 1950; Portugal+Spain" "European 1950; Tunisia"] \
		"European 1979" \
		"Finland Hayford" "Graciosa Base SW 1948" "Hermannskogel" \
		"Hermannskogel; old Yugoslavia" "Hjorsey 1955" \
	        "Hungarian Datum 1972" "Ireland 1965" "Lisboa" "NGO 1948" \
	        Norsk "NTF (Nouvelle Triangulation de France)" \
		"NTF (NTF ellipsoid)" \
		"Observatorio 1966" \
		@ [list "Ord Srvy Grt Britn" \
		   "Ord Srvy Grt Britn" "Ord Srvy Grt Britn; England" \
		   "Ord Srvy Grt Britn; England+Wales" \
		   "Ord Srvy Grt Britn; Scotland+Shetlands" \
		   "Ord Srvy Grt Britn; Wales"] \
		"Pico De Las Nieves" \
		"Porto Santo 1936" Potsdam "Pulkovo 1942" "Rome 1940" \
		"Rome 1940; Luxembourg" "Rijks Driehoeksmeting" "RT 90" \
		@ [list "S-42 (Pulkovo 1942)" \
		   "S-42 (Pulkovo 1942); Albania" \
		   "S-42 (Pulkovo 1942); Czechoslavakia" \
		   "S-42 (Pulkovo 1942); Hungary" \
		   "S-42 (Pulkovo 1942); Kazakhstan" \
		   "S-42 (Pulkovo 1942); Latvia" \
		   "S-42 (Pulkovo 1942); Romania" \
		   "S-42 (Pulkovo 1942); Poland"] \
		"S-JTSK" "Sao Braz" \
		"Southeast Base" "Southwest Base"] \
	@ [list America \
		"Bermuda 1957" "Bogota Observatory" "Campo Inchauspe" \
		"Cape Canaveral" "Chua Astro" "Corrego Alegre" \
		"Gan 1970" \
		@ [list NAD27 \
		   "NAD27 Alaska" "NAD27 Alaska; Aleutian East" \
		   "NAD27 Alaska; Aleutian West" "NAD27 Bahamas" \
		   "NAD27 Canada" "NAD27 Canada West" "NAD27 Canada Middle" \
		   "NAD27 Canada East" "NAD27 Canada North" \
		   "NAD27 Canada Yukon" "NAD27 Canal Zone" "NAD27 Caribbean" \
		   "NAD27 Central" "NAD27 CONUS" "NAD27 CONUS East" \
		   "NAD27 CONUS West" "NAD27 Cuba" \
		   "NAD27 Greenland" "NAD27 Mexico" "NAD27 San Salvador"] \
		@ [list NAD83 \
		   "NAD83" "NAD83; Aleutian Ids" "NAD83; Canada" \
		   "NAD83; CONUS" "NAD83; Hawaii" "NAD83; Mexico"] \
		"Naparima BWI" \
		@ [list "Old Hawaiian" \
		   "Old Hawaiian" "Old Hawaiian; Hawaii" \
		   "Old Hawaiian; Kauai" "Old Hawaiian; Maui" \
		   "Old Hawaiian; Oahu" "Old Hawaiian (Int)" \
		   "Old Hawaiian (Int); Hawaii" "Old Hawaiian (Int); Kauai" \
		   "Old Hawaiian (Int); Maui" "Old Hawaiian (Int); Oahu"] \
		@ [list "Prov So Amrican `56" \
		   "Prov So Amrican `56" "Prov So Amrican 56; Bolivia" \
		   "Prov So Amrican 56; Chile North" \
		   "Prov So Amrican 56; Chile South" \
		   "Prov So Amrican 56; Colombia" \
		   "Prov So Amrican 56; Ecuador" "Prov So Amrican 56; Guyana" \
		   "Prov So Amrican 56; Peru" \
		   "Prov So Amrican 56; Venezuela"] \
		"Prov So Chilean `63" "Puerto Rico" "Qornoq" \
		"Reunion" "Sapper Hill 1943" \
		@ [list "South American `69" \
		   "South American `69" "South American 69; Argentina" \
		   "South American 69; Baltra" \
		   "South American 69; Bolivia" "South American 69; Brazil" \
		   "South American 69; Brazil/IBGE" \
		   "South American 69; Chile" "South American 69; Colombia" \
		   "South American 69; Ecuador" \
		   "South American 69; Ecuador Galapagos" \
		   "South American 69; Guyana" "South American 69; Paraguay" \
		   "South American 69; Peru" \
		   "South American 69; Trinidad+Tobago" \
		   "South American 69; Venezuela"] \
		"Yacare"] \
	@ [list Africa \
	        @ [list Adindan \
		   "Adindan" "Adindan; B Faso" "Adindan; Cameroon" \
		   "Adindan; Ethiopia" "Adindan; Ethiopia+Sudan" \
		   "Adindan; Mali" "Adindan; Senegal" "Adindan; Sudan"] \
		"Afgooye" \
		@ [ list "Arc 1950" \
		   "Arc 1950" "Arc 1950; Botswana" "Arc 1950; Burundi" \
		   "Arc 1950; Lesotho" "Arc 1950; Malawi" \
		   "Arc 1950; Swaziland" "Arc 1950; Zaire" \
		   "Arc 1950; Zambia" "Arc 1950; Zimbabwe"] \
		@ [list "Arc 1960" \
		   "Arc 1960" "Arc 1960; Kenya" "Arc 1960; Kenya+Tanzania" \
		   "Arc 1960; Tanzania"] \
		"Astro DOS 71/4" "Ayabelle Lighthouse" \
		"Bissau" "Cape" "Carthage" "Dabola" "Egypt" \
		"European 1950; Tunisia" "Leigon" \
		"Liberia 1964" "M'Poraloko" "Massawa" \
		"Merchich" "Minna" "Minna; Cameroon" "North Sahara 1959" \
		"Old Egyptian" "Point 58" "Pointe Noire 1948" "Schwarzeck" \
		"Sierra Leone" "Tananarive Observatory 1925" \
		"Tristan Astro 1968" "Voirol 1874" "Voirol 1960" "Zanderij"] \
	@ [list "Middle East" \
		@ [list "Ain el Abd 1970" \
		   "Ain el Abd 1970" "Ain el Abd 1970; Bahrain" \
		   "Ain el Abd 1970; Saudi Arabia"] \
		@ [list "European 1950" \
		   "European 1950; Iran" "European 1950; Middle East"] \
		"Herat North" "Nahrwn Masirah Ilnd" \
		"Nahrwn Saudi Arbia" "Nahrwn United Arab" "Oman"] \
	@ [list Asia \
	        "Bukit Rimpah" "Gunung Segara" "Hong Kong 1963" \
		"Hu-Tzu-Shan" "Indian (Bangladesh)" "Indian (India, Nepal)" \
		"Indian (Pakistan)" "Indian Thailand" "Indian 1954" \
		"Indian 1960; Vietnam (Con Son)" "Indian 1960; Vietnam (N16)" \
		"Indian 1975" "Indonesian 1974" "Kandawala" \
		"Korean Geodetic System" "South Asia" \
		@ [list "Tokyo" \
		   "Tokyo" "Tokyo; Japan" "Tokyo; Okinawa" \
		   "Tokyo; South Korea"] \
		] \
	@ [list "Australia, NZ" \
		"Australian Geod `66" "Australian Geod `84" \
		"Geodetic Datum `49"] \
	@ [list "$TXT(others) \[A-I\]" \
	        "American Samoa 1962" "Anna 1 Astro 1965" \
		"Antigua Island Astro 1943" "Ascension Island `58" \
		"Astro B4 Sorol Atoll" "Astro Beacon \"E\"" \
		"Astronomic Stn `52" "Astro Tern Island (FRIG)" \
		"Bellevue (IGN)" "Camp Area Astro" "Canton Astro 1966" \
		"Chatham 1971" "Deception Island" "Dionisos" \
		"Djakarta (Batavia)" \
		"DOS 1968" "Easter Island 1967" "Fort Thomas 1955" \
		"Gandajika Base" "Guam 1963" \
		"GUX 1 Astro" Israeli "ISTS 061 Astro 1968" \
		"ISTS 073 Astro `69"] \
	@ [list "$TXT(others) \[J-Z\]" \
		"Johnston Island" "Johnston Island 1961" "K12 Astro 1955" \
		"Kerguelen Island" "Kertau 1948" "Kusaie Astro 1951" \
		"L.C. 5 Astro" "Luzon Mindanao" "Luzon Philippines" \
		"Mahe 1971" "Marco Astro" "Midway Astro 1961" \
		"Montserrat Island Astro 1958" \
		"Pitcairn Astro 1967" "Qatar National" "Santo (DOS)" \
		"Selvagem Grande 1938" "Timbalai 1948" "Viti Levu 1916" \
		"Wake Island Astro 1952" "Wake-Eniwetok `60"]
     ]

proc FillDatumMenu {menu commdargs args} {
    # fill menu with known datum names
    # see proc FillMenu (utils.tcl) for the details on the arguments
    #  $args not used
    global TXT DefSpecs PREDEFDATUMDESCR
    global $DefSpecs(datum,ulist)

    set descr $PREDEFDATUMDESCR
    set ulist [set $DefSpecs(datum,ulist)]
    if { $ulist != "" } {
	set ud [linsert $ulist 0 "$TXT(userdefs)..."]
	set descr [linsert $descr 0 @ $ud ---]
    }
    FillMenu $menu $commdargs $descr
    return
}

## datum definitions
# index of GDATUM is datum name
# each list contains:
#  - ellipsoid name
#  - dx, dy, dz
#  - reference id; if a number, it is the number used by GPStrans
#  - comment
#    (the following elements are not being used and may be absent)
#  - expected error estimate in metres: ex, ey, ez (-1 for unknown)
#  - number of satellite measurement stations (0 for unknown)
#  - min, max lat and min, max long in signed degrees (empty or "_" if unknown)

array set GDATUM {
"Adindan" { "Clarke 1880"
    -162 -12 206
    0 "Mean for Ethiopia, Mali, Senegal, Sudan" -1 -1 -1 0}

"Adindan; B Faso" { "Clarke 1880"
    -118 -14 218
    0a "Burkina Faso" 25 25 25 1 4 22 -5 8}

"Adindan; Cameroon" { "Clarke 1880"
    -134 -2 210
    0b "" 25 25 25 1 -4 19 3 23}

"Adindan; Ethiopia" { "Clarke 1880"
    -165 -11 206
    0c "" 3 3 3 8 -3 25 26 50}

"Adindan; Ethiopia+Sudan" { "Clarke 1880"
    -166 -15 204
    0c1 ""  5 5 3 22 -5 31 15 55}

"Adindan; Mali" { "Clarke 1880"
    -123 -20 220
    0d "" 25 25 25 1 3 31 -20 11}

"Adindan; Senegal" { "Clarke 1880"
    -128 -18 224
    0e "" 25 25 25 2 5 23 -24 -5}

"Adindan; Sudan" { "Clarke 1880"
    -161 -14 205
    0f "" 3 5 3 14 -3 31 15 45}

"Afgooye" { "Krassovsky 1940"
    -43 -163 45
    1 "Somalia" 25 25 25 1 -8 19 35 60}

"Ain el Abd 1970" { "International 1924"
    -150 -251 -2
    2 "Mean for Bahrain Island, Saudi Arabia" -1 -1 -1 0}

"Ain el Abd 1970; Bahrain" { "International 1924"
    -150 -250 -1
    2a "" 25 25 25 2 24 28 49 53}

"Ain el Abd 1970; Saudi Arabia" { "International 1924"
    -143 -236 7
    2b "" 10 10 10 9 8 38 28 62}

"Anna 1 Astro 1965" { "Australian National"
    -491 -22 435
    3 "Cocos Islands" 25 25 25 1 -14 -10 94 99}

"Arc 1950" { "Clarke 1880"
    -143 -90 -294
    4 "Mean for Botswana, Lesotho, Malawi, Swaziland, Zaire, Zambia, Zimbabwe"
    20 33 20 41 -36 10 4 42}

"Arc 1950; Botswana" { "Clarke 1880"
    -138 -105 -289
    4a "" 3 5 3 9 -33 -13 13 36}

"Arc 1950; Burundi" { "Clarke 1880"
    -153 -5 -292
    4b "" 20 20 20 3 -11 4 21 37}

"Arc 1950; Lesotho" { "Clarke 1880"
    -125 -108 -295
    4c "" 3 3 8 5 -36 -23 21 35}

"Arc 1950; Malawi" { "Clarke 1880"
    -161 -73 -317
    4d "" 9 24 8 6 -21 -3 26 42}

"Arc 1950; Swaziland" { "Clarke 1880"
    -134 -105 -295
    4e "" 15 15 15 4 -33 -20 25 40}

"Arc 1950; Zaire" { "Clarke 1880"
    -169 -19 -278
    4f "" 25 25 25 2 -21 10 4 38}

"Arc 1950; Zambia" { "Clarke 1880"
    -147 -74 -283
    4g "" 21 21 27 5 -24 -1 15 40}

"Arc 1950; Zimbabwe" { "Clarke 1880"
    -142 -96 -293
    4h "" 5 8 11 10 -29 -9 19 39}

"Arc 1960" { "Clarke 1880"
    -160 -8 -300
    5 "Kenya, Tanzania" -1 -1 -1 0}

"Arc 1960; Kenya+Tanzania" { "Clarke 1880"
    -160 -6 -302
    5a "" 20 20 20 25 -18 8 23 47}

"Arc 1960; Kenya" { "Clarke 1880"
    -157 -2 -299
    5b "" 4 3 3 24 -11 8 28 47}

"Arc 1960; Tanzania" { "Clarke 1880"
    -175 -23 -303
    5c "" 6 9 10 12 -18 5 23 47}

"Ascension Island `58" { "International 1924"
    -205 107 53
    6 "GPStrans gives: -207 107 52" 25 25 25 2 -9 -6 -16 -13}

"Astro B4 Sorol Atoll" { "International 1924"
    114 -116 -333
    7 "" -1 -1 -1 0}

"Astro Beacon \"E\"" { "International 1924"
    145 75 -272
    8 "Iwo Jima" 25 25 25 1 22 26 140 144}

"Astro DOS 71/4" { "International 1924"
    -320 550 -494
    9 "St Helena Island" 25 25 25 1 -18 -14 -7 -4}

"Astronomic Stn `52" { "International 1924"
    124 -234 -25
    10 "Marcus Island" 25 25 25 1 22 26 152 156}

"Australian Geod `66" { "Australian National"
    -133 -48 148
    11 "Australia; Tasmania" 3 3 3 105 -46 -4 109 161}

"Australian Geod `84" { "Australian National"
    -134 -48 149
    12 "Australia; Tasmania" 2 2 2 90 -46 -4 109 161}

"Bellevue (IGN)" { "International 1924"
    -127 -769 472
    13 "Efate and Erromango Islands" 20 20 20 3 -20 -16 167 171}

"Bermuda 1957" { "Clarke 1866"
    -73 213 296
    14 "" 20 20 20 3 31 34 -66 -63}

"Bogota Observatory" { "International 1924"
    307 304 -318
    15 "Colombia" 6 5 6 7 -10 16 -85 -61}

"Campo Inchauspe" { "International 1924"
    -148 136 90
    16 "Argentina" 5 5 5 20 -58 -27 -72 -51}

"Canton Astro 1966" { "International 1924"
    298 -304 -375
    17 "Phoenix Islands" 15 15 15 4 -13 3 -180 -165}

"Cape" { "Clarke 1880"
    -136 -108 -292
    18 "South Africa" 3 6 6 5 -43 -15 10 40}

"Cape Canaveral" { "Clarke 1866"
    -2 151 181
    19 "Bahamas, Florida" 3 3 3 19 15 38 -94 -12}

"Carthage" { "Clarke 1880"
    -263 6 431
    20 "Tunisia" 6 9 8 5 24 43 2 18}

"CH-1903" { "Bessel 1841"
    674 15 405
    21 "Switzerland"}

"Chatham 1971" { "International 1924"
    175 -38 113
    22 "New Zealand (Chatham Island)" 15 15 15 4 -46 -42 -180 -174}

"Chua Astro" { "International 1924"
    -134 229 -29
    23 "Paraguay" 6 9 5 6 -33 -14 -69 -49}

"Corrego Alegre" { "International 1924"
    -206 172 -6
    24 "Brazil" 5 3 5 17 -39 -2 -80 -29}

"Djakarta (Batavia)" { "Bessel 1841"
    -377 681 -50
    25 "Indonesia (Sumatra)" 3 3 3 5 -16 11 89 146}

"DOS 1968" { "International 1924"
    230 -199 -752
    26 "New Georgia Islands (Gizo Island)" 25 25 25 1 -10 -7 155 158}

"Easter Island 1967" { "International 1924"
    211 147 111
    27 "" 25 25 25 1 -29 -26 -111 -108}

"European 1950" { "International 1924"
    -87 -98 -121
    28 "Mean for Austria, Belgium, Denmark, Finland, France, W Germany, Gibraltar, Greece, Italy, Luxembourg, Netherlands, Norway, Portugal, Spain, Sweden, Switzerland"
    3 8 5 85 30 80 5 33}

"European 1950; Cyprus" { "International 1924"
    -104 -101 -140
    28a "" 15 15 15 4 33 37 31 36}

"European 1950; Egypt" { "International 1924"
    -130 -117 -151
    28b "" 6 8 8 14 16 38 19 42}

"European 1950; England Channel" { "International 1924"
    -86 -96 -120
    28c "England, Channel Islands, Scotland, Shetland Islands"
    3 3 3 40 48 62 -10 3}

"European 1950; England" { "International 1924"
    -86 -96 -120
    28d "England, Ireland, Scotland, Shetland Islands" 3 3 3 47 48 62 -12 3}

"European 1950; Finland+Norway" { "International 1924"
    -87 -95 -120
    28e "" 3 5 3 20 52 80 -2 38}

"European 1950; Greece" { "International 1924"
    -84 -95 -130
    28f "" 25 25 25 2 30 48 14 34}

"European 1950; Iran" { "International 1924"
    -117 -132 -164
    28g "" 9 12 11 27 19 47 37 69}

"European 1950; Italy (Sardinia)" { "International 1924"
    -97 -103 -120
    28h "" 25 25 25 2 37 43 6 12}

"European 1950; Italy (Sicily)" { "International 1924"
    -97 -88 -135
    28i "" 20 20 20 3 35 40 10 17}

"European 1950; Malta" { "International 1924"
    -107 -88 -149
    28j "" 25 25 25 1 34 38 12 16}

"European 1950; NW Europe" { "International 1924"
    -87 -96 -120
    28k "Mean for Austria, Denmark, France, W Germany, Netherlands, Switzerland"
    3 3 3 52 30 78 -15 25}

"European 1950; Middle East" { "International 1924"
    -103 -106 -141
    28l "Mean for Iraq, Israel, Jordan, Lebanon, Kuwait, Saudi Arabia, Syria"
    -1 -1 -1 0 4 38 36 57}

"European 1950; Portugal+Spain" { "International 1924"
    -84 -107 -120
    28m "" 5 6 3 18 30 49 -15 10}

"European 1950; Tunisia" { "International 1924"
    -112 -77 -145
    28n "" 25 25 25 4 24 43 2 18}

"European 1979" { "International 1924"
    -86 -98 -119
    29 "Mean for Austria, Finland, Netherlands, Norway, Spain, Sweden, Switzerland"
    3 3 3 22 30 80 -15 24}

"Finland Hayford" { "International 1924"
    -78 -231 -97
    30 ""}

"Gandajika Base" { "International 1924"
    -133 -321 50
    31 "Republic of Maldives"}

"Geodetic Datum `49" { "International 1924"
    84 -22 209
    32 "NZGD49, New Zealand" 5 3 5 14 -48 -33 165 180}

"Guam 1963" { "Clarke 1866"
    -100 -248 259
    33 "" 3 3 3 5 12 15 143 146}

"GUX 1 Astro" { "International 1924"
    252 -209 -751
    34 "Guadalcanal Island" 25 25 25 1 -12 -8 158 163}

"Hjorsey 1955" { "International 1924"
    -73 46 -86
    35 "Iceland" 3 3 6 6 61 69 -24 -11}

"Hong Kong 1963" { "International 1924"
    -156 -271 -189
    36 "" 25 25 25 2 21 24 112 116}

"Indian (Bangladesh)" {"Everest (India 1830)"
    282 726 254
    37 "GPStrans gives: 289 734 257" 10 8 12 6 15 33 80 100}

"Indian Thailand" { "Everest (India 1830)"
    214 836 303
    38 "Thailand, Vietnam"}

"Ireland 1965" { "Modified Airy"
    506 -122 611
    39 "" 3 3 3 7 50 57 -12 -4}

"ISTS 073 Astro `69" { "International 1924"
    208 -435 -229
    40 "Diego Garcia, South Georgia Islands" 25 25 25 2 -10 -4 69 75}

"Johnston Island" { "International 1924"
    189 -79 -202
    41 "GPStrans gives: 191 -77 -204" 25 25 25 1 -46 -43 -76 -73}

"Kandawala" { "Everest (India 1830)"
    -97 787 86
    42 "Sri Lanka" 20 20 20 3 4 12 77 85}

"Kerguelen Island" { "International 1924"
    145 -187 103
    43 "" 25 25 25 1 -81 -74 139 180}

"Kertau 1948" { "Everest (1948)"
    -11 851 5
    44 "West Malaysia and Singapore" 10 8 6 6 -5 12 94 112}

"L.C. 5 Astro" { "Clarke 1866"
    42 124 147
    45 "Cayman Brac Island" 25 25 25 1 18 21 -81 -78}

"Liberia 1964" { "Clarke 1880"
    -90 40 88
    46 "" 15 15 15 4 -1 14 -17 -1}

"Luzon Mindanao" { "Clarke 1866"
    -133 -79 -72
    47 "Also known as: Luzon" 25 25 25 1 4 12 120 128}

"Luzon Philippines" { "Clarke 1866"
    -133 -77 -51
    48 "Excluding Mindanao" 8 11 9 6 3 23 115 128}

"Mahe 1971" { "Clarke 1880"
    41 -220 -134
    49 "Mahe Island" 25 25 25 1 -6 -3 54 57}

"Marco Astro" { "International 1924"
    -289 -124 60
    50 "" -1 -1 -1 0}

"Massawa" { "Bessel 1841"
    639 405 60
    51 "Ethiopia (Eritrea)" 25 25 25 1 7 25 37 53}

"Merchich" { "Clarke 1880"
    31 146 47
    52 "Morocco" 5 3 3 9 22 42 -19 5}

"Midway Astro 1961" { "International 1924"
    912 -58 1227
    53 "Midway Islands" 25 25 25 1 25 30 -180 -169}

"Minna" { "Clarke 1880"
    -92 -93 122
    54 "Nigeria" 3 6 5 6 -1 21 -4 20}

"Minna; Cameroon" { "Clarke 1880"
    -81 -84 115
    54a "" 25 25 25 2 -4 19 3 23}

"NAD27 Alaska" { "Clarke 1866"
    -5 135 172
    55 "Excluding Aleutian Ids" 5 9 5 47 47 78 -175 -157}

"NAD27 Alaska; Aleutian East" { "Clarke 1866"
    -2 152 149
    55a "Alaska (Aleutian Islands East of 180W)" 6 8 10 6 50 58 -180 -161}

"NAD27 Alaska; Aleutian West" { "Clarke 1866"
    2 204 105
    55b "Alaska (Aleutian Islands West of 180W)" 10 10 10 5 50 58 169 180}

"NAD27 Bahamas" { "Clarke 1866"
    -4 154 178
    56 "Except San Salvador Id)" 5 3 5 11 19 29 -83 -71}

"NAD27 Canada" { "Clarke 1866"
    -10 158 187
    57 "Mean for Canada" 15 11 6 112 36 90 -150 -50}

"NAD27 Canada West" { "Clarke 1866"
    -7 162 188
    57a "Canada (Alberta, British Columbia)" 8 8 6 25 43 65 -145 -105}

"NAD27 Canada Middle" { "Clarke 1866"
    -9 157 184
    57b "Canada (Manitoba, Ontario)" 9 5 5 25 36 63 -108 -69}

"NAD27 Canada East" { "Clarke 1866"
    -22 160 190
    57c "Canada (New Brunswick, Newfoundland, Nova Scotia, Quebec)"
    6 6 3 37 38 68 -85 -45}

"NAD27 Canada North" { "Clarke 1866"
    4 159 188
    57d "Canada (Northwest Territories, Saskatchewan)" 5 5 3 17 43 90 -144 -55}

"NAD27 Canada Yukon" { "Clarke 1866"
    -7 139 181
    57e "" 5 8 3 8 53 75 -147 -117}

"NAD27 Canal Zone" { "Clarke 1866"
    0 125 201
    58 "" 20 20 20 3 3 15 -86 -74}

"NAD27 Caribbean" { "Clarke 1866"
    -3 142 183
    59 "GPStrans gives -7 152 178\nMean for Antigua, Barbados, Barbuda, Caicos Islands, Cuba, Dominican Republic, Grand Cayman, Jamaica, Turks Islands"
    3 9 12 15 8 29 -87 -58}

"NAD27 Central" { "Clarke 1866"
    0 125 194
    60 "Mean for Belize, Costa Rica, El Salvador, Guatemala, Honduras, Nicaragua"
    8 3 5 19 3 25 -98 -77}

"NAD27 CONUS" { "Clarke 1866"
    -8 160 176
    61 "" 5 5 6 405 15 60 -135 -60}

"NAD27 CONUS East" { "Clarke 1866"
    -9 161 179
    61a "Mean for CONUS (West of Mississippi River Excluding Louisiana, Missouri, Minnesota)"
    5 5 8 129 18 55 -102 -60}

"NAD27 CONUS West" { "Clarke 1866"
    -8 159 175
    61b "" 5 3 3 276 19 55 -132 -87}

"NAD27 Cuba" { "Clarke 1866"
    -9 152 178
    62 "" 25 25 25 1 18 25 -87 -78}

"NAD27 Greenland" { "Clarke 1866"
    11 114 195
    63 "Greenland (Hayes Peninsula)" 25 25 25 1 74 81 74 56}

"NAD27 Mexico" { "Clarke 1866"
    -12 130 190
    64 "" 8 6 6 22 10 38 -122 -80}

"NAD27 San Salvador" { "Clarke 1866"
    1 140 165
    65 "" 25 25 25 1 23 26 -75 -74}

"NAD83" { "GRS 80"
    0 0 0
    66 "Alaska (Excluding Aleutian Islands), Canada, CONUS, Central America, Mexico"
    2 2 2 42 48 78 -175 -135}

"NAD83; Aleutian Ids" { "GRS 80"
    -2 0 4
    66a "" 5 2 5 4 51 74 -180 180}

"NAD83; Canada" { "GRS 80"
    0 0 0
    66b "Canada" 2 2 2 96 36 90 -150 -50}

"NAD83; CONUS" { "GRS 80"
    0 0 0
    66c "CONUS" 2 2 2 216 15 60 -135 -60}

"NAD83; Hawaii" { "GRS 80"
    1 1 -1
    66d "" 2 2 2 0 17 24 -164 -153}

"NAD83; Mexico" {"GRS 80"
    0 0 0
   66e "Mexico; Central America" 2 2 2 25 28 11 -122 -72}

"Nahrwn Masirah Ilnd" { "Clarke 1880"
    -247 -148 369
    67 "" 25 25 25 2 19 22 57 60}

"Nahrwn Saudi Arbia" { "Clarke 1880"
    -243 -192 477
    68 "GPStrans gives: -231 -196 482" 20 20 20 3 8 38 28 62}

"Nahrwn United Arab" { "Clarke 1880"
    -249 -156 381
    69 "United Arab Emirates" 25 25 25 2 17 32 45 62}

"Naparima BWI" { "International 1924"
    -10 375 165
    70 "Trinidad and Tobago; GPStrans gives: -2 374 172"
    15 15 15 4 8 13 -64 -59}

"Observatorio 1966" { "International 1924"
    -425 -169 81
    71 "Azores (Corvo and Flores Islands); also known as: Observatorio Meteorologico 1939"
    20 20 20 3 38 41 -33 -30}

"Old Egyptian" { "Helmert 1906"
    -130 110 -13
    72 "" 3 6 8 14 16 38 19 42}

"Old Hawaiian" { "Clarke 1866"
    61 -285 -181
    73 "Mean for Hawaii, Kauai, Maui, Oahu" 25 20 20 15 17 24 -164 -153}

"Old Hawaiian; Hawaii" { "Clarke 1866"
    89 -279 -183
    73a "" 25 25 25 2 17 22 -158 -153}

"Old Hawaiian; Kauai" { "Clarke 1866"
    45 -290 -172
    73b "" 20 20 20 3 20 24 -161 -158}

"Old Hawaiian; Maui" { "Clarke 1866"
    65 -290 -190
    73c "" 25 25 25 2 19 23 -158 -154}

"Old Hawaiian; Oahu" { "Clarke 1866"
    58 -283 -182
    73d "" 10 6 6 8 20 23 -160 -156}

"Old Hawaiian (Int)" { "International 1924"
    201 -228 -346
    73e "Mean for Hawaii, Kauai, Maui, Oahu" 25 20 20 0 17 24 -164 -153}

"Old Hawaiian (Int); Hawaii" { "International 1924"
    229 -222 -348
    73f "" 25 25 25 0 17 22 -158 -153}

"Old Hawaiian (Int); Kauai" { "International 1924"
    185 -233 -337
    73g "" 20 20 20 0 20 24 -161 -158}

"Old Hawaiian (Int); Maui" { "International 1924"
    205 -233 -355
    73h "" 25 25 25 0 19 23 -158 -154}

"Old Hawaiian (Int); Oahu" { "International 1924"
    198 -226 -347
    73i "" 10 6 6 0 20 23 -160 -156}

"Oman" { "Clarke 1880"
    -346 -1 224
    74 "" 3 3 9 7 10 32 46 65}

"Ord Srvy Grt Britn" { "Airy 1830"
    375 -111 431
    75 "Ordnance Survey Great Britain 1936\nMean for England, Isle of Man, Scotland, Shetland Islands, Wales"
    10 10 15 38 44 66 -14 7}

"Ord Srvy Grt Britn; England" { "Airy 1830"
    371 -112 434
    75a "" 5 5 6 21 44 61 -12 7}

"Ord Srvy Grt Britn; England+Wales" { "Airy 1830"
    371 -111 434
    75b "England, Isle of Man, Wales" 10 10 15 25 44 61 -12 7}

"Ord Srvy Grt Britn; Scotland+Shetlands" { "Airy 1830"
    384 -111 425
    75c "Scotland, Shetland Islands" 10 10 10 13 49 66 -14 4}

"Ord Srvy Grt Britn; Wales" { "Airy 1830"
    370 -108 434
    75d "" 20 20 20 3 46 59 -11 3}

"Pico De Las Nieves" { "International 1924"
    -307 -92 127
    76 "Canary Islands" 25 25 25 1 26 31 -20 -12}

"Pitcairn Astro 1967" { "International 1924"
    185 165 42
    77 "Pitcairn Island" 25 25 25 1 -27 -21 -134 -119}

"Prov So Amrican `56" { "International 1924"
    -288 175 -376
    78 "Provisional South American 1956\nMean for Bolivia, Chile, Colombia, Ecuador, Guyana, Peru, Venezuela"
    17 27 27 63 -64 18 -87 -51}

"Prov So Amrican 56; Bolivia" { "International 1924"
    -270 188 -388
    78a "" 5 11 14 5 -28 -4 -75 -51}

"Prov So Amrican 56; Chile North" { "International 1924"
    -270 183 -390
    78b "Chile (Northern, Near S19)" 25 25 25 1 -45 -12 -83 -60}

"Prov So Amrican 56; Chile South" { "International 1924"
    -305 243 -442
    78c "Chile (Southern, Near S43)" 20 20 20 3 -64 -20 -83 -60}

"Prov So Amrican 56; Colombia" { "International 1924"
    -282 169 -371
    78d "" 15 15 15 4 -10 16 -85 -61}

"Prov So Amrican 56; Ecuador" { "International 1924"
    -278 171 -367
    78e "" 3 5 3 11 -11 7 -85 -70}

"Prov So Amrican 56; Guyana" { "International 1924"
    -298 159 -369
    78f "" 6 14 5 9 -4 14 -67 -51}

"Prov So Amrican 56; Peru" { "International 1924"
    -279 175 -379
    78g "" 6 8 12 6 -24 5 -87 -63}

"Prov So Amrican 56; Venezuela" { "International 1924"
    -295 173 -371
    78h "" 9 14 15 24 -5 18 -79 -54}

"Prov So Chilean `63" { "International 1924"
    16 196 93
    79 "Chile (Near S53); (Hito XVIII)" 25 25 25 2 -64 -25 -83 -60}

"Puerto Rico" { "Clarke 1866"
    11 72 -101
    80 "Puerto Rico, Virgin Islands" 3 3 3 11 16 20 -69 -63}

"Qatar National" { "International 1924"
    -128 -283 22
    81 "" 20 20 20 3 19 32 45 57}

"Qornoq" { "International 1924"
    164 138 -189
    82 "Greenland (South)" 25 25 32 2 57 85 -77 -7}

"Reunion" { "International 1924"
    94 -948 -1262
    83 "Mascarenhas Islands" 25 25 25 1 -27 -12 47 65}

"Rome 1940" { "International 1924"
    -225 -65 9
    84 "Italy (Sardinia)" 25 25 25 1 37 43 6 12}

"RT 90" { "Bessel 1841"
    498 -36 568
    85 "Sweden"}

"Santo (DOS)" { "International 1924"
    170 42 84
    86 "Espirito Santo Island" 25 25 25 1 -17 -13 160 169}

"Sao Braz" { "International 1924"
    -203 141 53
    87 "Azores (Sao Miguel, Santa Maria Ids)" 25 25 25 2 35 39 -27 -23}

"Sapper Hill 1943" { "International 1924"
    -355 21 72
    88 "East Falkland Island; GPStrans gives: -355 16 74"
    1 1 1 5 -54 -50 -61 -56}

"Schwarzeck" { "Bessel 1841 (Namibia)"
    616 97 -251
    89 "Namibia" 20 20 20 3 -35 -11 5 31}

"South American `69" { "South American 1969"
    -57 1 -41
    90 "Mean for Argentina, Bolivia, Brazil, Chile, Colombia, Ecuador, Guyana, Paraguay, Peru, Trinidad and Tobago, Venezuela"
    15 6 9 84 -65 -50 -90 -25}

"South American 69; Argentina" { "South American 1969"
    -62 -1 -37
    90a "" 5 5 5 10 -62 -23 -76 -47}

"South American 69; Baltra" { "South American 1969"
    -47 26 -42
    90b "" 25 25 25 0 -2 1 -92 -89}

"South American 69; Bolivia" { "South American 1969"
    -61 2 -48
    90c "" 15 15 15 4 -28 -4 -75 -51}

"South American 69; Brazil" { "South American 1969"
    -60 -2 -41
    90d "" 3 5 5 22 -39 -2 -80 -29}

"South American 69; Chile" { "South American 1969"
    -75 -1 -44
    90e "" 15 8 11 9 -64 -12 -83 -60}

"South American 69; Colombia" { "South American 1969"
    -44 6 -36
    90f "" 6 6 5 7 -10 16 -85 -61}

"South American 69; Ecuador" { "South American 1969"
    -48 3 -44
    90g "" 3 3 3 11 -11 7 -85 -70}

"South American 69; Ecuador Galapagos" { "South American 1969"
    -47 26 -42
    90h "Ecuador (Baltra, Galapagos)" 25 25 25 1}

"South American 69; Guyana" { "South American 1969"
    -53 3 -47
    90i "" 9 5 5 5 -4 14 -67 -51}

"South American 69; Paraguay" { "South American 1969"
    -61 2 -33
    90j "" 15 15 15 4 -33 -14 -69 -49}

"South American 69; Peru" { "South American 1969"
    -58 0 -44
    90k "" 5 5 5 6 -24 5 -87 -63}

"South American 69; Trinidad+Tobago" { "South American 1969"
    -45 12 -33
    90l "" 25 25 25 1 4 17 -68 -55}

"South American 69; Venezuela" { "South American 1969"
    -45 8 -33
    90m "" 3 6 3 5 -5 18 -79 -54}

"South Asia" { "Modified Fischer 1960"
    7 -10 -26
    91 "Singapore" 25 25 25 1 0 3 102 106}

"Southeast Base" { "International 1924"
    -499 -249 314
    92 "= Porto Santo 1936"}

"Southwest Base" { "International 1924"
    -104 167 -38
    93 "= Graciosa Base SW 1948"}

"Timbalai 1948" { "Everest (Sabah Sarawak)"
    -679 669 -48
    94 "Brunei, E. Malaysia (Sabah Sarawak)\nGPStrans gives: datum=Everest (India 1830), -689 691 -46"
    10 10 12 8 -5 15 101 125}

"Tokyo" { "Bessel 1841"
    -148 507 685
    95 "Mean for Japan, South Korea, Okinawa; GPStrans gives: -128 481 664"
    20 5 20 31 23 53 120 155}

"Tokyo; Japan" { "Bessel 1841"
    -148 507 685
    95a "" 8 5 8 16 19 51 119 156}

"Tokyo; Okinawa" { "Bessel 1841"
    -158 507 676
    95b "" 20 5 20 3 19 31 119 134}

"Tokyo; South Korea" { "Bessel 1841"
    -147 506 687
    95c "also as: -146 507 687, 8 5 8" 2 2 2 29 27 45 120 139}

"Tristan Astro 1968" { "International 1924"
    -632 438 -609
    96 "Tristao da Cunha" 25 25 25 1 -39 -36 -14 -11}

"Viti Levu 1916" { "Clarke 1880"
    51 391 -36
    97 "Fiji (Viti Levu Island)" 25 25 25 1 -20 -16 176 180}

"Wake-Eniwetok `60" { "Hough 1960"
    102 52 -38
    98 "Marshall Islands" 3 3 3 10 1 16 159 175}

"WGS 72" { "WGS 72"
    0 0 5
    99 "" -1 -1 -1 0}

"WGS 84" { "WGS 84"
    0 0 0
    100 "" -1 -1 -1 0}

"Zanderij" { "International 1924"
    -265 120 -358
    101 "Suriname" 5 5 8 5 -10 20 -76 -47}

"Potsdam" { "Bessel 1841"
    587 16 393
    102 "GPStrans has: 606 23 413"}

"ED73" { "WGS 84"
    238 -87 -27
    gm102 ""}

"Lisboa" { "WGS 84"
    183.544 -46.902 -24.135
    gm103 ""}

"American Samoa 1962" { "Clarke 1866"
    -115 118 426
    gm105 "American Samoa Islands" 25 25 25 2 -19 -9 -174 -165}

"Antigua Island Astro 1943" { "Clarke 1880"
    -270 13 62
    gm106 "Also known as: USNHO Astro 1943\nAntigua (Leeward Islands)"
    25 25 25 1 16 20 -65 -61}

"Astro Tern Island (FRIG)" { "International 1924"
    114 -116 -333
    gm107 "1961" 25 25 25 1 22 26 -166 -166}

"Ayabelle Lighthouse" { "Clarke 1880"
    -79 -129 -145
    gm108 "Djibouti" 25 25 25 1 5 20 36 49}

"Bissau" { "International 1924"
    -173 253 27
    gm109 "Guinea-Bissau" 25 25 25 2 5 19 -23 -7}

"Bukit Rimpah" { "Bessel 1841"
    -384 664 -48
    gm110 "Indonesia (Bangka and Belitung Ids)" -1 -1 -1 0 -6 0 103 110}

"Camp Area Astro" { "International 1924"
    -104 -129 239
    gm111 "Antarctica (McMurdo Camp Area)" -1 -1 -1 0 -85 -70 135 180}

"Dabola" { "Clarke 1880"
    -83 37 124
    gm112 "Guinea" 15 15 15 4 1 19 12 11}

"Deception Island" { "Clarke 1880"
    260 12 -147
    gm113 "Deception Island, Antarctica" 20 20 20 3 -65 -62 58 62}

"Fort Thomas 1955" { "Clarke 1880"
    -7 215 225
    gm114 "Nevis, St. Kitts (Leeward Islands)" 25 25 25 2 16 19 -64 -61}

"Gan 1970" { "International 1924"
    -133 -321 50
    gm115 "Republic of Maldives" 25 25 25 1 -2 9 71 75}

"Gunung Segara" { "Bessel 1841"
    -403 684 41
    gm116 "Indonesia (Kalimantan)" -1 -1 -1 0 -6 9 106 121}

"Herat North" { "International 1924"
    -333 -222 114
    gm117 "Afghanistan" -1 -1 -1 0 23 44 55 81}

"Hu-Tzu-Shan" { "International 1924"
    -637 -549 -203
    gm118 "Taiwan; = TWD67" 15 15 15 4 20 28 117 124}

"Indian (India, Nepal)" { "Everest (India 1956)"
    295 736 257
    gm119 "India, Nepal" 12 10 15 7 2 44 62 105}

"Indian (Pakistan)" { "Everest (Pakistan)"
    283 682 231
    gm120 "Pakistan" -1 -1 -1 0 17 44 55 81}

"Indian 1954" { "Everest (India 1830)"
    217 823 299
    gm121 "Thailand" 15 6 12 11 0 27 91 111}

"Indian 1975" { "Everest (India 1830)"
    210 814 289
    gm122 "Thailand; also as: 209 818 290, 12 10 12" 3 2 3 62 0 27 91 111}

"Indonesian 1974" { "Indonesian 1974"
    -24 -15 5
    gm123 "Indonesia" 25 25 25 1 -16 11 89 146}

"ISTS 061 Astro 1968" { "International 1924"
    -794 119 -298
    gm124 "South Georgia Islands" 25 25 25 1 -56 -52 -38 -34}

"Johnston Island 1961" { "International 1924"
    189 -79 -202
    gm125 "Johnston Island"}

"Kusaie Astro 1951" { "International 1924"
    647 1777 -1124
    gm126 "Caroline Islands" 25 25 25 1 -1 12 134 167}

"Leigon" { "Clarke 1880"
    -130 29 364
    gm127 "Ghana" 2 3 2 8 -1 17 -9 7}

"M'Poraloko" { "Clarke 1880"
    -74 -130 42
    gm128 "Gabon" 25 25 25 1 -10 8 3 20}

"Montserrat Island Astro 1958" { "Clarke 1880"
    174 359 365
    gm129 "Montserrat (Leeward Islands)" 25 25 25 1 15 18 -64 -61}

"North Sahara 1959" { "Clarke 1880"
    -186 -93 310
    gm130 "Algeria" 25 25 25 3 13 43 -15 11}

"NTF (Nouvelle Triangulation de France)" { "Clarke 1880"
    -168 -60 320
    gm131 "France (incl. Corsica)"}

"Point 58" { "Clarke 1880"
    -106 -129 165
    gm133 "Mean for Burkina Faso and Niger" 25 25 25 1 0 10 -15 25}

"Pointe Noire 1948" { "Clarke 1880"
    -148 51 -291
    gm134 "Congo" 25 25 25 1 -11 10 5 25}

"Porto Santo 1936" { "International 1924"
    -499 -249 314
    gm135 "Porto Santo, Madeira Islands; = Southeast Base"
    25 25 25 2 31 35 -18 -15}

"Pulkovo 1942" { "Krassovsky 1940"
    28 -130 -95
    gm136 "Russia" -1 -1 -1 0 36 89 -180 180}

"Rijks Driehoeksmeting" { "Bessel 1841"
    593.032 26 478.741
    gm137 "Netherlands; also known as Amersfoort"}

"S-42 (Pulkovo 1942); Albania" { "Krassovsky 1940"
    24 -130 -92
    gm138a "" 3 3 3 7 34 48 14 26}

"S-42 (Pulkovo 1942); Czechoslavakia" { "Krassovsky 1940"
    26 -121 -78
    gm138b "" 3 3 2 6 42 57 6 28}

"S-42 (Pulkovo 1942); Hungary" { "Krassovsky 1940"
    28 -121 -77
    gm138c "" 2 2 2 5 40 54 11 29}

"S-42 (Pulkovo 1942); Kazakhstan" { "Krassovsky 1940"
    15 -130 -84
    gm138d "" 25 25 25 2 35 62 41 93}

"S-42 (Pulkovo 1942); Latvia" { "Krassovsky 1940"
    24 -124 -82
    gm138e "" 2 2 2 5 50 64 15 34}

"S-42 (Pulkovo 1942); Romania" { "Krassovsky 1940"
    28 -121 -77
    gm138f "" 3 5 3 4 38 54 15 35}

"S-42 (Pulkovo 1942); Poland" { "Krassovsky 1940"
    23 -124 -82
    gm138g "" 4 2 4 11 43 60 8 30}

"S-JTSK" { "Bessel 1841"
    589 76 480
    gm139 "Czechoslavakia (Prior 1 JAN 1993)" 4 2 3 6 43 56 6 28}

"Selvagem Grande 1938" { "International 1924"
    -289 -124 60
    gm140 "Salvage Islands" 25 25 25 1 28 32 -18 -14}

"Tananarive Observatory 1925" { "International 1924"
    -189 -242 -91
    gm141 "Madagascar" -1 -1 -1 0 -34 -8 40 53}

"Voirol 1960" { "Clarke 1880"
    -123 -206 219
    gm142 "Algeria" 25 25 25 2 13 43 -15 11}

"Voirol 1874" { "Clarke 1880"
    -73 -247 227
    gm142a "Algeria" -1 -1 -1 0 13 43 -15 11}

"Wake Island Astro 1952" { "International 1924"
    276 -57 149
    gm143 "Wake Atoll" 25 25 25 2 17 21 -176 -171}

"Yacare" { "International 1924"
    -155 171 37
    gm144 "Uruguay" -1 -1 -1 0 -40 -25 -65 -47}

"Austrian (MGI)" { "Bessel 1841"
    -575 -93 -466
    gm145 "Austria"}

"K12 Astro 1955" { "Clarke 1880"
    9 183 236
    gm146 "Nevis, St. Kitts (Leeward Islands)"}

"Indian 1960; Vietnam (Con Son)" { "Everest (India 1830)"
    182 915 344
    gm147 "" 25 25 25 1 6 11 104 109}

"Indian 1960; Vietnam (N16)" { "Everest (India 1830)"
    198 881 317
    gm148 "" 25 25 25 2 11 23 101 115}

"Estonia Coord System 1937" { "Bessel 1841"
    374 150 588
    gm149 "Estonia" 2 3 3 19 52 65 16 34}

"Graciosa Base SW 1948" { "International 1924"
    -104 167 -38
    gm150 "Azores (Faial, Graciosa, Pico, Sao Jorge, Terceira); = Southwest Base"
    3 3 3 5 37 41 -30 -26}

"Hermannskogel" { "Bessel 1841 (Namibia)"
    653 -212 449
    gm151 "Croatia-Serbia, Bosnia-Herzegovina" -1 -1 -1 0}

"Hermannskogel; old Yugoslavia" { "Bessel 1841"
    682 -203 480
    gm151b "" -1 -1 -1 0 35 52 7 29}

"Korean Geodetic System" { "GRS 80"
    0 0 0
    gm152 "South Korea" 2 2 2 12}

"Sierra Leone" { "Clarke 1880"
    -88 4 101 
    gm153 "" 15 15 15 8 1 16 -19 -4}

"Israeli" { "Israeli"
    -235 -85 264
    gm154 "" -1 -1 -1 0}

"South American 69; Brazil/IBGE" { "South American 1969"
    -66.87 4.37 -39
    gm155 "" -1 -1 -1 0}

"Datum 73" { "International 1924"
    223.237 110.193 -36.649
    gm156 "Portugal" -1 -1 -1 0}

"Dionisos" { "GRS 80"
    -200 75 246
    gm157 "" -1 -1 -1 0}

"NTF (NTF ellipsoid)" { "NTF"
    168 60 -320
    gm158 "Nouvelle Triangulation de France" -1 -1 -1 0}

"Rome 1940; Luxembourg" { "International 1924"
    263 -76 -45
    gm159 "" -1 -1 -1 0}

"European 1950; Belgium" { "International 1924"
    -126 80 -101
    gm160 "" -1 -1 -1 0}

"Austrian" { "Bessel 1841"
    594 84 471
    gm161 ""}

"Egypt" { "International 1924"
    -133 -321 50
    gm162 ""}

"NGO 1948" { "Bessel 1841 (Norway)"
    315 -217 528
    gm163 ""}

"Norsk" { "Bessel 1841 (Norway)"
    278 93 474
    gm164 ""}

"Deutsches Hauptdreiecksnetz" { "Bessel 1841"
    598.1 73.7 418.2
    gm164 ""}

"Hungarian Datum 1972" { "GRS 67"
    56.91 -70.18 -9.49
    gm165 "ftp/gps/honlap-jan2002.pdf"}
}

###### known ellipsoids
     # when changing this list make sure you update the definitions below

## description of predefined ellipsoid menu

set PREDEFELLIPSOIDDESCR \
      [list \
         @ [list Airy \
              {Airy 1830} {Modified Airy}] \
         {Andrae 1876} {Appl. Physics. 1965} {Australian National} \
	 @ [list Bessel \
              {Bessel (Portugal)} {Bessel 1841} {Bessel 1841 (Namibia)} \
	      {Bessel 1841 (Norway)}] \
         @ [list Clarke \
	      {Clarke 1858} {Clarke 1866} {Clarke 1880}] \
         {Comm. des Poids et Mesures 1799} {Delambre 1810} {Engelis 1985} \
	 @ [list Everest \
              {Everest (1948)} {Everest (India 1830)} \
              {Everest (India 1956)} {Everest (Malaysia 1969)} \
              {Everest (Pakistan)} {Everest (Sabah Sarawak)}] \
         @ [list Fischer \
              {Fischer 1960 (Mercury)} {Fischer 1968} \
	      {Modified Fischer 1960}] \
         @ [list GRS \
              {GRS 67} {GRS 75} {GRS 80}] \
         {Helmert 1906} \
         @ [list Hough \
              {Hough 1956} {Hough 1960}] \
         {IAU 1976} {Indonesian 1974} {International 1924} Israeli \
	 {Kaula 1961} \
         {Krassovsky 1940} {Lerch 1979} {MERIT 1983} {Maupertius 1738} NTF \
         NWL9D {Plessis 1817} SGS85 {South American 1969} {Southeast Asia} \
         @ [list WGS \
              {WGS 60} {WGS 66} {WGS 72} {WGS 84}] \
         Walbeck]

## ellipsoid definitions
# indexed by name
# each list has:
#  - a (semi-major axis in metre, float), invf (inverse of flattening)
#  - comment

## those after "WGS 66" until "Walbeck" were taken from PROJ4.0

array set ELLPSDDEF {
"Airy 1830" {6377563.396 299.3249646 "used in Great Britain"}

"Andrae 1876" {6377104.43 300.0 "used in Denmark, Iceland"}

"Appl. Physics. 1965" {6378137.0 298.25 ""}

"Australian National" {6378160.0   298.25 ""}

"Bessel 1841" {6377397.155 299.1528128 "used in Germany, Japan"}

"Bessel (Portugal)" {6377397.155 297.152818860 ""}

"Bessel 1841 (Namibia)" {6377483.865 299.1528128 ""}

"Bessel 1841 (Norway)" {6377492.012 299.1528128 ""}

"Clarke 1858" {6378363.63547 294.97870 "a=20926348 USft or ft?"}

"Clarke 1866" {6378206.4   294.9786982 "used in North America"}

"Clarke 1880" {6378249.145 293.465
 "used in France, Africa; also know as: Clarke 1880 (Modified)"}

"Comm. des Poids et Mesures 1799" {6375738.7 334.29 ""}

"Delambre 1810" {6376428.0 311.5 "used in Belgium"}

"Engelis 1985" {6378136.05 298.2566 ""}

"Everest (1948)" {6377304.063 300.8017
 "also known as: Everest (Malaysia+Singapore)"}

"Everest (India 1830)" {6377276.345  300.8017 "used in India"}

"Everest (India 1956)" {6377301.243       300.8017 ""}

"Everest (Malaysia 1969)" {6377295.664    300.8017 ""}

"Everest (Pakistan)" {6377309.613         300.8017 ""}

"Everest (Sabah Sarawak)" {6377298.556    300.8017
 "also known as: Everest (E. Malaysia, Brunei"}

"Fischer 1960 (Mercury)" {6378166.0   298.3 ""}

"Fischer 1968" {6378150.0 298.3 ""}

"GRS 67" {6378160.0   298.247167427 "used in Australia and Hungary"}

"GRS 75" {6378140.0   298.257 ""}

"GRS 80" {6378137.0   298.257222101 "used in Australia"}

"Helmert 1906" {6378200.0   298.3 ""}

"Hough 1956" {6378270.0   297.0 ""}

"Hough 1960" {6378270.0   297.0 ""}

"IAU 1976" {6378140.0 298.257 ""}

"Indonesian 1974" {6378160.0   298.247 ""}

"International 1924" {6378388.0   297.0 "used in Europe, New Zealand"}

"Israeli" {6377973.0 293.466001939}

"Kaula 1961" {6378163.0 298.24 ""}

"Krassovsky 1940" {6378245.0 298.3
 "used in Russia; also known as: Krassovsky 1942)"}

"Lerch 1979" {6378139.0 298.257 ""}

"MERIT 1983" {6378137.0 298.257 ""}

"Maupertius 1738" {6397300.0 191.0 ""}

"Modified Airy" {6377340.189 299.3249646 ""}

"Modified Fischer 1960" {6378155.0   298.3 ""}

"NWL9D" {6378145.0 298.25 "Naval Weapons Lab., 1965"}

"NTF" {6378025.0 303.20855706 "Nouvelle Triangulation de France"}

"Plessis 1817" {6376523.0 308.640997 "used in France"}

"SGS85" {6378136.0 298.257 "Soviet Geodetic System 85"}

"South American 1969" {6378160.0   298.25 ""}

"Southeast Asia" {6378155.0 298.3 ""}

"WGS 60" {6378165.0   298.3 ""}

"WGS 66" {6378145.0   298.25 "used in USA/DoD"}

"WGS 72" {6378135.0   298.26 "used in USA/DoD"}

"WGS 84" {6378137.0   298.257223563 ""}

"Walbeck" {6376896.0 302.780 ""}
}
