#
# This file is part of:
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2013 Miguel Filgueiras migfilg@t-online.de
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#  File: i18n-utf8.tcl
#  Last change:  6 October 2013
#

##### this is a UTF-8 encoded file; do not change its encoding!!!

## names of known languages in their own language

array set TXT {
    LANGdeutsch  Deutsch
    LANGengl   English
    LANGes     Español
    LANGfr     Français
    LANGid     {Bahasa Indonesia}
    LANGit     Italiano
    LANGnl     Nederlands
    LANGport   Português
    LANGru     Русский
}

## possible abbreviations for month names in all known languages
  # English abbreviations must not be deleted

set ALLMONTH(1) "Jan Gen jan Ene Янв"
set ALLMONTH(2) "Feb Fev feb Фев"
set ALLMONTH(3) "Mar MДr mrt Мар"
set ALLMONTH(4) "Apr Abr Avr apr Апр"
set ALLMONTH(5) "May Mai Mag mei Mei Май"
set ALLMONTH(6) "Jun Giu jun Июн"
set ALLMONTH(7) "Jul Lug jul Июл"
set ALLMONTH(8) "Aug Ago Aou aug Agt Авг"
set ALLMONTH(9) "Sep Set sep Сен"
set ALLMONTH(10) "Oct Okt Out Ott okt Окт"
set ALLMONTH(11) "Nov nov Nop Ноя"
set ALLMONTH(12) "Dec Dez Dic dec Des Дек"


