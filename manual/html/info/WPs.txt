% Written by GPSManager 04-Jul-2002 19:03:58 (GMT)
% Edit at your own risk!

!Format: DMS 0 Lisboa
!Creation: no

!W:
!Datum: WGS 84
XINZO	LARC PISCINAS	42 03 28 	-7 43 43	symbol=swimming
!NB:	Xinzo de Limia (piscinas)
Baliza 16 Larouco 97

!Position: UTM/UPS
!Datum: European 1950
BORFUT	BRNES M CAV FUT	29	T	669620	4600220	symbol=stadium
!NB:	Macedo de Cavaleiros, campo de futebol
G2	Bornes 99

!Position: DMS
!Datum: WGS 84
XIROND	LAROUCO	41 54 0	-7 38 16	symbol=church
!NB:	Xironda (igreja)
Baliza 6 Larouco 97

!Position: UTM/UPS
!Datum: European 1950
BORGRJ	BRNES GRIJO BAL	29	T	669100	4596250	symbol=house
!NB:	Grij�, telhado vermelho
B18	Bornes 99

!Position: DMS
!Datum: WGS 84
-AVE-	IC1 RIO AVE	N41 21 59.7	W8 43 23.8	symbol=bridge
!NB:	Ponte sobre o Ave
IC1

!Position: UTM/UPS
!Datum: European 1950
BORIZD	BRNES IZEDA PRS	29	T	689540	4604120	symbol=monument
!NB:	Izeda, pris�o
G3	Bornes 99

!Position: DMS
!Datum: WGS 84
-CAVD-	IC1 CAVADO	N41 30 47.2	W8 45 14.9	symbol=bridge
!NB:	Ponte sobre o C�vado
IC1

!Position: UTM/UPS
!Datum: European 1950
BORMCV	BRNES M CAV ARM	29	T	669790	4599540	symbol=factory
!NB:	Macedo de Cavaleiros, armaz�m
B5	Bornes 99

!Position: DMS
!Datum: WGS 84
CSTQJO	CAST QUEIJO PRT	N41 10 01.4	W8 41 21.2	symbol=castle
!NB:	Castelo do Queijo
Porto

!Position: UTM/UPS
!Datum: European 1950
BORMIR	BRNS MIRDLA AERO	29	T	648100	4592520	symbol=airport
!NB:	Mirandela, aer�dromo
G6	Bornes 99

BORMRS	BRNES MORAIS	29	T	685640	4595820	symbol=church
!NB:	Morais, igreja
B15	Bornes 99

!Position: DMS
!Datum: WGS 84
BORNES	DESCOLAGEM NW	41 27 50.2 	-6 57 50.7	symbol=take_off
!NB:	Serra de Bornes

!Position: UTM/UPS
!Datum: European 1950
BORPVG	BORNES P VIGIA	29	T	671530	4593380	symbol=short_tower
!NB:	S Bornes, posto de vigia
B1	Bornes 99

BORQCM	BRNES Q CRUZ MOS	29	T	682580	4620440	symbol=house
!NB:	Quinta do Cruzamento de M�s
B11	Bornes 99

BORRSS	BRNES ROSSAS FUT	29	T	680680	4615060	symbol=stadium
!NB:	Rossas, campo de futebol
G4	Bornes 99

BORSAM	BRNES S AMBROSIO	29	T	677150	4599680	symbol=chapel
!NB:	Santu�rio do Santo Ambr�sio
B16	Bornes 99

BORSND	BRNES SENDAS	29	T	678680	4607120	symbol=beach
!NB:	Sendas, areal
B14	Bornes 99

BORVBF	BRNS VBFEITO BAL	29	T	667086	4593620	symbol=house
!NB:	Vale Benfeito, casa em constru��o
B4	Bornes 99

!Position: DMS
!Datum: WGS 84
BOTICA	BOTICAS	41 41 42.4 	-7 40 02.7	symbol=small_city
BRGNAE	BRAGANCA AERODR	N41 51 18.0	W6 42 23.0	symbol=airport
BOULSA	LARC BOULLOSA	41 55 26	-7 45 12	symbol=church
!NB:	Boullosa (igreja)
Baliza 7 Larouco 97

!Position: UTM/UPS
!Datum: European 1950
BRGNCT	BRAGANCA CASTELO	29	T	687108	4630748	symbol=castle
!NB:	B10	Bornes 99

BRGNNB	BRAGANCA NERBA	29	T	684544	4628530
!NB:	Nerba de Bragan�a
B11	Bornes 99

!Position: DMS
!Datum: WGS 84
CABSTO	CABECEIRAS BASTO	41 30 58.0 	-7 59 29.1	symbol=small_city
CASTNH	CASTANHEIRA	N41 47 41.9	W7 49 08.5	symbol=take_off
!NB:	Castanheira
Larouco

CEBOLA	ESTR SERRA DA	40 10 50	-7 48 35	symbol=elevation
!NB:	Serra da Cebola
Baliza 1 Linhares 97

CHA	LARC IGREJA	N41 47 06.0	W7 47 08.0	symbol=church
CHAVES	B7 LR FPVL 97	41 43 0	-7 27 0	symbol=airport
!NB:	Chaves (aer�dromo)
Baliza 7 Enc FPVL Larouco 97

COVLHA	ESTR AERODROMO	40 15 55	-7 28 50	symbol=airport
!NB:	Covilh� (aer�dromo)
Baliza 4 Linhares 97

CPFIGR	CTRL PEDRA FIGRA	40 17 00	-7 35 20	symbol=dam
!NB:	Central da Pedra da Figueira
Baliza 5 Linhares 97

COVLHS	ESTR SANATORIO	40 17 30	-7 31 0	symbol=1st_aid
!NB:	Covilh� (sanat�rio)
Baliza 9 Linhares 97

!Position: UTM/UPS
CRANDN	CALVOS RANDIN	29	T	591533	4644374	symbol=stadium
!NB:	Calvos de Rand�n (pav desportos)

!Position: DMS
CRVLHH	CARVALHELHOS	41 41 17.0 	-7 43 43.9	symbol=small_city
!NB:	(no local)

CUALDR	LARC CUALEDRO	41 59 28	-7 35 32	symbol=church
!NB:	Cualedro (igreja)
Baliza 11 Larouco 97

ERADA	ESTRELA	40 13 30	-7 38 55	symbol=small_city
!NB:	Baliza 2 Linhares 97

FLGSNH	ESTR FOLGOSINHO	40 30 40	-7 30 40	symbol=small_city
!NB:	Baliza 12 Linhares 97

FRSTRS	LARC FRIESTRAS	42 0 23	-7 40 35	symbol=factory
!NB:	Friestras (pocilgas)
Baliza 12 Larouco 97

GOUVAE	ALVAO GOUVAES	41 28 46.9 	-7 43 54.2	symbol=small_city
!NB:	Gouv�es (Alv�o)

GRALHS	LARC GRALHAS	41 50 58	-7 42 12	symbol=church
!NB:	Gralhas (igreja)
Baliza 6 Enc FPVL Larouco 97

GUDINA	A GUDINA	42 03 53.3 	-7 08 19.6	symbol=small_city
!NB:	A Gudi�a (no local)

GRIJO	BORNES	41 29 56.7 	-6 58 17.1	symbol=small_city
!NB:	Grij�
Serra de Bornes

LAMA-O	ALVAO LAMAS OLO	41 22 20.1 	-7 47 37.0	symbol=small_city
!NB:	Lamas de Olo (Alv�o)

QUIAIO	QUIAIOS	N40 12 08.6	W8 53 33.6	symbol=take_off
!NB:	Quiaios

LAMAS	ALVAO LAMAS	41 27 11.0 	-7 46 05.4	symbol=small_city
RABACA	SICO RABACAL	N40 02 57.0	W8 27 50.6	symbol=take_off
!NB:	ZC

LAR-AN	LAROUCO ATR N	N41 55 59.5	W7 43 02.6	symbol=landing
!NB:	Larouco - aterragem Norte

REDNHA	SICO REDINHA	39 58 30.3 	-8 33 11.0	symbol=take_off
LAR-AS	LAROUCO ATR S	N41 51 29.6	W7 42 44.9	symbol=landing
!NB:	Larouco - aterragem Sul

RIOS	LARC VERIN	41 58 08.8 	-7 16 31.9	symbol=small_city
!NB:	Rios (prox Verin)

!Position: UTM/UPS
LARC-C	LAROUCO CASETA	29	T	609500	4643500	symbol=house
!Position: DMS
S-GRCA	ALVAO SRA GRACA	41 25 00.0 	-7 55 00.4	symbol=take_off
!NB:	Sra da Gra�a (Mondim de Basto)

LARC-N	LAROUCO NORTE	N41 55 02.0	W7 42 58.9	symbol=take_off
!NB:	Larouco - descolagem N

SAMARD	ALVAO SAMARDAES	41 24 27.0 	-7 43 16.2	symbol=small_city
!NB:	Samard�es (Alv�o)

LARC-S	LAROUCO SUL	41 52 49.0 	-7 43 14.0	symbol=take_off
!NB:	Larouco - descolagem S

SANABR	PUEBLA SANABRIA	42 07 17.1 	-6 42 53.8	symbol=small_city
!NB:	Puebla de Sanabria

LARC-W	LAROUCO OESTE	41 53 04.2 	-7 43 35.7	symbol=take_off
SANDRE	LARC STO ANDRE	41 51 55	-7 40 1	symbol=small_city
!NB:	Baliza 8(2) Larouco 97

LGCOMP	LAGOA COMPRIDA	40 21 45	-7 39 0	symbol=buoy_blue
!NB:	Lagoa Comprida
Baliza 8 Linhares 97

LAROA	LARC S PEDRO	42 01 31	-7 41 51	symbol=church
!NB:	S. Pedro de Laroa (igreja)
Baliza 27 Enc FPVL Larouco 97

SANTNN	LARC S ANTONINO	41 56 27	-7 44 11	symbol=small_city
!NB:	Baliza 17(2) Larouco 97

LINDOS	LINDOSO	41 52 16.1 	-8 11 38.3	symbol=small_city
!NB:	Lindoso

-ESPSD	ESPOSENDE B RED	N41 32 57.2	W8 47 12.3	symbol=house
!NB:	Bou�a da Redonda
Esposende

SEIA	DESCOLAGEM	40 25 15.3 	-7 38 11.2	symbol=take_off
SARAUS	LARC SARREAUS	42 05 22	-7 36 10	symbol=swimming
!NB:	Sarreaus (piscinas)
Baliza 24(2) Larouco 97

LINHRS	ESTR LINHARES	40 31 59.2 	-7 26 45.9	symbol=take_off
!NB:	Linhares (descolagem)

-PORTO	01-AUG-97 1235	41 08 59.4 	-8 39 27.7	symbol=house
SEIA-A	AERODROMO	40 27 10	-7 41 0	symbol=airport
!NB:	Baliza 10 Linhares 97

LODSLO	LARC LODOSELO	42 4 18	-7 35 36	symbol=church
!NB:	Lodoselo (igreja)
Baliza 23(2) Larouco 97

ALBARE	ALBARELLOS-MONTE	41 57 19.9 	-7 29 45.0	symbol=small_city
!NB:	Albarellos de Monterey
	(no local)

SEIA-G	BOMBAS GASOLINA	40 24 45 	-7 40 30	symbol=fuel
!NB:	Baliza 11 Linhares 97

LOUSA	05-MAY-01 1243	N40 06 16.9	W8 12 30.8	symbol=take_off
ALGODR	FORNOS ALGODRES	N40 39 02.6	W7 30 38.6	symbol=take_off
!NB:	ZC

SENDIM	LAROUCO	41 53 27	-7 45 54	symbol=small_city
!NB:	Baliza 4 Larouco 97

LOUSAA	05-MAY-01 1512	N40 07 29.8	W8 13 10.8	symbol=landing
ALIJO	09-SEP-97 1613	41 16 42.2 	-7 28 22.6	symbol=small_city
SHELEN	TAROUCA	N41 00 37.6	W7 48 26.8	symbol=take_off
LUBIAN	LAROUCO	42 02 06.4 	-6 54 40.8	symbol=small_city
!Position: UTM/UPS
ALLRIZ	ALLARIZ MOSTEIRO	29	T	599300	4671200	symbol=church
SLVEIR	SOLVEIRA IGREJA	29	T	610500	4633500	symbol=church
!Position: DMS
LUCNZA	LARC LUCENZA	41 57 13	-7 37 24	symbol=small_city
!NB:	Baliza 20(2) Larouco 97

ALVADI	ALVAO ALVADIA	41 26 06.6 	-7 46 49.7	symbol=small_city
!NB:	Alvadia (Alv�o)

!Position: UTM/UPS
SNDIAS	SANDIAS ESCOLAS	29	T	603200	4663500	symbol=school
!Position: DMS
MANZAN	MANZANEDA	42 15 35.5 	-7 17 53.1	symbol=small_city
!NB:	Manzaneda

ALVADO	COSTA DE ALVADOS	N39 32 36.3	W8 47 15.0	symbol=take_off
!NB:	ZC

TEIXRA	CRUZMT TEIXEIRA	40 14 55	-7 43 30	symbol=street_int
!NB:	Cruzamento da Teixeira
Baliza 3 Linhares 97

MONTLG	MONTALEGRE CAST	41 49 30.0 	-7 47 25.0	symbol=castle
!NB:	Montalegre (castelo)
Baliza 3 Larouco 97

MEIXDO	LARC MEIXEDO	41 50 1	-7 44 20	symbol=small_city
!NB:	Meixedo
Baliza 11(2) Larouco 97

AMARAN	AMARANTE	41 16 00.9 	-8 05 38.4	symbol=medium_city
TORRE	ESTRELA	40 19 20	-7 38 10	symbol=short_tower
!NB:	Baliza 7 Linhares 97

MURCA	09-SEP-97 1615	41 24 15.4 	-7 27 28.9	symbol=small_city
-A1LH7	04-AUG-97 1450	40 27 32.6 	-7 38 32.7
!NB:	Aterragem Manga 1
Linhares 97

ARGANI	ARGANIL	N40 13 30.6	W7 58 27.9	symbol=take_off
!NB:	ZC

ATAIJA	SICO	N39 32 58.8	W8 52 04.5	symbol=take_off
!NB:	ZC

TRSMIR	LARC TRASMIRAS	42 1 25	-7 36 56	symbol=church
!NB:	Trasmiras (igreja)
Baliza 15 Larouco 97

TRPENA	LARC TORRE PENA	42 5 21	-7 40 36	symbol=small_city
!NB:	Torre de Pena
Baliza 19 Larouco 97

NINOAG	NINO D AGUIA	41 57 14	-7 40 44	symbol=church
!NB:	Ni�o d'Aguia (igreja)
Baliza 9 Larouco 97

-A2LH7	05-AUG-97 1312	40 25 58.2 	-7 38 32.4
!NB:	Aterragem Manga 2
Linhares 97

BAGEIX	BORNES BAGUEIXE	41 34 03.3 	-6 46 21.2	symbol=small_city
!NB:	Serra de Bornes

UNHAIS	DESCOLAGEM	40 18 27.0 	-7 36 27.5	symbol=take_off
!NB:	Unhais (descolagem)

PDONOE	LARC PTE DONOES	41 49 32	-7 48 43	symbol=bridge
!NB:	Ponte de Don�es
Baliza 13(2) Larouco 97

-A2LRF	23-AUG-97 0923	41 55 32.2 	-7 46 06.7
!NB:	Aterragem Manga 2
Encontro FPVL Larouco 97

BAIONA	DESCOLAGEM NE	42 04 24.7 	-8 49 39.8	symbol=take_off
!NB:	Baiona (descolagem NE)
	(no local)

V-SECA	LARC AERODROMO	42 03 04	-7 35 58	symbol=ultralight
!NB:	Vila Seca (campo ultralights)
Baliza 25(2) Larouco 97

PDRNLS	LARC PADORNELOS	41 51 41	-7 45 12	symbol=small_city
!NB:	Baliza 7(2) Larouco 97

EIRNHS	25-AUG-97 1304	41 03 42.0 	-8 15 52.2
!NB:	Eirinhas, Castelo de Paiva

BALTAR	LARC CTR MEDICO	41 56 49	-7 43 14	symbol=1st_aid
!NB:	Baltar (centro m�dico)
Baliza 18 Enc FPVL Larouco 97

VBARRI	VILAR DE BARRIO	42 9 40	-7 36 38	symbol=small_city
!NB:	Baliza 22 Larouco 97

PEGAS	VALE DE PEGAS	N40 04 45.6	W8 27 46.6	symbol=take_off
!NB:	ZC

TZC000	BORNES TERMICA	41 31 55.1 	-6 49 38.6
!NB:	Base de t�rmica ZC 980628
Serra de Bornes

BILHO	ALVAO	41 24 31.6 	-7 50 26.8	symbol=small_city
!NB:	Bilh� (Alv�o)

VBFEIT	VALE BENFEITO	41 28 59.2 	-6 58 59.2	symbol=small_city
!NB:	Serra de Bornes

PINHAO	09-SEP-97 1618	41 11 21.3 	-7 32 47.6	symbol=small_city
TZC001	BORNES TERMICA	41 31 26.5 	-6 50 41.5
!NB:	Base de t�rmica ZC 980628
Serra de Bornes

!Position: UTM/UPS
BLNCOS	LARC OS BLANCOS	29	T	603600	4650800	symbol=small_city
!Position: DMS
VDNOVA	VENDA NOVA	41 40 13.8 	-7 57 19.9	symbol=dam
!NB:	Venda Nova (barragem)

PISOES	LARC BARRAGEM	41 44 28	-7 51 20	symbol=dam
!NB:	Pis�es (torre N barragem)
Baliza 2 Enc FPVL Larouco 97

TZC002	BORNES TERMICA	41 30 07.7 	-6 53 57.0
!NB:	Base de t�rmica ZC 980628
Serra de Bornes

!Position: UTM/UPS
!Datum: European 1950
BORAFB	BRNES A FE BARR	29	T	671900	4582130	symbol=dam
!NB:	Alf�ndefa da F�, barragem
G5	Bornes 99

!Position: DMS
!Datum: WGS 84
VERIN	09-SEP-97 2337	41 56 40.2 	-7 26 42.3	symbol=small_city
PITOES	LARC DAS JUNIAS	41 50 23.5 	-7 56 46.0	symbol=small_city
!NB:	Pit�es das J�nias (no local)

TZC003	BORNES TERMICA	41 29 56.1 	-6 54 25.2
!NB:	Base de t�rmica ZC 980628
Serra de Bornes

!Position: UTM/UPS
!Datum: European 1950
BORAFE	BRNES ALFANDG FE	29	T	670600	4579050	symbol=store
!NB:	Alf�ndega da F�, mercado
B17	Bornes 99

!Datum: WGS 84
VERINA	VERIN ANTENA	29	T	636573	4655616	symbol=short_tower
!Position: DMS
PNHDRD	PENHAS DOURADAS	40 24 15	-7 33 55	symbol=elevation
!NB:	Baliza 13 Linhares 97

!Position: UTM/UPS
!Datum: European 1950
BORANT	BRNES ANTENAS	29	T	666700	4588900	symbol=short_tower
!NB:	S Bornes, antenas
B2	Bornes 99

!Datum: WGS 84
VIDFRR	VIDEFERRE IGREJA	29	T	617500	4634200	symbol=church
!NB:	Videferre (igreja)

!Datum: European 1950
BORBAZ	BRNES BARR AZIBO	29	T	675250	4605830	symbol=dam
!NB:	Barragem do Azibo
B8	Bornes 99

BORBLS	BRNES BALSEMAO	29	T	679180	4593860	symbol=chapel
!NB:	Santu�rio de Balsem�o
B9	Bornes 99

!Position: DMS
!Datum: WGS 84
VINHAI	VINHAIS	41 50 19.6 	-7 00 50.7	symbol=small_city
VILDEV	VILAR DEVOS	41 54 25.1 	-7 20 14.9	symbol=small_city
!NB:	Vilar Devos (prox Verin)

!Position: UTM/UPS
!Datum: European 1950
BORCGR	BRNES C GUARDA	29	T	674330	4596740	symbol=house
!NB:	S Bornes, casa do guarda
B6	Bornes 99

!Position: DMS
!Datum: WGS 84
VPERDZ	VILAR PERDIZES	N41 51 21.0	W7 38 05.0	symbol=church
!NB:	Vilar de Perdizes (igreja)

!Position: UTM/UPS
!Datum: European 1950
BORCHA	BRNES CHAO CAPL	29	T	679540	4610770	symbol=chapel
!NB:	Ch�os, capela
B13	Bornes 99

!Position: DMS
!Datum: WGS 84
VPOUCA	V POUCA AGUIAR	41 29 48.2 	-7 38 51.1	symbol=small_city
!NB:	Vila Pouca de Aguiar

!Position: UTM/UPS
!Datum: European 1950
BORCRZ	BRNES CRZ SAMBD	29	T	665670	4590420	symbol=street_int
!NB:	S Bornes, cruzamento para Sambade
B3	Bornes 99

!Position: DMS
!Datum: WGS 84
VREAL	VILA REAL	41 17 58.9 	-7 44 49.8	symbol=medium_city
!Position: UTM/UPS
!Datum: European 1950
BORFAB	BRNES FABRICA M	29	T	674350	4599110	symbol=factory
!NB:	F�brica Mitalco
B7	Bornes 99


!Position: DMS
!Datum: Lisboa
!W:
!Datum: WGS 84
XINZO	LARC PISCINAS	42 03 28 	-7 43 43	symbol=swimming
!NB:	Xinzo de Limia (piscinas)
Baliza 16 Larouco 97

!Position: UTM/UPS
!Datum: European 1950
BORFUT	BRNES M CAV FUT	29	T	669620	4600220	symbol=stadium
!NB:	Macedo de Cavaleiros, campo de futebol
G2	Bornes 99

!Position: DMS
!Datum: WGS 84
XIROND	LAROUCO	41 54 0	-7 38 16	symbol=church
!NB:	Xironda (igreja)
Baliza 6 Larouco 97

!Position: UTM/UPS
!Datum: European 1950
BORGRJ	BRNES GRIJO BAL	29	T	669100	4596250	symbol=house
!NB:	Grij�, telhado vermelho
B18	Bornes 99

BORIZD	BRNES IZEDA PRS	29	T	689540	4604120	symbol=monument
!NB:	Izeda, pris�o
G3	Bornes 99

BORMCV	BRNES M CAV ARM	29	T	669790	4599540	symbol=factory
!NB:	Macedo de Cavaleiros, armaz�m
B5	Bornes 99

BORMIR	BRNS MIRDLA AERO	29	T	648100	4592520	symbol=airport
!NB:	Mirandela, aer�dromo
G6	Bornes 99

BORMRS	BRNES MORAIS	29	T	685640	4595820	symbol=church
!NB:	Morais, igreja
B15	Bornes 99

!Position: DMS
!Datum: WGS 84
BORNES	DESCOLAGEM NW	41 27 50.2 	-6 57 50.7	symbol=take_off
!NB:	Serra de Bornes

!Position: UTM/UPS
!Datum: European 1950
BORPVG	BORNES P VIGIA	29	T	671530	4593380	symbol=short_tower
!NB:	S Bornes, posto de vigia
B1	Bornes 99

BORQCM	BRNES Q CRUZ MOS	29	T	682580	4620440	symbol=house
!NB:	Quinta do Cruzamento de M�s
B11	Bornes 99

BORRSS	BRNES ROSSAS FUT	29	T	680680	4615060	symbol=stadium
!NB:	Rossas, campo de futebol
G4	Bornes 99

BORSAM	BRNES S AMBROSIO	29	T	677150	4599680	symbol=chapel
!NB:	Santu�rio do Santo Ambr�sio
B16	Bornes 99

BORSND	BRNES SENDAS	29	T	678680	4607120	symbol=beach
!NB:	Sendas, areal
B14	Bornes 99

BORVBF	BRNS VBFEITO BAL	29	T	667086	4593620	symbol=house
!NB:	Vale Benfeito, casa em constru��o
B4	Bornes 99

!Position: DMS
!Datum: WGS 84
BOULSA	LARC BOULLOSA	41 55 26	-7 45 12	symbol=church
!NB:	Boullosa (igreja)
Baliza 7 Larouco 97

BRGNAE	BRAGANCA AERODR	N41 51 18.0	W6 42 23.0	symbol=airport
!Position: UTM/UPS
!Datum: European 1950
BRGNCT	BRAGANCA CASTELO	29	T	687108	4630748	symbol=castle
!NB:	B10	Bornes 99

BRGNNB	BRAGANCA NERBA	29	T	684544	4628530
!NB:	Nerba de Bragan�a
B11	Bornes 99

!Position: DMS
!Datum: WGS 84
CABSTO	CABECEIRAS BASTO	41 30 58.0 	-7 59 29.1	symbol=small_city
CASTNH	CASTANHEIRA	N41 47 41.9	W7 49 08.5	symbol=take_off
!NB:	Castanheira
Larouco

CEBOLA	ESTR SERRA DA	40 10 50	-7 48 35	symbol=elevation
!NB:	Serra da Cebola
Baliza 1 Linhares 97

CHA	LARC IGREJA	N41 47 06.0	W7 47 08.0	symbol=church
CHAVES	B7 LR FPVL 97	41 43 0	-7 27 0	symbol=airport
!NB:	Chaves (aer�dromo)
Baliza 7 Enc FPVL Larouco 97

COVLHA	ESTR AERODROMO	40 15 55	-7 28 50	symbol=airport
!NB:	Covilh� (aer�dromo)
Baliza 4 Linhares 97

COVLHS	ESTR SANATORIO	40 17 30	-7 31 0	symbol=1st_aid
!NB:	Covilh� (sanat�rio)
Baliza 9 Linhares 97

CPFIGR	CTRL PEDRA FIGRA	40 17 00	-7 35 20	symbol=dam
!NB:	Central da Pedra da Figueira
Baliza 5 Linhares 97

!Position: UTM/UPS
CRANDN	CALVOS RANDIN	29	T	591533	4644374	symbol=stadium
!NB:	Calvos de Rand�n (pav desportos)

!Position: DMS
CRVLHH	CARVALHELHOS	41 41 17.0 	-7 43 43.9	symbol=small_city
!NB:	(no local)

CUALDR	LARC CUALEDRO	41 59 28	-7 35 32	symbol=church
!NB:	Cualedro (igreja)
Baliza 11 Larouco 97

ERADA	ESTRELA	40 13 30	-7 38 55	symbol=small_city
!NB:	Baliza 2 Linhares 97

FLGSNH	ESTR FOLGOSINHO	40 30 40	-7 30 40	symbol=small_city
!NB:	Baliza 12 Linhares 97

FRSTRS	LARC FRIESTRAS	42 0 23	-7 40 35	symbol=factory
!NB:	Friestras (pocilgas)
Baliza 12 Larouco 97

GOUVAE	ALVAO GOUVAES	41 28 46.9 	-7 43 54.2	symbol=small_city
!NB:	Gouv�es (Alv�o)

GRALHS	LARC GRALHAS	41 50 58	-7 42 12	symbol=church
!NB:	Gralhas (igreja)
Baliza 6 Enc FPVL Larouco 97

GRIJO	BORNES	41 29 56.7 	-6 58 17.1	symbol=small_city
!NB:	Grij�
Serra de Bornes

LAMA-O	ALVAO LAMAS OLO	41 22 20.1 	-7 47 37.0	symbol=small_city
!NB:	Lamas de Olo (Alv�o)

LAMAS	ALVAO LAMAS	41 27 11.0 	-7 46 05.4	symbol=small_city
LAR-AN	LAROUCO ATR N	N41 55 59.5	W7 43 02.6	symbol=landing
!NB:	Larouco - aterragem Norte

LAR-AS	LAROUCO ATR S	N41 51 29.6	W7 42 44.9	symbol=landing
!NB:	Larouco - aterragem Sul

!Position: UTM/UPS
LARC-C	LAROUCO CASETA	29	T	609500	4643500	symbol=house
!Position: DMS
LARC-N	LAROUCO NORTE	N41 55 02.0	W7 42 58.9	symbol=take_off
!NB:	Larouco - descolagem N

S-GRCA	ALVAO SRA GRACA	41 25 00.0 	-7 55 00.4	symbol=take_off
!NB:	Sra da Gra�a (Mondim de Basto)

LARC-S	LAROUCO SUL	41 52 49.0 	-7 43 14.0	symbol=take_off
!NB:	Larouco - descolagem S

SAMARD	ALVAO SAMARDAES	41 24 27.0 	-7 43 16.2	symbol=small_city
!NB:	Samard�es (Alv�o)

LARC-W	LAROUCO OESTE	41 53 04.2 	-7 43 35.7	symbol=take_off
SANDRE	LARC STO ANDRE	41 51 55	-7 40 1	symbol=small_city
!NB:	Baliza 8(2) Larouco 97

LAROA	LARC S PEDRO	42 01 31	-7 41 51	symbol=church
!NB:	S. Pedro de Laroa (igreja)
Baliza 27 Enc FPVL Larouco 97

LGCOMP	LAGOA COMPRIDA	40 21 45	-7 39 0	symbol=buoy_blue
!NB:	Lagoa Comprida
Baliza 8 Linhares 97

SANTNN	LARC S ANTONINO	41 56 27	-7 44 11	symbol=small_city
!NB:	Baliza 17(2) Larouco 97

SARAUS	LARC SARREAUS	42 05 22	-7 36 10	symbol=swimming
!NB:	Sarreaus (piscinas)
Baliza 24(2) Larouco 97

LINHRS	ESTR LINHARES	40 31 59.2 	-7 26 45.9	symbol=take_off
!NB:	Linhares (descolagem)

SEIA	DESCOLAGEM	40 25 15.3 	-7 38 11.2	symbol=take_off
ALBARE	ALBARELLOS-MONTE	41 57 19.9 	-7 29 45.0	symbol=small_city
!NB:	Albarellos de Monterey
	(no local)

LODSLO	LARC LODOSELO	42 4 18	-7 35 36	symbol=church
!NB:	Lodoselo (igreja)
Baliza 23(2) Larouco 97

SEIA-A	AERODROMO	40 27 10	-7 41 0	symbol=airport
!NB:	Baliza 10 Linhares 97

SEIA-G	BOMBAS GASOLINA	40 24 45 	-7 40 30	symbol=fuel
!NB:	Baliza 11 Linhares 97

SENDIM	LAROUCO	41 53 27	-7 45 54	symbol=small_city
!NB:	Baliza 4 Larouco 97

!Position: UTM/UPS
ALLRIZ	ALLARIZ MOSTEIRO	29	T	599300	4671200	symbol=church
SLVEIR	SOLVEIRA IGREJA	29	T	610500	4633500	symbol=church
!Position: DMS
LUCNZA	LARC LUCENZA	41 57 13	-7 37 24	symbol=small_city
!NB:	Baliza 20(2) Larouco 97

ALVADI	ALVAO ALVADIA	41 26 06.6 	-7 46 49.7	symbol=small_city
!NB:	Alvadia (Alv�o)

!Position: UTM/UPS
SNDIAS	SANDIAS ESCOLAS	29	T	603200	4663500	symbol=school
!Position: DMS
MONTLG	MONTALEGRE CAST	41 49 30.0 	-7 47 25.0	symbol=castle
!NB:	Montalegre (castelo)
Baliza 3 Larouco 97

MEIXDO	LARC MEIXEDO	41 50 1	-7 44 20	symbol=small_city
!NB:	Meixedo
Baliza 11(2) Larouco 97

TEIXRA	CRUZMT TEIXEIRA	40 14 55	-7 43 30	symbol=street_int
!NB:	Cruzamento da Teixeira
Baliza 3 Linhares 97

AMARAN	AMARANTE	41 16 00.9 	-8 05 38.4	symbol=medium_city
TORRE	ESTRELA	40 19 20	-7 38 10	symbol=short_tower
!NB:	Baliza 7 Linhares 97

TRSMIR	LARC TRASMIRAS	42 1 25	-7 36 56	symbol=church
!NB:	Trasmiras (igreja)
Baliza 15 Larouco 97

TRPENA	LARC TORRE PENA	42 5 21	-7 40 36	symbol=small_city
!NB:	Torre de Pena
Baliza 19 Larouco 97

NINOAG	NINO D AGUIA	41 57 14	-7 40 44	symbol=church
!NB:	Ni�o d'Aguia (igreja)
Baliza 9 Larouco 97

BAGEIX	BORNES BAGUEIXE	41 34 03.3 	-6 46 21.2	symbol=small_city
!NB:	Serra de Bornes

PDONOE	LARC PTE DONOES	41 49 32	-7 48 43	symbol=bridge
!NB:	Ponte de Don�es
Baliza 13(2) Larouco 97

UNHAIS	DESCOLAGEM	40 18 27.0 	-7 36 27.5	symbol=take_off
!NB:	Unhais (descolagem)

V-SECA	LARC AERODROMO	42 03 04	-7 35 58	symbol=ultralight
!NB:	Vila Seca (campo ultralights)
Baliza 25(2) Larouco 97

PDRNLS	LARC PADORNELOS	41 51 41	-7 45 12	symbol=small_city
!NB:	Baliza 7(2) Larouco 97

BALTAR	LARC CTR MEDICO	41 56 49	-7 43 14	symbol=1st_aid
!NB:	Baltar (centro m�dico)
Baliza 18 Enc FPVL Larouco 97

VBARRI	VILAR DE BARRIO	42 9 40	-7 36 38	symbol=small_city
!NB:	Baliza 22 Larouco 97

BILHO	ALVAO	41 24 31.6 	-7 50 26.8	symbol=small_city
!NB:	Bilh� (Alv�o)

!Position: UTM/UPS
BLNCOS	LARC OS BLANCOS	29	T	603600	4650800	symbol=small_city
!Position: DMS
VBFEIT	VALE BENFEITO	41 28 59.2 	-6 58 59.2	symbol=small_city
!NB:	Serra de Bornes

PISOES	LARC BARRAGEM	41 44 28	-7 51 20	symbol=dam
!NB:	Pis�es (torre N barragem)
Baliza 2 Enc FPVL Larouco 97

!Position: UTM/UPS
!Datum: European 1950
BORAFB	BRNES A FE BARR	29	T	671900	4582130	symbol=dam
!NB:	Alf�ndefa da F�, barragem
G5	Bornes 99

!Position: DMS
!Datum: WGS 84
PITOES	LARC DAS JUNIAS	41 50 23.5 	-7 56 46.0	symbol=small_city
!NB:	Pit�es das J�nias (no local)

VERIN	09-SEP-97 2337	41 56 40.2 	-7 26 42.3	symbol=small_city
!Position: UTM/UPS
!Datum: European 1950
BORAFE	BRNES ALFANDG FE	29	T	670600	4579050	symbol=store
!NB:	Alf�ndega da F�, mercado
B17	Bornes 99

!Datum: WGS 84
VERINA	VERIN ANTENA	29	T	636573	4655616	symbol=short_tower
!Datum: European 1950
BORANT	BRNES ANTENAS	29	T	666700	4588900	symbol=short_tower
!NB:	S Bornes, antenas
B2	Bornes 99

!Position: DMS
!Datum: WGS 84
PNHDRD	PENHAS DOURADAS	40 24 15	-7 33 55	symbol=elevation
!NB:	Baliza 13 Linhares 97

!Position: UTM/UPS
!Datum: European 1950
BORBAZ	BRNES BARR AZIBO	29	T	675250	4605830	symbol=dam
!NB:	Barragem do Azibo
B8	Bornes 99

BORBLS	BRNES BALSEMAO	29	T	679180	4593860	symbol=chapel
!NB:	Santu�rio de Balsem�o
B9	Bornes 99

BORCGR	BRNES C GUARDA	29	T	674330	4596740	symbol=house
!NB:	S Bornes, casa do guarda
B6	Bornes 99

!Position: DMS
!Datum: WGS 84
VPERDZ	VILAR PERDIZES	N41 51 21.0	W7 38 05.0	symbol=church
!NB:	Vilar de Perdizes (igreja)

!Position: UTM/UPS
!Datum: European 1950
BORCHA	BRNES CHAO CAPL	29	T	679540	4610770	symbol=chapel
!NB:	Ch�os, capela
B13	Bornes 99

!Position: DMS
!Datum: WGS 84
VPOUCA	V POUCA AGUIAR	41 29 48.2 	-7 38 51.1	symbol=small_city
!NB:	Vila Pouca de Aguiar

!Position: UTM/UPS
!Datum: European 1950
BORCRZ	BRNES CRZ SAMBD	29	T	665670	4590420	symbol=street_int
!NB:	S Bornes, cruzamento para Sambade
B3	Bornes 99

BORFAB	BRNES FABRICA M	29	T	674350	4599110	symbol=factory
!NB:	F�brica Mitalco
B7	Bornes 99


!G:	S Alv�o
!GW:	ALVADI
	AMARAN
	BILHO
	CABSTO
	GOUVAE
	LAMA-O
	LAMAS
	S-GRCA
	SAMARD
	VPOUCA

!G:	S Bornes
!GW:	BORNES
	GRIJO
	VBFEIT
	BAGEIX
	BORAFB
	BORAFE
	BORANT
	BORBAZ
	BORBLS
	BORCGR
	BORCHA
	BORCRZ
	BORFAB
	BORFUT
	BORGRJ
	BORIZD
	BORMCV
	BORMIR
	BORMRS
	BORPVG
	BORQCM
	BORRSS
	BORSAM
	BORSND
	BORVBF
	BRGNAE
	BRGNCT
	BRGNNB

!G:	S Estrela
!GW:	CEBOLA
	COVLHA
	COVLHS
	CPFIGR
	ERADA
	FLGSNH
	LGCOMP
	LINHRS
	PNHDRD
	SEIA
	SEIA-A
	SEIA-G
	TEIXRA
	TORRE
	UNHAIS

!G:	S Larouco
!GW:	BALTAR
	BOULSA
	CASTNH
	CHAVES
	CRVLHH
	CUALDR
	FRSTRS
	GRALHS
	LARC-N
	LARC-S
	LARC-W
	LAR-AN
	LAR-AS
	LAROA
	LODSLO
	LUCNZA
	MEIXDO
	MONTLG
	NINOAG
	PDONOE
	PDRNLS
	PISOES
	SANDRE
	SANTNN
	SARAUS
	SENDIM
	TRPENA
	TRSMIR
	V-SECA
	VBARRI
	XINZO
	XIROND
	CRANDN
	LARC-C
	SLVEIR
	VERIN
	VERINA
	VPERDZ
	ALBARE
	ALLRIZ
	PITOES
	CHA
	SNDIAS
	BLNCOS

