#!/bin/sh
# This is a Tcl/Tk script to be interpreted by wish (Tk8.0 or better): \
exec wish "$0" "$@"

#  wpsinfull --- convert old GPSManager files into new format with
#                route waypoints given in full
#
# Copyright (c) 2003-2006 Miguel Filgueiras mig@ncc.up.pt Universidade do Porto
#
#  To be used with the source of
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
# Copyright (c) 1998-2006 Miguel Filgueiras mig@ncc.up.pt Universidade do Porto
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 2 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#
#  File: wpsinfull.tcl
#  Last change:  11 July 2006
#

############ configuration parameters
#
# the first variable below MUST be correctly set!
#

 # path to directory with the sources of GPSMan
 # NOTE for non-Unix users: use "/" (not "\") in pathnames
set SRCDIR ../gmsrc

set LANG engl

#### configuration continues below
foreach f "config lang$LANG gendials compute check util files" {
    source [file join $SRCDIR $f.tcl]
}

#### more configuration

 ## for all these variables see gpsman.tcl, the GPSMan options menus
 ##   and the GPSMan documentation
set DISTUNIT KM
set ISOLATIN1 1
set DELETE 1

set DateFormat DDMMMYYYY

set TimeOffset 0
set CREATIONDATE 0

set EPOSX 340 ; set EPOSY 50
set DPOSX 290 ; set DPOSY 50
set COLOUR(messbg) "#ff8d90"
set COLOUR(dialbg) gray
set COLOUR(selbg) "#F0E9C0"
set COLOUR(check) red

########## no configurable values after this point

set FontSize [font actual default -size]

if { $FONTSIZE != "Tcl/Tk" } {
    set f [font actual default]
    set f [eval font create $f]
    font configure $f -size $FONTSIZE
    option add *Font $f
}
if { $FIXEDFONTSIZE != "Tcl/Tk" } {
    set FixedFont "fixed $FIXEDFONTSIZE"
} else { set FixedFont fixed }	    

set CMDLINE 0
set CursorsChanged 0

array set FCOMMAND {
    format  "!Format:"
    pformat "!Position:"
    datum   "!Datum:"
    dates   "!Creation:"
    0   no
    1   yes
    WP   "!W:"
    RT   "!R:"
    comment   "%"
}

set File(Data) ""
set File(old) ""
set FileTypes {old Data}
set TXT(nameold) old

set WindowStack ""

set UNIX [expr ! [string compare $tcl_platform(platform) "unix"]]

proc SetCursor {args} {}
proc ResetCursor {args} {}

### conversion

proc WPsInFull {file outfile} {
    # convert GPSMan data file to new format with RTs having WPs in full
    global MESS FCOMMAND LFileBuffFull LFileBuff LFileEOF

    puts $outfile \
	    "$FCOMMAND(comment) Written by wpsinfull/GPSManager [NowTZ]"
    puts $outfile ""
    set line [ReadFileNL $file]
    while { ! $LFileEOF($file) } {
	puts $outfile $line
	if { [string first $FCOMMAND(WP) $line] == 0 } {
	    # read/write WPs but save their textual definitions
	    set line [ReadFileNL $file]
	    while { ! $LFileEOF($file) } {
		if { [string first ! $line] == 0 } {
		    set LFileBuffFull($file) 1
		    break
		}
		puts $outfile $line
		set def $line
		set name [lindex [split $line \t] 0]
		set line [ReadFileNL $file]
		# check for remark
		if { [string first $FCOMMAND(nb) $line] == 0 } {
		    puts $outfile $line
		    set rmrk $line
		    while { [set line [ReadFileNL $file]] != "" && \
			    ! $LFileEOF($file) } {
			puts $outfile $line
			set rmrk [format "%s\n%s" $rmrk $line]
		    }
		    puts $outfile $line
		    set wptext($name) "$def\n$rmrk\n"
		    set line [ReadFileNL $file]
		} else { set wptext($name) $def }
	    }
	} elseif { [string first $FCOMMAND(RT) $line] == 0 } {
	    # read/write RTs, replacing isolated WP names by definitions
	    set line [ReadFileNL $file]
	    while { ! $LFileEOF($file) } {
		if { [string first ! $line] == 0 && \
			[string first $FCOMMAND(RS) $line] != 0 && \
			[string first $FCOMMAND(nb) $line] != 0 } {
		    set LFileBuffFull($file) 1
		    break
		}
		if { [llength [split $line \t]] == 1 } {
		    if { [catch {set def $wptext($line)}] } {
			GMMessage [format $MESS(undefinedWP) $line]
			set def [set wptext($line) $line]
		    }
		    puts $outfile $def
		} else { puts $outfile $line }
		set line [ReadFileNL $file]
	    }
	}
	set line [ReadFileNL $file]
    }
    return
}

wm protocol . WM_DELETE_WINDOW { exit 1 }
frame .fr -relief flat -borderwidth 5 -bg $COLOUR(messbg)
label .fr.title -text "wpsinfull" -relief sunken

pack .fr.title -side top -pady 5
pack .fr -side top
update idletasks

set saved ""

while 1 {
    if { [OpenInputFileFails Data GPSMan] } { exit }
    set old $File(Data) ; set File(Data) $saved
    if { [set outfile [GMOpenFile $TXT(saveto) Data w]] == ".." } { exit }
    WPsInFull $LChannel(Data) $outfile
    close $outfile
    CloseInputFile Data
    set saved $File(Data) ; set File(Data) $old
}

