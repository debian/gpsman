#!/bin/sh
# This is a Tcl/Tk script to be interpreted by wish (Tk8.0 or better): \
exec wish "$0" -- "$@"

#  mou2gmn --- convert MapsOnUS files into GPSManager files
#
#  Copyright (c) 2004 Frank Kujawski (Frank@Kujawski.org) and
#                     Miguel Filgueiras (mig@ncc.up.pt)
#
#  To be used with the source of
#
#  gpsman --- GPS Manager: a manager for GPS receiver data
#
#  Copyright (c) 2004 Miguel Filgueiras (mig@ncc.up.pt) / Universidade do Porto
#
#    This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 2 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.
#
#
#  File: mou2gmn.tcl
#  Last change:  25 July 2004
#

############ configuration parameters
#
# the first variable below MUST be correctly set for mou2gmn to work!
#

 # path to directory with the sources of GPSMan
 # NOTE for non-Unix users: use "/" (not "\") in pathnames
set SRCDIR ../gmsrc

  ## routes are created with id number starting from:
set RTID 900

 ## for all these variables see gpsman.tcl, the GPSMan options menus
 ##   and the GPSMan documentation
set DISTUNIT KM
set LANG engl
set ISOLATIN1 1
set DELETE 1

set DateFormat DDMMMYYYY

set ALTUNIT M

set TimeOffset 0
set CREATIONDATE 0

set PERMS 0644

set LISTHEIGHT 22

set EPOSX 340 ; set EPOSY 50
set DPOSX 290 ; set DPOSY 50
set COLOUR(messbg) "#ff8d90"
set COLOUR(dialbg) gray
set COLOUR(selbg) "#F0E9C0"
set COLOUR(check) red

set ALLMONTH(1) "Jan Gen jan"
set ALLMONTH(2) "Feb Fev feb"
set ALLMONTH(3) "Mar M�r mrt"
set ALLMONTH(4) "Apr Abr Avr apr"
set ALLMONTH(5) "May Mai Mag mei"
set ALLMONTH(6) "Jun Giu jun"
set ALLMONTH(7) "Jul Lug jul"
set ALLMONTH(8) "Aug Ago Aou aug"
set ALLMONTH(9) "Sep Set sep"
set ALLMONTH(10) "Oct Okt Out Ott okt"
set ALLMONTH(11) "Nov nov"
set ALLMONTH(12) "Dec Dez Dic dec"

########## no configurable values after this point

set CMDLINE 0

foreach f "lang$LANG gendials compute check util" {
    source [file join $SRCDIR $f.tcl]
}

array set FCOMMAND {
    format  "!Format:"
    pformat "!Position:"
    datum   "!Datum:"
    dates   "!Creation:"
    0   no
    1   yes
    WP   "!W:"
    RT   "!R:"
    comment   "%"
    nb   "!NB:"
}

set File(RT) ""
set File(MapsOnUS) ""
set FileTypes {RT MapsOnUS}
set TXT(nameMapsOnUS) MapsOnUS

set RTCount 0

set WindowStack ""

set UNIX [expr ! [string compare $tcl_platform(platform) "unix"]]

### conversion

proc MapsOnUSToGPSMan {infile outfile} {
    # based on the ConvertMoU.pl Perl script
    # by Frank Kujawski (Frank@Kujawski.org)
    # with changes by Miguel Filgueiras <mig@ncc.up.pt>
    global MESS FCOMMAND TimeOffset CREATIONDATE RTID RTCount

    set date [NowTZ]
    puts $outfile \
	   "$FCOMMAND(comment) Written by mou2gm: MapsOnUS to GPSManager $date"
    puts $outfile ""
    puts $outfile "$FCOMMAND(format) DDD $TimeOffset WGS 84"
    puts $outfile "$FCOMMAND(dates) $FCOMMAND($CREATIONDATE)"
    puts $outfile ""
    puts $outfile "$FCOMMAND(RT)\t[expr $RTID+$RTCount]"
    set void 1
    while { ! [eof $infile] } {
	gets $infile line
        if { [regexp {>Start<} $line] } {
	    gets $infile line
	    if { ! [regexp {>([-0-9]+\.[0-9]+), ([-0-9]+\.[0-9]+)<} $line \
		    match long lat] } { continue }
	    set name ST
	    gets $infile ; gets $infile nb
	} elseif { [regexp {>Turn ([0-9]+)<} $line match no] } {
	    gets $infile line
	    if { ! [regexp {>([-0-9]+\.[0-9]+), ([-0-9]+\.[0-9]+)<} $line \
		    match long lat] } { continue }
	    set name T$no
	    gets $infile ; gets $infile ; gets $infile ; gets $infile nb
	} elseif { [regexp {>End<} $line] } {
	    gets $infile line
	    if { ! [regexp {>([-0-9]+\.[0-9]+), ([-0-9]+\.[0-9]+)<} $line \
		    match long lat] } { continue }
	    set name END
	    set nb ""
	} else { continue }
	set nb [string trimright "$nb" ","]
	set name "$RTCount-$name"
        if { ! [CheckLat GMMessage $lat DDD] || \
                ! [CheckLong GMMessage $long DDD] } { continue }
        set latd [Coord DDD $lat S] ; set longd [Coord DDD $long W]
	set p "$latd\t$longd"
	if { $CREATIONDATE } {
	    puts $outfile "$name\t\t$date\t$p"
	} else {
	    puts $outfile "$name\t\t$p"
	}
	if { "$nb" != "" } {
	    puts $outfile "$FCOMMAND(nb) $nb"
	    puts $outfile ""
	}
	set void 0
    }
    if { $void } {
	GMMessage $MESS(voidRT) wait
	return
    }
    puts $outfile ""
    incr RTCount
    return
}

wm protocol . WM_DELETE_WINDOW { exit 1 }
frame .fr -relief flat -borderwidth 5 -bg $COLOUR(messbg)
label .fr.title -text "mou2gmn" -relief sunken

pack .fr.title -side top -pady 5
pack .fr -side top
update idletasks

if { ! [GMChooseParams $TXT(select) RTID \
	               [list "=$TXT(number) ($TXT(nameRT))"]] } {
    exit
}

set RTID [string trim "$RTID" " "]
if { ! [regexp {^[0-9]+$} $RTID] } { bell ; exit }

while 1 {
    if { [set infile [GMOpenFile $TXT(loadfrm) MapsOnUS r]] == ".." } {
	exit
    }
    if { [set outfile [GMOpenFile $TXT(saveto) RT w]] == ".." } { exit }
    MapsOnUSToGPSMan $infile $outfile
    close $outfile
    close $infile
}





